<div id="analyst-opt-out-modal" class="analyst-modal">
	<div class="analyst-modal-content" style="width: 600px">
		<div class="analyst-disable-modal-mask" id="analyst-disable-opt-out-modal-mask" style="display: none"></div>
		<div style="display: flex">
			<div class="analyst-install-image-block" style="width: 120px">
				<img src="<?=$shieldImage?>"/>
			</div>
			<div class="analyst-install-description-block">
				<strong class="analyst-modal-header">By opting out, we cannot alert you anymore  in case of important security updates.</strong>
				<p class="analyst-install-description-text">
					In addition, we won’t get pointers how to further improve the plugin based on your integration with our plugin.
				</p>
			</div>
		</div>
		<div class="analyst-modal-def-top-padding">
			<button class="analyst-btn-success opt-out-modal-close">Ok, don't opt out</button>
		</div>
		<div class="analyst-modal-def-top-padding" style="text-align: center;">
			<button class="analyst-btn-secondary-ghost" id="opt-out-action">Opt out</button>
		</div>
	</div>
	</div>
</div>

<script type="text/javascript">

	(function ($) {
	  var isOptingOut = false

	  $('#analyst-opt-out-modal').appendTo($('body'))

      $(document).on('click', '.analyst-opt-out', function() {
        var pluginId = $(this).attr('analyst-plugin-id')

        $('#analyst-opt-out-modal')
		  .attr({'analyst-plugin-id': pluginId})
		  .show()
      })

	  $('.opt-out-modal-close').click(function () {
        $('#analyst-opt-out-modal').hide()
      })

	  $('#opt-out-action').click(function () {
		if (isOptingOut) return

		var pluginId = $('#analyst-opt-out-modal').attr('analyst-plugin-id')

        $('#analyst-disable-opt-out-modal-mask').show()

		var self = this

	    isOptingOut = true

        $(self).text('Opting out...')

        $.ajax({
          url: ajaxurl,
          method: 'POST',
          data: {
            action: 'analyst_opt_out_' + pluginId,
          },
          success: function () {
            $('#analyst-opt-out-modal').css({display: 'none'})

            $(self).text('Opt out')

			isOptingOut = false

            var optInAction = $('<a />').attr({
			  class: 'analyst-action-opt analyst-opt-in',
			  'analyst-plugin-id': pluginId,
                'analyst-plugin-signed': '1'
            })
			  .text('Opt In')
            $('.analyst-opt-out[analyst-plugin-id="'+ pluginId +'"').replaceWith(optInAction)

            $('[analyst-plugin-id="' + pluginId + '"').attr('analyst-plugin-opted-in', 0)

            $('#analyst-disable-opt-out-modal-mask').hide()
          }
        })
      })
    })(jQuery)
</script>
