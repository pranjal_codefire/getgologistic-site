<?php

return [
    // The sdk version
    'sdk' => '1.3.9',

    // Minimum supported WordPress version
    'wp' => '4.7',

    // Supported PHP version
    'php' => '5.4',

    // Path to current SDK$
    'path' => __DIR__,
];
