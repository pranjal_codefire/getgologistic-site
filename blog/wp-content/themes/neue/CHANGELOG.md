### V1.8.2 – June 26th, 2015
* IMPROVED: Link to user page for YouTube social counter
* IMPROVED: Facebook counter for social widget
* IMPROVED: Demo content importer

### V1.8.1 – June 16th, 2015
* UPDATED: Translation files
* IMPROVED: YouTube counter in Social Counter widget
* FIXED: Author email link

### V1.8.0 – May 30th, 2015
* IMPROVED: WP4.2 supports
* IMPROVED: Translatable from child theme
* IMPROVED: Lightbox for images in post content
* IMPROVED: Post view counter
* IMPROVED: Social counter displaying
* UPDATED: Demo importing
* UPDATED: Post options panel
* UPDATED: Post options panel
* UPDATED: Plugin activation
* FIXED: Post meta layout
* FIXED: Author bio in archive page

## V1.7.1 – February 17th, 2015
* IMPROVED: Review score for rich snippets
* IMPROVED: Icon list shortcode for a long line
* FIXED: Remove extra space (BR tags) from Custom Content section
* FIXED: Lightbox for image links in post content

### V1.7.0 – February 2nd, 2015
* IMPROVED: Lightbox for image links in post content
* IMPROVED: Facebook counter for Social Counter widget
* FIXED: Big Slider
* FIXED: Posts shortcode
* FIXED: Big slider on mobile
* FIXED: Blog left sidebar
* FIXED: Flexslider title
* FIXED: Space of sidebar in Page Composer
* FIXED: Login url in for the Login widget
* FIXED: Error on related posts section
* FIXED: Review score was disappeared when the score is zero
* FIXED: Column shortcode on mobile device

### V1.6.1 – November 22th, 2014
* FIXED: Error when using HTML tags in shortcode attribute on WP 4.0.1

### V1.6.0 – November 5th, 2014
* NEW: Exclude categories option for Post Slider and Big Slider in Simple Page Composer
* NEW: Back-to-top button and option to disable in theme options
* IMPROVED: Shortcode supports in header ads
* IMPROVED: Theme option panel engine

### V1.5.1 – September 26th, 2014
* IMPROVED: Wordpress 4.0 supports
* FIXED: Responsive logo and header ads
* FIXED: The zero of Facebook and twitter counter (rare case)

### V1.5.0 – September 21th, 2014
* NEW: General link and Personal website option for author profile
* IMPROVED: Author's public email protection
* IMPROVED: Google Responsive Ads in Header Ads
* FIXED: Dropcap shortcode option in shortcode editor
* FIXED: Page title color when displayed on full-width page template
* FIXED: Post shortcode when the specified category is not existing
* FIXED: Default post layout option

### V1.4.0 – August 17th, 2014
* NEW: StumbleOpon and Email icon for site's social profiles in Theme Options
* NEW: Offset option for Post Box in Page Composer
* IMPROVED: Shortcode editor for WP4.0
* FIXED: Category exclusion option in Post shortcode
* FIXED: Invalid email link in author profile

### V1.3.1 – July 28th, 2014
* IMPROVED: Page Composer for WP4.0
* IMPROVED: Link to attachment page
* FIXED: Instant search was broken since v1.3.0
* FIXED: Footer sidebar No.4
* FIXED: Shortcode editor on WPEngine

### V1.3.0 – July 20th, 2014
* NEW: Soundcloud icon for author profile
* IMPROVED: Google Plus counter of social counter widget
* IMPROVED: Author description
* IMPROVED: Open Graph with BuddyPress
* IMPROVED: Instant search on PHP5.5
* IMPROVED: Translation for instant search
* FIXED: Default post layout setting
* FIXED: Invalid user avatar on Login widget
* FIXED: Show Google+ icon for author

### V1.2.1 – July 3rd, 2014
* FIXED: Default value of 'Force Enable RTL' option. This is caused of right alignment of layout.
  This problem is occurred only V1.2.0 from adding a new option 'Force Enable RTL'.
  Who are using V1.2.0 and found this problem. Please turn off the option 'Force Enable RTL' in 'Theme Options > General' page.

### V1.2.0 – July 2nd, 2014
* NEW: Custom font shortcode, Just use [customfont1] or [customfont2]
* NEW: Option to set the main menu font
* NEW: Option to force enable RTL
* IMPROVED: RTL Supports
* FIXED: Full-width featured image when using the minify plugin
* FIXED: Big slider on large screen
* FIXED: Header ads and logo layout

### V1.1.0 – June 26th, 2014
* NEW: Full-Width-Top-Aligned post layout
* NEW: Theme option for disabling the breaking news
* NEW: Theme option for selecting the blog sidebar position and 2 sidebar supports
* NEW: Big slider (Full-width) for Simple Page Composer
* IMPROVED: Caching of Social Counter widget
* IMPROVED: Page load performance
* IMPROVED: Rich snippets
* FIXED: Mini logo spacing on load
* FIXED: Email text box for Contact form
* FIXED: EM shortcode on shortcode editor
* FIXED: Category thumbnail did not display in some case

### V1.0.0 – June 9th, 2014
* NEW: Clean and Modern Magazine Wordpress Theme