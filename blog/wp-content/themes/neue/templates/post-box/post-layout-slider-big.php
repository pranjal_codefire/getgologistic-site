<div class="flexslider vw-post-slider vw-post-box-layout-slider-big vw-post-slider-no-control-nav vw-post-slider-template-default clearfix">
	<ul class="slides">

	<?php while( have_posts() ) : the_post(); ?>
		<li class="vw-post-slider-slide">
			<?php get_template_part( 'templates/post-box/post-box-big-boxed', get_post_format() ); ?>
		</li>
	<?php endwhile; ?>
	
	</ul>
</div>