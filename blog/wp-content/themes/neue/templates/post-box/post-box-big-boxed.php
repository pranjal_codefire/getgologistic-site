<div class="vw-post-box vw-post-box-big-boxed">
	<div class="vw-post-box-thumbnail"><?php the_post_thumbnail( VW_CONST_THUMBNAIL_SIZE_POST_BOX_BIG_BOXED ); ?></div>

	<div class="vw-post-box-overlay">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<?php vw_the_category(); ?>
					<h1 class="vw-post-box-post-title">
						<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark"><?php the_title(); ?></a>
					</h1>
					<div class="vw-post-meta">

						<div class="vw-post-meta-left">
							<!-- Author -->
							<?php echo vw_get_avatar( get_the_author_meta('user_email'), VW_CONST_AVATAR_SIZE_SMALL ); ?>
							<a class="author-name author" href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>" title="<?php _e('View all posts by', 'envirra'); ?> <?php the_author(); ?>"><?php the_author(); ?></a>
							
							<span class="vw-post-meta-separator">&mdash;</span>

							<!-- Post date -->
							<a href="<?php the_permalink(); ?>" class="vw-post-date updated" title="<?php printf( esc_attr__('Permalink to %s', 'envirra'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php vw_the_post_date(); ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>