<?php /* Alternate version of 'post-box-medium.php' for using on 'post-layout-medium-grid-3-col' and reduce image size to be loaded */ ?>
<div class="vw-post-box vw-post-box-style-top-thumbnail vw-post-box-medium">
	<?php if ( has_post_thumbnail() ) : ?>
	<a class="vw-post-box-thumbnail" href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark"><?php the_post_thumbnail( 'vw_one_third_thumbnail' ); ?></a>
	<?php endif; ?>

	<?php vw_the_category(); ?>
	<h4 class="vw-post-box-post-title">
		<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark"><?php the_title(); ?></a>
	</h4>
	
	<?php get_template_part( 'templates/post-box/post-box-meta-small' ); ?>

</div>