<?php get_header(); ?>

<?php get_template_part( 'templates/page-title', apply_filters( 'vw_filter_page_title_template_name', '' ) ); ?>

<?php $blog_sidebar_position = vw_get_theme_option( 'blog_sidebar_position' ); ?>

<div class="vw-page-wrapper clearfix <?php printf( 'vw-blog-sidebar-postition-%s', $blog_sidebar_position ); ?>">

	<div class="container">
		<div class="row">
			<?php if ( $blog_sidebar_position == 'left-content-right' || $blog_sidebar_position == 'content-left-right' ) : ?>
			<div class="vw-page-content col-md-6" role="main">
			<?php else: ?>
			<div class="vw-page-content col-md-8" role="main">
			<?php endif; ?>

				<?php if ( vw_has_category_top_content() ) vw_the_category_top_content(); ?>

				<?php if ( vw_has_category_slider() ) vw_the_category_post_slider(); ?>

				<?php do_action( 'vw_action_before_archive_posts' ); ?>
				
				<?php get_template_part( 'templates/post-box/post-layout-'.vw_get_archive_blog_layout() ); ?>

				<?php vw_the_pagination(); ?>

				<?php do_action( 'vw_action_after_archive_posts' ); ?>

			</div>

		<?php if ( $blog_sidebar_position == 'left-content-right' || $blog_sidebar_position == 'content-left-right' ) : ?>

			<aside class="vw-page-sidebar vw-page-left-sidebar col-md-3">
				<?php get_sidebar( apply_filters( 'vw_filter_single_post_left_sidebar_name', 'left' ) ); ?>
			</aside>

			<aside class="vw-page-sidebar vw-page-right-sidebar col-md-3">
				<?php get_sidebar( apply_filters( 'vw_filter_single_post_sidebar_name', '' ) ); ?>
			</aside>

		<?php elseif ( $blog_sidebar_position == 'left' ) : ?>

			<aside class="vw-page-sidebar vw-page-left-sidebar col-md-4">
				<?php get_sidebar( apply_filters( 'vw_filter_single_post_left_sidebar_name', 'left' ) ); ?>
			</aside>

		<?php else: ?>

			<aside class="vw-page-sidebar vw-page-right-sidebar col-md-4">
				<?php get_sidebar( apply_filters( 'vw_filter_single_post_sidebar_name', '' ) ); ?>
			</aside>

		<?php endif; ?>

		</div>
	</div>
</div>

<?php get_footer(); ?>