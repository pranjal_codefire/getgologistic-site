<?php

if ( ! defined( 'VW_CONST_REDUX_ASSET_URL' ) ) define( 'VW_CONST_REDUX_ASSET_URL', get_template_directory_uri() . '/images/admin' );

/* -----------------------------------------------------------------------------
 * Prepare Options
 * -------------------------------------------------------------------------- */
$theme = wp_get_theme();
$vw_opt_name = 'vw_neue';
$args = array(
	// TYPICAL -> Change these values as you need/desire
	'opt_name'             => $vw_opt_name,
	// This is where your data is stored in the database and also becomes your global variable name.
	'display_name'         => $theme->get( 'Name' ),
	// Name that appears at the top of your panel
	'display_version'      => $theme->get( 'Version' ),
	// Version that appears at the top of your panel
	'menu_type'            => 'menu',
	//Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
	'allow_sub_menu'       => true,
	// Show the sections below the admin menu item or not
	'menu_title'           => 'Theme Options',
	'page_title'           => 'Theme Options',
	// You will need to generate a Google API key to use this feature.
	// Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
	'google_api_key'       => 'AIzaSyCNyDK8sPUuf9bTcG1TdFFLAVUfA1IDm38',
	// Set it you want google fonts to update weekly. A google_api_key value is required.
	'google_update_weekly' => false,
	// Must be defined to add google fonts to the typography module
	'async_typography'     => true,
	// Use a asynchronous font on the front end or font string
	//'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
	'admin_bar'            => true,
	// Show the panel pages on the admin bar
	'admin_bar_icon'       => 'dashicons-portfolio',
	// Choose an icon for the admin bar menu
	'admin_bar_priority'   => 50,
	// Choose an priority for the admin bar menu
	'global_variable'      => '',
	// Set a different name for your global variable other than the opt_name
	'dev_mode'             => false,
	// Show the time the page took to load, etc
	'update_notice'        => false,
	// If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
	'customizer'           => true,
	// Enable basic customizer support
	//'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
	//'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

	// OPTIONAL -> Give you extra features
	'page_priority'        => null,
	// Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
	'page_parent'          => 'themes.php',
	// For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
	'page_permissions'     => 'manage_options',
	// Permissions needed to access the options panel.
	'menu_icon'            => '',
	// Specify a custom URL to an icon
	'last_tab'             => '',
	// Force your panel to always open to a specific tab (by id)
	'page_icon'            => 'icon-themes',
	// Icon displayed in the admin panel next to your menu_title
	'page_slug'            => 'vw_theme_options',
	// Page slug used to denote the panel
	'save_defaults'        => true,
	// On load save the defaults to DB before user clicks save or not
	'default_show'         => false,
	// If true, shows the default value next to each field that is not the default value.
	'default_mark'         => '',
	// What to print by the field's title if the value shown is default. Suggested: *
	'show_import_export'   => true,
	// Shows the Import/Export panel when not used as a field.

	// CAREFUL -> These options are for advanced use only
	'transient_time'       => 60 * MINUTE_IN_SECONDS,
	'output'               => true,
	// Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
	'output_tag'           => true,
	// Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
	// 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

	// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
	'database'             => '',
	// possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
	'system_info'          => false,
	// REMOVE

	//'compiler'             => true,

	// HINTS
	'hints'                => array(
		'icon'          => 'el el-question-sign',
		'icon_position' => 'right',
		'icon_color'    => 'lightgray',
		'icon_size'     => 'normal',
		'tip_style'     => array(
			'color'   => 'light',
			'shadow'  => true,
			'rounded' => false,
			'style'   => '',
		),
		'tip_position'  => array(
			'my' => 'top left',
			'at' => 'bottom right',
		),
		'tip_effect'    => array(
			'show' => array(
				'effect'   => 'slide',
				'duration' => '500',
				'event'    => 'mouseover',
			),
			'hide' => array(
				'effect'   => 'slide',
				'duration' => '500',
				'event'    => 'click mouseleave',
			),
		),
	)
);

$help_html = '<a href="http://envirra.com/themes/neue/document/" target="_blank"><img src="'.get_template_directory_uri().'/images/admin/help-documentation.png"></a>';
$help_html .= '<a href="http://envirra.com/themes/neue/document/#troubleshooting" target="_blank"><img src="'.get_template_directory_uri().'/images/admin/help-troubleshooting.png"></a>';
$help_html .= '<a href="http://themeforest.net/user/envirra/portfolio?ref=envirra" target="_blank"><img src="'.get_template_directory_uri().'/images/admin/help-more-themes.png"></a>';

/* -----------------------------------------------------------------------------
 * Init Options
 * -------------------------------------------------------------------------- */
if ( ! class_exists( 'Redux' ) && file_exists( get_template_directory() . '/framework/redux-framework/redux-framework.php' ) ) {
	require_once( get_template_directory() . '/framework/redux-framework/redux-framework.php' );
}

if ( ! class_exists( 'Redux' ) ) return;

Redux::setArgs( $vw_opt_name, $args );

/**
General
 */

Redux::setSection( $vw_opt_name, array(
	'title' => 'General',
	'id'    => 'vw-options-general',
	'desc'  => '',
	'icon'  => 'el el-website',
	'fields'     => array(
		array(
			'id'=>'theme_info_1',
			'type' => 'raw', 
			'content' => $help_html,
		),
		
		array(
			'id'=>'site_enable_back_to_top',
			'type' => 'switch', 
			'title' => 'Enable Back To Top Button',
			'default' => 1,
		),

		array(
			'id'=>'site_enable_open_graph',
			'type' => 'switch', 
			'title' => 'Enable Facebook Open Graph Supports',
			'default' => 1,
		),

		array(
			'id'=>'site_force_enable_rtl',
			'type' => 'switch', 
			'title' => 'Force Enable RTL',
			'subtitle'=> 'Enabling this option, The site will be shown in RTL direction. Otherwise, The RTL will be turned on automatically when site language is also RTL lanugage.',
			'default' => 0,
		),

		array(
			'id'=>'page_force_disable_comments',
			'type' => 'switch', 
			'title' => 'Force Disable Page Comments',
			'subtitle'=> 'Enabling this option, All page comment will be disabled.',
			'default' => 0,
		),

		array(
			'id'=>'site_404',
			'type' => 'select',
			'title' => '404 Page', 
			'subtitle' => 'Select the page to be displayed on page/post not found',
			'data' => 'page',
		),

		array(
			'id'=>'tracking_code',
			'type' => 'ace_editor',
			'theme' => 'monokai',
			'mode' => 'html',
			'title' => 'Tracking Code',
			'subtitle' => 'Paste your Google Analytics or other tracking code here.',
			'desc'=> 'The code must be a valid HTML and including the <em>&lt;script&gt;</em> tag.',
		),
	),
) );

/**
Site
 */

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Site',
	'desc'       => '',
	'id'         => 'vw-options-site',
	'icon'       => 'el el-home',
	'fields'     => array(
		array(
			'id'=>'site_layout',
			'type' => 'select',
			'title' => 'Site Layout', 
			'subtitle' => 'Select the site layout.',
			'options' => array(
				'full-width' => 'Full-Width Layout',
				'boxed' => 'Boxed Layout',
			),
			'default' => 'full-width',
		),

		array(
			'id'=>'site_background',
			'type' => 'background', 
			'url'=> true,
			'title' => 'Site Background',
			'subtitle' => 'Upload background image to be a site background (only visible for <strong>Boxed layout</strong>).',
			'output' => array( 'body' ),
		),

		array(
			'id'=>'header_background',
			'type' => 'background', 
			'url'=> true,
			'title' => 'Header Background',
			// 'compiler' => 'true',
			'subtitle' => 'Upload background image for header',
			'output' => array( '.vw-site-header-background' ),
		),

		array(
			'id'=>'site_top_bar',
			'type' => 'select',
			'title' => 'Site Top-bar', 
			'subtitle' => 'Select a site top-bar style.',
			'desc' => 'When choosing "Custom 1" or "Custom 2", You need to override the template file from child theme. Please see the file name to be overriden on documentation.',
			'options' => array(
				'none' => 'Not Shown',
				'menu-social' => 'Top Menu / Social Links',
				'custom-1' => 'Custom 1',
				'custom-2' => 'Custom 2',
			),
			'default' => 'menu-social',
		),

		array(
			'id'=>'site_enable_breaking_news',
			'type' => 'switch', 
			'title' => 'Enable Breaking News',
			'default' => 1,
		),

		array(
			'id'=>'site_enable_sticky_menu',
			'type' => 'switch', 
			'title' => 'Enable Sticky Menu',
			'default' => 1,
		),

		array(
			'id'=>'site_enable_breadcrumb',
			'type' => 'switch', 
			'title' => 'Enable Breadcrumb',
			'default' => 1,
		),

		array(
			'id'=>'site_bottom_bar',
			'type' => 'select',
			'title' => 'Site Bottom-bar', 
			'subtitle' => 'Select the site bottom-bar style.',
			'desc' => 'When choosing "Custom 1" or "Custom 2", You need to override the template file from child theme. Please see the file name to be overriden on documentation.',
			'options' => array(
				'none' => 'Not Shown',
				'copyright-menu' => 'Copyright / Bottom Menu',
				'copyright-social' => 'Copyright / Social Links',
				'menu-social' => 'Bottom Menu / Social Links',
				'menu-copyright' => 'Bottom Menu / Copyright',
				'custom-1' => 'Custom 1',
				'custom-2' => 'Custom 2',
			),
			'default' => 'copyright-social',
		),

		array(
			'id'=>'copyright_text',
			'type' => 'textarea', 
			'title' => 'Copyright',
			'subtitle'=> 'Enter copyright text',
			'default' => 'Copyright &copy;, All Rights Reserved.',
		),

		array(
			'id'=>'site_footer_layout',
			'type' => 'image_select',
			'title' => 'Site Footer Layout', 
			'subtitle' => 'Select footer sidebar layout.',
			'options' => array(
					'3,3,3,3' => array('alt' => '1/4 + 1/4 + 1/4 + 1/4 Column', 'img' => VW_CONST_REDUX_ASSET_URL.'/footer-layout-1_4-1_4-1_4-1_4.png'),
					'6,3,3' => array('alt' => '1/2 + 1/4 + 1/4 Column', 'img' => VW_CONST_REDUX_ASSET_URL.'/footer-layout-1_2-1_4-1_4.png'),
					'3,3,6' => array('alt' => '1/4 + 1/4 + 1/2 Column', 'img' => VW_CONST_REDUX_ASSET_URL.'/footer-layout-1_4-1_4-1_2.png'),
					'6,6' => array('alt' => '1/2 + 1/2 Column', 'img' => VW_CONST_REDUX_ASSET_URL.'/footer-layout-1_2-1_2.png'),
					'4,4,4' => array('alt' => '1/3 + 1/3 + 1/3 Column', 'img' => VW_CONST_REDUX_ASSET_URL.'/footer-layout-1_3-1_3-1_3.png'),
					'8,4' => array('alt' => '2/3 + 1/3 Column', 'img' => VW_CONST_REDUX_ASSET_URL.'/footer-layout-2_3-1_3.png'),
					'4,8' => array('alt' => '1/3 + 2/3 Column', 'img' => VW_CONST_REDUX_ASSET_URL.'/footer-layout-1_3-2_3.png'),
					'3,6,3' => array('alt' => '1/4 + 1/2 + 1/4 Column', 'img' => VW_CONST_REDUX_ASSET_URL.'/footer-layout-1_4-1_2-1_4.png'),
					'12' => array('alt' => '1/1 Column', 'img' => VW_CONST_REDUX_ASSET_URL.'/footer-layout-1_1.png'),
					'none' => array('alt' => 'No footer', 'img' => VW_CONST_REDUX_ASSET_URL.'/footer-layout-none.png'),
				),
			'default' => '4,4,4',
		),
	),
) );

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Header Ads',
	'desc'       => 'Insert Ads on site header. The ads will be displayed depends on width of screen.',
	'id'         => 'vw-options-site-header-ads',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'=>'header_ads_banner',
			'type' => 'ace_editor',
			'theme' => 'monokai',
			'mode' => 'html',
			'title' => '728x90 Ads',
			'subtitle' => 'Paste your ads code here. If you are using Responsive Ads, Please enter the code only on this option.',
		),

		array(
			'id'=>'header_ads_leaderboard',
			'type' => 'ace_editor',
			'theme' => 'monokai',
			'mode' => 'html',
			'title' => '468x60 Ads',
			'subtitle' => 'Paste your ads code here.',
		),
	),
) );

/**
Blog/Archive
 */
Redux::setSection( $vw_opt_name, array(
	'title'      => 'Blog / Archive',
	'desc'       => '',
	'id'         => 'vw-options-blog',
	'icon'       => 'el el-pencil',
	'fields'     => array(
		array(
			'id'=>'blog_default_layout',
			'type' => 'select',
			'title' => 'Default Blog Layout', 
			'subtitle' => 'Select default blog layout for blog page, search page, archive and category.',
			'options' => array(
				'classic-1-col' => 'Classic',
				'article-1-col' => 'Large Article',
				'large-grid' => '2-Columns Grid',
				'custom-1' => 'Custom 1',
				'custom-2' => 'Custom 2',
				'custom-3' => 'Custom 3',
				'custom-4' => 'Custom 4',
			),
			'default' => 'article-1-col',
		),

		array(
			'id'=>'blog_post_layout',
			'type' => 'select',
			'title' => 'Default Post Layout', 
			'subtitle' => 'Select default post layout.',
			'options' => array(
				'classic' => 'Classic',
				'classic-no-featured-image' => 'Classic - No Featured Image',
				'full-width-featured-image' => 'Full-Width Featured Image',
				'full-width-featured-image-top' => 'Full-Width Featured Image (Top Aligned)',
				'custom-1' => 'Custom 1',
				'custom-2' => 'Custom 2',
			),
			'default' => 'classic',
		),

		array(
			'id'=>'blog_default_sidebar',
			'type' => 'select',
			'title' => 'Default Right Sidebar',
			'subtitle' => 'Select a default right sidebar for blog pages.',
			'data' => 'sidebar',
			'default' => 'blog-sidebar',
		),

		array(
			'id'=>'blog_default_left_sidebar',
			'type' => 'select',
			'title' => 'Default Left Sidebar',
			'subtitle' => 'Select a default left sidebar for blog pages. <strong>Do not</strong> choose the same sidebar with Default Right Sidebar option.',
			'data' => 'sidebar',
			'default' => 'blog-sidebar',
		),

		array(
			'id'=>'blog_sidebar_position',
			'type' => 'select',
			'title' => 'Default Sidebar Position', 
			'subtitle' => 'Select default sidebar position.',
			'options' => array(
				'right' => 'Right',
				'left' => 'Left',
				'left-content-right' => 'Left / Content / Right',
				'content-left-right' => 'Content / Left / Right',
			),
			'default' => 'right',
		),

		array(
			'id'=>'blog_excerpt_length',
			'type' => 'text',
			'title' => 'Excerpt Length', 
			'subtitle'=> 'The number of first words to be show when the custom excerpt is not provided.',
			'validate' => 'numeric',
			'default' => '50',
		),

		array(
			'id'=>'blog_enable_post_views',
			'type' => 'switch', 
			'title' => 'Enable Post Views',
			'subtitle'=> 'Turn on this option to show the post views.',
			'default' => 1,
		),
	),
) );

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Post',
	'id'         => 'vw-options-blog-post',
	'subsection' => true,
	'fields'     => array(
		array(
			'id' => 'post_footer_sections',
			'type' => 'sorter',
			'title' => 'Post Footer Sections',
			'subtitle' => 'Organize how you want the order of additional sections to appear on the footer of post.',
			'options' => array(
				'enabled' => array(
					'post-navigation' => 'Next/Previous Post',
					'about-author' => 'About Author',
					'related-posts' => 'Related Posts',
					'comments' => 'Comments',
				),
				'disabled' => array(
					'custom-1' => 'Custom Section 1',
					'custom-2' => 'Custom Section 2',
				)
			),
		),

		array(
			'id'=>'post_footer_section_custom_1',
			'type' => 'editor', 
			'title' => 'Post Footer - Custom Section 1',
			'subtitle'=> 'Enter the content.',
		),

		array(
			'id'=>'post_footer_section_custom_2',
			'type' => 'editor', 
			'title' => 'Post Footer - Custom Section 2',
			'subtitle'=> 'Enter the content.',
		),
	),
) );

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Related Posts',
	'id'         => 'vw-options-blog-related-posts',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'=>'related_post_layout',
			'type' => 'select',
			'title' => 'Related Post Layout', 
			'subtitle' => 'Select related post layout.',
			'options' => array(
				'medium-grid-3-col' => 'Medium Grid',
				'small-grid' => 'Small Grid',
				'boxed' => 'Boxed',
				'custom-1' => 'Custom 1',
				'custom-2' => 'Custom 2',
				'custom-3' => 'Custom 3',
				'custom-4' => 'Custom 4',
			),
			'default' => 'boxed',
		),
		array(
			'id'=>'related_post_count',
			'type' => 'text',
			'title' => 'Number of Related Posts', 
			'subtitle'=> 'The number of related posts to be displayed.',
			'validate' => 'numeric',
			'default' => '4',
		),
	),
) );

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Custom Tiled Gallery',
	'id'         => 'vw-options-blog-custom-tiled-galley',
	'subsection' => true,
	'fields'     => array(
		array(
			'id' => 'blog_enable_custom_tiled_gallery',
			'type' => 'switch',
			'title' => 'Enable Custom Tiled Gallery',
			'subtitle' => 'Turn it off if you need to use the Jetpack Carousel or other gallery plugins.',
			'default' => '1' // 1 = checked | 0 = unchecked
		),

		array(
			'id' => 'blog_custom_tiled_gallery_layout',
			'type' => 'text',
			'title' => 'Tiled Gallery Layout',
			'subtitle' => 'A numbers representing the number of columns for each row. Example, "213" is the 1st row has 2 images, 2nd row has 1 image, 3rd row has 3 images.',
			'validate' => 'numeric',
			'default' => '213'
		),
	),
) );

/**
Typography
 */
Redux::setSection( $vw_opt_name, array(
	'title'      => 'Typography',
	'desc'       => '',
	'id'         => 'vw-options-typography',
	'icon'       => 'el el-fontsize',
	'fields'     => array(
		array(
			'id'=>'typography_header',
			'type' => 'typography', 
			'title' => 'Heading Text',
			'subtitle'=> 'Choose font for heading text (H1 - H6).',
			// 'compiler'=>true, // Use if you want to hook in your own CSS compiler
			'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
			'font-backup'=>false, // Select a backup non-google font in addition to a google font
			// 'font-style'=>true, // Includes font-style and weight. Can use font-style or font-weight to declare
			'subsets'=>false, // Only appears if google is true and subsets not set to false
			'font-size'=>false,
			'text-align'=>false,
			'line-height'=>false,
			'text-transform'=>true,
			'custom_fonts'=>true,
			'all_styles'=> VW_CONST_LOAD_ALL_HEADER_GOOGLE_FONT_STYLES,
			//'word-spacing'=>true, // Defaults to false
			// 'letter-spacing'=>true, // Defaults to false
			//'color'=>false,
			//'preview'=>false, // Disable the previewer
			// 'output'=>false, // Disable the output
			'output' => array('h1, h2, h3, h4, h5, h6'), // An array of CSS selectors to apply this font style to dynamically
			'units'=>'px', // Defaults to px
			'default'=> array(
				'color'=>"#444444", 
				'font-weight'=>'400', 
				'font-family'=>'Bitter', 
				'google' => true,
				'text-transform'=>'none',
				// 'font-size'=>'14px', 
				// 'line-height'=>'1.5',
			),
		),
		array(
			'id'=>'typography_main_menu',
			'type' => 'typography', 
			'title' => 'Main Menu Text',
			'subtitle'=> 'Choose font for top level menu link.',
			// 'compiler'=>true, // Use if you want to hook in your own CSS compiler
			'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
			'font-backup'=>false, // Select a backup non-google font in addition to a google font
			// 'font-style'=>true, // Includes font-style and weight. Can use font-style or font-weight to declare
			'subsets'=>false, // Only appears if google is true and subsets not set to false
			'font-size'=>true,
			'text-align'=>false,
			'line-height'=>false,
			'text-transform'=>true,
			'custom_fonts'=>true,
			//'word-spacing'=>true, // Defaults to false
			// 'letter-spacing'=>true, // Defaults to false
			'color'=>false,
			//'preview'=>false, // Disable the previewer
			// 'output'=>false, // Disable the output
			'output' => array('.vw-menu-location-main .main-menu-link span'), // An array of CSS selectors to apply this font style to dynamically
			'units'=>'px', // Defaults to px
			'default'=> array(
				'font-weight'=>'400', 
				'font-family'=>'Bitter', 
				'google' => true,
				'text-transform'=>'none',
				'font-size'=>'13px', 
				// 'line-height'=>'1.5',
			),
		),
		array(
			'id'=>'typography_body',
			'type' => 'typography', 
			'title' => 'Body Text',
			'subtitle'=> 'Choose font for body text.',
			// 'compiler'=>true, // Use if you want to hook in your own CSS compiler
			'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
			'font-backup'=>true, // Select a backup non-google font in addition to a google font
			// 'font-style'=>true, // Includes font-style and weight. Can use font-style or font-weight to declare
			'subsets'=>false, // Only appears if google is true and subsets not set to false
			'font-size'=>true,
			'text-align'=>false,
			'line-height'=>false,
			'text-transform'=>false,
			'all_styles'=> VW_CONST_LOAD_ALL_BODY_GOOGLE_FONT_STYLES,
			//'word-spacing'=>true, // Defaults to false
			// 'letter-spacing'=>true, // Defaults to false
			'color'=>true,
			//'preview'=>false, // Disable the previewer
			// 'output'=>false, // Disable the output
			'output' => array( 'body', '#bbpress-forums' ), // An array of CSS selectors to apply this font style to dynamically
			'units'=>'px', // Defaults to px
			'default'=> array(
				'color'=>"#777777", 
				'font-weight'=>'400', 
				'font-backup'=>'Arial, Helvetica, sans-serif', 
				'google' => true,
				// 'text-transform'=>'none',
				'font-size'=>'13px',
				// 'line-height'=>'1.5',
			),
		),
	),
) );

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Custom Font 1',
	'desc'       => "Upload your font files for using as font name <strong>Custom Font 1</strong>. You can also use the shortcode <strong>[customfont1]Your Text[/customfont1]</strong> in the content.</strong>",
	'id'         => 'vw-options-typography-custom-font-1',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'=>'section',
			'type' => 'section',
			'title' => 'Custom Font 1',
			'subtitle' => "",
			'indent' => true,
		),
		array(
			'id'=>'custom_font1_ttf',
			'type' => 'media',
			'preview'=> false,
			'mode'=> 'font',
			'title' => '.TTF/.OTF Font File',
		),
		array(
			'id'=>'custom_font1_woff',
			'type' => 'media',
			'preview'=> false,
			'mode'=> 'font',
			'title' => '.WOFF Font File',
		),
		array(
			'id'=>'custom_font1_svg',
			'type' => 'media',
			'preview'=> false,
			'mode'=> 'font',
			'title' => '.SVG Font File',
		),
		array(
			'id'=>'custom_font1_eot',
			'type' => 'media',
			'preview'=> false,
			'mode'=> 'font',
			'title' => '.EOT Font File',
		),
	),
) );

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Custom Font 2',
	'desc'       => "Upload your font files for using as font name <strong>Custom Font 2</strong>. You can also use the shortcode <strong>[customfont2]Your Text[/customfont2]</strong> in the content.</strong>",
	'id'         => 'vw-options-typography-custom-font-2',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'=>'custom_font2_ttf',
			'type' => 'media',
			'preview'=> false,
			'mode'=> 'font',
			'title' => '.TTF/.OTF Font File',
		),
		array(
			'id'=>'custom_font2_woff',
			'type' => 'media',
			'preview'=> false,
			'mode'=> 'font',
			'title' => '.WOFF Font File',
		),
		array(
			'id'=>'custom_font2_svg',
			'type' => 'media',
			'preview'=> false,
			'mode'=> 'font',
			'title' => '.SVG Font File',
		),
		array(
			'id'=>'custom_font2_eot',
			'type' => 'media',
			'preview'=> false,
			'mode'=> 'font',
			'title' => '.EOT Font File',
		),
	),
) );

/**
Logo / Favicon
 */

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Logo / Favicon',
	'desc'       => 'These are options for site logo',
	'id'         => 'vw-options-logo',
	'icon'       => 'el el-star-empty',
	'fields'     => array(
		array(
			'id' => 'site_logo',
			'type' => 'media',
			'title' => 'Origial Logo', 
			'subtitle' => 'Upload the original site logo.',
		),
		array(
			'id' => 'site_logo_2x',
			'type' => 'media',
			'title' => 'Retina Logo', 
			'subtitle' => 'The retina logo must be double size (2X) of the original logo.',
		),
		array(
			'id' => 'site_logo_margin',
			'type' => 'spacing',
			'title' => 'Logo Margin', 
			'subtitle' => 'Adjust logo margin here.',
			'mode' => 'margin',
			'units'=> array( 'em', 'px' ),
			'output' => array( '.vw-site-logo-link' ),
			'default' => array(
				'margin-top' => '0px',
				'margin-bottom' => '0px',
				'margin-left' => '0px',
				'margin-right' => '0px',
				'units' => 'px',
			),
		),
		array(
			'id'=>'logo_position',
			'type' => 'select',
			'title' => 'Logo Position', 
			'subtitle' => 'Select a position of logo to be placed on site header.',
			'options' => array(
				'left' => 'Left',
				'center' => 'Center',
			),
			'default' => 'left',
		),
	),
) );

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Navigation Logo',
	'desc'       => '',
	'id'         => 'vw-options-logo-navigation',
	'subsection' => true,
	'fields'     => array(
		array(
			'id' => 'nav_logo',
			'type' => 'media',
			'title' => 'Logo', 
			'subtitle' => 'Upload the original site logo.',
		),
		array(
			'id' => 'nav_logo_margin',
			'type' => 'spacing',
			'title' => 'Logo Margin', 
			'subtitle' => 'Adjust logo margin here.',
			'mode' => 'margin',
			'units'=> array( 'em', 'px' ),
			'output' => array( '.vw-menu-additional-logo img' ),
			'default' => array(
				'margin-top' => '0px',
				'margin-bottom' => '0px',
				'margin-left' => '0px',
				'margin-right' => '0px',
				'units' => 'px',
			),
		),
	),
) );

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Favicons',
	'desc'       => '',
	'id'         => 'vw-options-logo-favicons',
	'subsection' => true,
	'fields'     => array(
		array(
			'id' => 'fav_icon_url',
			'type' => 'media',
			'title' => 'Favicon (16x16)', 
			'subtitle' => 'Default Favicon.',
		),
		array(
			'id' => 'fav_icon_iphone_url',
			'type' => 'media',
			'title' => 'Apple iPhone Icon (57x57)', 
			'subtitle' => 'Icon for Classic iphone.',
		),
		array(
			'id' => 'fav_icon_iphone_retina_url',
			'type' => 'media',
			'title' => 'Apple iPhone Retina Icon (114x114)', 
			'subtitle' => 'Icon for Retina iPhone.',
		),
		array(
			'id' => 'fav_icon_ipad_url',
			'type' => 'media',
			'title' => 'Apple iPad Icon (72x72)', 
			'subtitle' => 'Icon for Classic iPad.',
		),
		array(
			'id' => 'fav_icon_ipad_retina_url',
			'type' => 'media',
			'title' => 'Apple iPad Retina Icon (144x144)', 
			'subtitle' => 'Icon for Retina iPad.',
		),
	),
) );

/**
Font Icons
 */

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Font Icons',
	'desc'       => 'You can choose additional icon fonts. The default font icons that are already in use are <a href="https://useiconic.com/icons/">Iconic</a> (Icon listing <a href="'.get_template_directory_uri().'/components/font-icons/iconic/demo.html">here</a>) and <a href="http://zocial.smcllns.com">Zocial</a> (Icon listing <a href="'.get_template_directory_uri().'/components/font-icons/social-icons/demo.html">here</a>).',
	'id'         => 'vw-options-font-icons',
	'icon'       => 'el el-puzzle',
	'fields'     => array(

		array(
			'id' => 'icon_elusive',
			'type' => 'switch',
			'title' => 'Include Elusive Icons', 
			'desc' => 'by <a href="http://aristeides.com">Aristeides Stathopoulos</a>, The icon listing is <a href="'.get_template_directory_uri().'/framework/font-icons/elusive/demo.html">here</a>',
			'default' => 1
		),
		array(
			'id' => 'icon_awesome',
			'type' => 'switch',
			'title' => 'Include Font Awesome Icons', 
			'desc' => 'by <a href="http://fontawesome.io">Dav Gandy</a>, The icon listing is <a href="'.get_template_directory_uri().'/framework/font-icons/awesome/demo.html">here</a>',
			'default' => 0
		),
		array(
			'id' => 'icon_iconic',
			'type' => 'switch',
			'title' => 'Include Iconic Icons', 
			'desc' => 'by <a href="http://somerandomdude.com/work/iconic">P.J. Onori</a>, The icon listing is <a href="'.get_template_directory_uri().'/framework/font-icons/iconic/demo.html">here</a>',
			'default' => 0
		),
		array(
			'id' => 'icon_typicons',
			'type' => 'switch',
			'title' => 'Include Typicons Icons', 
			'desc' => 'by <a href="http://typicons.com">Stephen Hutchings</a>, The icon listing is <a href="'.get_template_directory_uri().'/framework/font-icons/typicons/demo.html">here</a>',
			'default' => 0
		),
	),
) );

/**
Social Profiles
 */

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Social Profiles',
	'desc'       => "These are options for setting up the site's social media profiles.",
	'id'         => 'vw-options-social',
	'icon'       => 'el el-share-alt',
	'fields'     => array(

		array(
			'id' => 'social_delicious',
			'type' => 'text',
			'title' => 'Delicious URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
		),
		array(
			'id' => 'social_digg',
			'type' => 'text',
			'title' => 'Digg URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
		),
		array(
			'id' => 'social_dribbble',
			'type' => 'text',
			'title' => 'Dribbble URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
		),
		array(
			'id' => 'social_email',
			'type' => 'text',
			'title' => 'Email', 
			'subtitle' => 'Enter email address.',
			'placeholder' => 'contact@example.com',
			'validate' => 'email',
		),
		array(
			'id' => 'social_facebook',
			'type' => 'text',
			'title' => 'Facebook URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'default' => 'https://facebook.com',
			'validate' => 'url',
		),
		array(
			'id' => 'social_flickr',
			'type' => 'text',
			'title' => 'Flickr URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
		),
		array(
			'id' => 'social_forrst',
			'type' => 'text',
			'title' => 'Forrst URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
		),
		array(
			'id' => 'social_github',
			'type' => 'text',
			'title' => 'Github URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
		),
		array(
			'id' => 'social_googleplus',
			'type' => 'text',
			'title' => 'Google+ URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
			'default' => 'https://plus.google.com',
		),
		array(
			'id' => 'social_instagram',
			'type' => 'text',
			'title' => 'Instagram URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
		),
		array(
			'id' => 'social_lastfm',
			'type' => 'text',
			'title' => 'Last.fm URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
		),
		array(
			'id' => 'social_linkedin',
			'type' => 'text',
			'title' => 'Linkedin URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
		),
		array(
			'id' => 'social_pinterest',
			'type' => 'text',
			'title' => 'Pinterest URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
		),
		array(
			'id' => 'social_rss',
			'type' => 'text',
			'title' => 'Rss URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
		),
		array(
			'id' => 'social_skype',
			'type' => 'text',
			'title' => 'Skype URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
		),
		array(
			'id' => 'social_stumbleupon',
			'type' => 'text',
			'title' => 'StumbleUpon URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
		),
		array(
			'id' => 'social_tumblr',
			'type' => 'text',
			'title' => 'Tumblr URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
		),
		array(
			'id' => 'social_twitter',
			'type' => 'text',
			'title' => 'Twitter URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'default' => 'https://twitter.com',
			'validate' => 'url',
		),
		array(
			'id' => 'social_vimeo',
			'type' => 'text',
			'title' => 'Vimeo URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
		),
		array(
			'id' => 'social_yahoo',
			'type' => 'text',
			'title' => 'Yahoo URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
		),
		array(
			'id' => 'social_youtube',
			'type' => 'text',
			'title' => 'Youtube URL', 
			'subtitle' => 'Enter URL to your account page.',
			'placeholder' => 'http://',
			'validate' => 'url',
		),
	),
) );

/**
Gallery Slider
 */

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Gallery Slider',
	'desc'       => 'These are the options for the image gallery slider that is displayed in the blog entry, page composer.',
	'id'         => 'vw-options-gallery-slider',
	'icon'       => 'el el-picture',
	'fields'     => array(
		array(
			'id' => 'flexslider_slideshow',
			'type' => 'checkbox',
			'title' => 'Automatic Start', 
			'switch' => true,
			'default' => '1' // 1 = checked | 0 = unchecked
		),
		array(
			'id' => 'flexslider_randomize',
			'type' => 'checkbox',
			'title' => 'Random Order', 
			'switch' => true,
			'default' => '0' // 1 = checked | 0 = unchecked
		),
		array(
			'id' => 'flexslider_pauseonhover',
			'type' => 'checkbox',
			'title' => 'Pause On Hover', 
			'subtitle' => 'Stop playing the slider when mouse hover', 
			'switch' => true,
			'default' => '1' // 1 = checked | 0 = unchecked
		),
		array(
			'id' => 'flexslider_smooth_height',
			'type' => 'checkbox',
			'title' => 'Smooth Height', 
			'subtitle' => 'Auto adjust height of slider for every images.', 
			'switch' => true,
			'default' => '0' // 1 = checked | 0 = unchecked
		),
		array(
			'id' => 'flexslider_animation',
			'type' => 'select',
			'title' => 'Animation', 
			'subtitle' => 'Choose the animation style.',
			'default' => 'fade',
			'options' => array(
				'fade' => 'Fade',
				'slide' => 'Slide',
			),
		),
		array(
			'id' => 'flexslider_easing',
			'type' => 'select',
			'title' => 'Easing', 
			'subtitle' => 'Choose the easing of transition.',
			'default' => 'easeInCirc',
			'options' => array(
				'linear' => 'Linear',
				'easeInSine' => 'Ease In Sine',
				'easeOutSine' => 'Ease Out Sine',
				'easeInOutSine' => 'Ease In-Out Sine',
				'easeInQuad' => 'Ease In Quad',
				'easeOutQuad' => 'Ease Out Quad',
				'easeInOutQuad' => 'Ease In-Out Quad',
				'easeInCubic' => 'Ease In Cubic',
				'easeOutCubic' => 'Ease Out Cubic',
				'easeInOutCubic' => 'Ease In-Out Cubic',
				'easeInQuart' => 'Ease In Quart',
				'easeOutQuart' => 'Ease Out Quart',
				'easeInOutQuart' => 'Ease In-Out Quart',
				'easeInQuint' => 'Ease In Quint',
				'easeOutQuint' => 'Ease Out Quint',
				'easeInOutQuint' => 'Ease In-Out Quint',
				'easeInExpo' => 'Ease In Expo',
				'easeOutExpo' => 'Ease Out Expo',
				'easeInOutExpo' => 'Ease In-Out Expo',
				'easeInCirc' => 'Ease In Circ',
				'easeOutCirc' => 'Ease Out Circ',
				'easeInOutCirc' => 'Ease In-Out Circ',
				'easeInBack' => 'Ease In Back',
				'easeOutBack' => 'Ease Out Back',
				'easeInOutBack' => 'Ease In-Out Back',
				'easeInElastic' => 'Ease In Elastic',
				'easeOutElastic' => 'Ease Out Elastic',
				'easeInOutElastic' => 'Ease In-Out Elastic',
				'easeInBounce' => 'Ease In Bounce',
				'easeOutBounce' => 'Ease Out Bounce',
				'easeInOutBounce' => 'Ease In-Out Bounce',
			),
		),
		array(
			'id' => 'flexslider_slideshowspeed',
			'type' => 'text',
			'title' => 'Slideshow Duration', 
			'subtitle' => 'The time for showing slide, in milliseconds.',
			'validate' => 'numeric',
			'default' => '4000',
		),
		array(
			'id' => 'flexslider_animationspeed',
			'type' => 'text',
			'title' => 'Animation Speed', 
			'subtitle' => 'The time for transition, in milliseconds.',
			'validate' => 'numeric',
			'default' => '600',
		),
	),
) );

/**
Colors
 */
Redux::setSection( $vw_opt_name, array(
	'title'      => 'Background / Colors',
	'desc'       => 'These are options for theme colors and background.',
	'id'         => 'vw-options-background-colors',
	'icon'       => 'el el-tint',
	'fields'     => array(
		array(
			'id'=>'color_primary',
			'type' => 'color', 
			'title' => 'Primary Color',
			'subtitle'=> 'An accent color for theme.',
			'default' => '#e74c3c',
			'validate' => 'color',
		),
	),
) );

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Top Bar Colors',
	'desc'       => '',
	'id'         => 'vw-options-colors-top-bar',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'=>'color_topbar_bg',
			'type' => 'color', 
			'title' => 'Background Color',
			'default' => '#e74c3c',
			'validate' => 'color',
		),
		array(
			'id'=>'color_topbar_text',
			'type' => 'color', 
			'title' => 'Text Color',
			'default' => '#f1f1f1',
			'validate' => 'color',
		),
		array(
			'id'=>'color_topbar_hilight',
			'type' => 'color', 
			'title' => 'Hilight Color',
			'default' => '#b73839',
			'validate' => 'color',
		),
		array(
			'id'=>'color_topbar_submenu_bg',
			'type' => 'color', 
			'title' => 'Submenu Background Color',
			'default' => '#2a2a2a',
			'validate' => 'color',
		),
		array(
			'id'=>'color_topbar_submenu_text',
			'type' => 'color', 
			'title' => 'Submenu Text Color',
			'default' => '#ffffff',
			'validate' => 'color',
		),
		array(
			'id'=>'color_topbar_submenu_hilight',
			'type' => 'color', 
			'title' => 'Submenu Hilight Color',
			'default' => '#e74c3c',
			'validate' => 'color',
		),
	),
) );

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Main Menu Colors',
	'desc'       => '',
	'id'         => 'vw-options-colors-main-menu',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'=>'color_main_menu_bg',
			'type' => 'color', 
			'title' => 'Background Color',
			'default' => '#3a3a3a',
			'validate' => 'color',
		),
		array(
			'id'=>'color_main_menu_text',
			'type' => 'color', 
			'title' => 'Text Color',
			'default' => '#dddddd',
			'validate' => 'color',
		),
		array(
			'id'=>'color_main_menu_hilight',
			'type' => 'color', 
			'title' => 'Hilight Color',
			'default' => '#e74c3c',
			'validate' => 'color',
		),
		array(
			'id'=>'color_main_mega_post_bg',
			'type' => 'color', 
			'title' => 'Post Background Color',
			'default' => '#eeeeee',
			'validate' => 'color',
		),
		array(
			'id'=>'color_main_submenu_bg',
			'type' => 'color', 
			'title' => 'Submenu Background Color',
			'default' => '#2a2a2a',
			'validate' => 'color',
		),
		array(
			'id'=>'color_main_submenu_text',
			'type' => 'color', 
			'title' => 'Submenu Text Color',
			'default' => '#ffffff',
			'validate' => 'color',
		),
		array(
			'id'=>'color_main_submenu_hilight',
			'type' => 'color', 
			'title' => 'Submenu Hilight Color',
			'default' => '#e74c3c',
			'validate' => 'color',
		),
	),
) );

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Breadcrumb Colors',
	'desc'       => '',
	'id'         => 'vw-options-colors-breacrumb',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'=>'color_breadcrumb_bg',
			'type' => 'color', 
			'title' => 'Background Color',
			'default' => '#eeeeee',
			'validate' => 'color',
		),
	),
) );

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Footer Colors',
	'desc'       => '',
	'id'         => 'vw-options-colors-footer',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'=>'color_footer_bg',
			'type' => 'color', 
			'title' => 'Background Color',
			'default' => '#3a3a3a',
			'validate' => 'color',
		),
		array(
			'id'=>'color_footer_text',
			'type' => 'color', 
			'title' => 'Text Color',
			'default' => '#bbbbbb',
			'validate' => 'color',
		),
		array(
			'id'=>'color_footer_link',
			'type' => 'color', 
			'title' => 'Link Color',
			'default' => '#dddddd',
			'validate' => 'color',
		),
	),
) );

Redux::setSection( $vw_opt_name, array(
	'title'      => 'Bottom Bar Colors',
	'desc'       => '',
	'id'         => 'vw-options-colors-bottom-bar',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'=>'color_bottombar_bg',
			'type' => 'color', 
			'title' => 'Background Color',
			'default' => '#222222',
			'validate' => 'color',
		),
		array(
			'id'=>'color_bottombar_text',
			'type' => 'color', 
			'title' => 'Text Color',
			'default' => '#aaaaaa',
			'validate' => 'color',
		),
		array(
			'id'=>'color_bottombar_hilight',
			'type' => 'color', 
			'title' => 'Hilight Color',
			'default' => '#e74c3c',
			'validate' => 'color',
		),
	),
) );

/**
WooCommerce
 */

Redux::setSection( $vw_opt_name, array(
	'title'      => 'WooCommerce',
	'desc'       => 'These are options for WooCommerce. You need to install the <a href="http://wordpress.org/plugins/woocommerce/" target="_blank">WooCommerce plugin</a> before using these options.',
	'id'         => 'vw-options-woocommerce',
	'icon'       => 'el el-shopping-cart',
	'fields'     => array(
		array(
			'id'=>'woocommerce_enable_shop_sidebar',
			'type' => 'switch', 
			'title' => 'Enable Sidebar for Shop page',
			'subtitle'=> 'Turn on this option to show sidebar on shop page.',
			'default' => 0,
		),

		array(
			'id'=>'woocommerce_shop_sidebar',
			'type' => 'select',
			'title' => 'Sidebar for Shop pages', 
			'data' => 'sidebar',
			'default' => 'blog-sidebar',
		),

		array(
			'id'=>'woocommerce_products_per_page',
			'type' => 'spinner', 
			'title' => 'Products per page',
			'subtitle'=> 'The number of products displayed per page.',
			'default' => 9,
			'min' => 2,
			'max' => 50,
		),

		array(
			'id'=>'woocommerce_enable_product_sidebar',
			'type' => 'switch', 
			'title' => 'Enable Sidebar for Product page',
			'subtitle'=> 'Turn on this option to show sidebar on single product page.',
			'default' => 0,
		),

		array(
			'id'=>'woocommerce_product_sidebar',
			'type' => 'select',
			'title' => 'Sidebar for Product page', 
			'data' => 'sidebar',
			'default' => 'blog-sidebar',
		),
	),
) );

/**
bbPress
 */

Redux::setSection( $vw_opt_name, array(
	'title'      => 'bbPress',
	'desc'       => 'These are options for bbPress. You need to install the <a href="https://wordpress.org/plugins/bbpress/" target="_blank">bbPress plugin</a> before using these options.',
	'id'         => 'vw-options-bbpress',
	'icon'       => 'el el-group-alt',
	'fields'     => array(
		array(
			'id'=>'bbpress_default_sidebar',
			'type' => 'select',
			'title' => 'Sidebar', 
			'subtitle' => 'Select default sidebar.',
			'data' => 'sidebar',
			'default' => 'blog-sidebar',
		),
	),
) );

/**
buddypress
 */

Redux::setSection( $vw_opt_name, array(
	'title'      => 'BuddyPress',
	'desc'       => 'These are options for Buddypress. You need to install the <a href="https://wordpress.org/plugins/buddypress/" target="_blank">BuddyPress plugin</a> before using these options.',
	'id'         => 'vw-options-buddypress',
	'icon'       => 'el el-group-alt',
	'fields'     => array(

		array(
			'id'=>'buddypress_default_sidebar',
			'type' => 'select',
			'title' => 'Sidebar', 
			'subtitle' => 'Select default sidebar.',
			'data' => 'sidebar',
			'default' => 'blog-sidebar',
		),
	),
) );

/**
Customization
 */
Redux::setSection( $vw_opt_name, array(
	'title'      => 'Custom CSS/JS',
	'desc'       => '',
	'id'         => 'vw-options-custom-css-js',
	'icon'       => 'el el-certificate',
	'fields'     => array(
		array(
			'id'=>'custom_css',
			'type' => 'ace_editor', 
			'theme' => 'monokai',
			'mode' => 'css',
			'title' => 'Custom CSS',
			'subtitle'=> 'Paste your CSS code here.',
		),
		array(
			'id'=>'custom_js',
			'type' => 'ace_editor', 
			'theme' => 'monokai',
			'mode' => 'html',
			'title' => 'Custom JS',
			'subtitle' => 'Paste your JS code here.',
			'desc'=> 'The code <u>must</u> include <em>&lt;script&gt;</em> tag.',
		),
		array(
			'id'=>'custom_jquery',
			'type' => 'ace_editor',
			'theme' => 'monokai',
			'mode' => 'javascript',
			'title' => 'Custom jQuery',
			'subtitle' => 'Paste your jQuery code here.',
			'desc'=> 'The code <u>must not</u> include <em>&lt;script&gt;</em> tag, The code will be run on <em>$(document).ready()</em>',
		),
	),
) );

do_action( 'vw_action_init_theme_options', $vw_opt_name );

/* -----------------------------------------------------------------------------
 * Actions
 * -------------------------------------------------------------------------- */
add_action( 'redux/options/'.$vw_opt_name.'/saved', 'vw_options_saved' );
if ( ! function_exists( 'vw_options_saved' ) ) {
	function vw_options_saved() {
		if ( function_exists( 'icl_register_string' ) ) {
			$copyright_text = vw_get_theme_option( 'copyright_text' );
			icl_register_string( VW_THEME_NAME.' Copyright', strtolower(VW_THEME_NAME.'_copyright'), $copyright_text );
		}
	}
}