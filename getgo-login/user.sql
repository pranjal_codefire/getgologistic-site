CREATE TABLE `user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `interested_in` varchar(255) DEFAULT NULL,
  `transport_bill_monthly` varchar(255) DEFAULT NULL,
  `assigned_to` varchar(255) DEFAULT NULL,
  `number_of_shipments` varchar(100) DEFAULT NULL,
  `shipments_sent` int(11) DEFAULT NULL,
  `is_lead` int(1) NOT NULL DEFAULT '0',
  `zepo_user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `partner_company_name` varchar(100) DEFAULT NULL,
  `website_url` varchar(100) DEFAULT NULL,
  `address_line1` varchar(255) DEFAULT NULL COMMENT '(Address Fields can be used as Default pickup address.)',
  `address_line2` varchar(255) DEFAULT NULL,
  `landmark` varchar(255) DEFAULT NULL,
  `pincode` varchar(20) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `account_number` varchar(100) DEFAULT NULL,
  `account_holder_name` varchar(100) DEFAULT NULL,
  `account_type` varchar(100) DEFAULT NULL,
  `ifsc_code` varchar(100) DEFAULT NULL,
  `ie_code` varchar(100) DEFAULT NULL,
  `user_type` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '(0=admin,1=partner,2=retailer default NULL)',
  `partner_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT '(user id of partner)',
  `api_key` varchar(100) DEFAULT NULL,
  `api_secret_key` varchar(100) DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `partner_allowed_plan_ids` text COMMENT 'Json array of Plan.id( this is the array of plans which are allowed for Partners child user)',
  `lead_source` text,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `payment_count` int(10) UNSIGNED DEFAULT '0',
  `is_agreement_signed` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `is_postpaid` tinyint(3) UNSIGNED NOT NULL,
  `security_deposit` float(11,2) NOT NULL DEFAULT '2000.00',
  `preferred_refund_mode` int(2) DEFAULT NULL COMMENT '1-source,2-zepo cash',
  `whitelabel_status` tinyint(3) DEFAULT '0',
  `billing_cycle` tinyint(2) DEFAULT '1',
  `remittance_cycle` tinyint(2) DEFAULT '14',
  `is_calling_consignee_allowed` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `override_payment_check` tinyint(3) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

