<!doctype html>
<html data-n-head-ssr lang="en" data-n-head="lang">
<?php include 'seo.php';?>
<head>
	<title data-n-head="true"><?=$title; ?></title>
	<meta name="description" content="<?=$description; ?>" />
	<meta data-n-head="true" name="viewport"
		content="width=device-width,initial-scale=1,maximum-scale=5,minimum-scale=1,user-scalable=no,minimal-ui">
	
	<link href="img/favicon.png?v=2.2" rel="icon" type="image/png">
	<link href="css/owl.carousel.min.css?v=2.2" rel="stylesheet">
	<link href="css/owl.theme.css?v=2.2" rel="stylesheet">
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.3.1/mapbox-gl.css' rel='stylesheet' />
	<link href="css/style.css?v=2.72" rel="stylesheet">
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-143272371-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-143272371-1');
	</script>
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '1092773500916933');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=1092773500916933&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	
	<!-- Quora Pixel Code (JS Helper) -->
	<script>
	!function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
	qp('init', '659e9a2d40bb4975b4c63b3dde0aa5d2');
	qp('track', 'ViewContent');
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/659e9a2d40bb4975b4c63b3dde0aa5d2/pixel?tag=ViewContent&noscript=1"/></noscript>
	<!-- End of Quora Pixel Code -->   <script>qp('track', 'Generic');</script>
	<script type='text/javascript'>
	window.smartlook||(function(d) {
		var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
		var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
		c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
		})(document);
		smartlook('init', 'b89a3c426b938f33d5df82e14345e0d49739a5f3');
	</script>
	<meta name="p:domain_verify" content="e0336fa9d899942e98e43431a2e00ec5"/>
</head>

<body data-n-head="">
	<script src="https://unpkg.com/vue"></script>
	<script src="https://unpkg.com/zircle"></script>
	<link href="https://unpkg.com/zircle/dist/zircle.css" rel="stylesheet">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>
	<div data-server-rendered="true" id="__nuxt">
		<div id="__layout">
			<div second>
				<header class="header container-full js-header nte white" first second>
					<div class="burger" first><span first></span></div>
					<a href="./" class="logo nte nuxt-link-exact-active nuxt-link-active clickNext" first>
						<img class="blocklogo" src="img/logo.png?v=1.4" alt="" />
						<img class="hide" src="img/bluelogo.png?v=1.4" alt="" />
						<span class="visually-hidden" first></span>
					</a>
					<div class="wrapper-content nte" first>
						<nav class="nav-header nte" first>
							<ul role="navigation" first>
								<div class="wrapper-mobile" forth first>
									<li class="menu-link nte has-submenu" third forth>
										<span class="no-link" third>
											Business solutions
											<svg class="icon icon-arrow-down" third>
												<use xlink:href="#icon-arrow-down"></use>
											</svg>
										</span>
										<ul class="submenu nte" third>
											<div class="before nte" third></div>
											<div class="submenu-bridge nte" third></div>
											<div class="caret nte" third></div>
											<li class="nte" third><a href="courier" class="nte clickNext" third><span
														class="nte" third>
														Courier &amp; Parcels</span></a></li>
											<li class="nte" third><a href="cargo" class="nte clickNext" third><span
														class="nte" third>Express Cargo / Part Load</span></a></li>
											<div class="after nte" third></div>
										</ul>
									</li>
								</div>
								<li class="menu-link nte has-submenu wrapper-dashboard" third first>
									<span class="no-link" third>
										Business solutions
										<svg class="icon icon-arrow-down" third>
											<use xlink:href="#icon-arrow-down"></use>
										</svg>
									</span>
									<ul class="submenu nte" third>
										<div class="before nte" third></div>
										<div class="submenu-bridge nte" third></div>
										<div class="caret nte" third></div>
										<li class="nte" third><a href="courier" class="nte clickNext" third><span
													class="nte" third>
													Courier &amp; Parcels</span></a></li>
										<li class="nte" third><a href="cargo" class="nte clickNext" third><span
													class="nte" third>Express Cargo / Part Load</span></a></li>
										<div class="after nte" third></div>
									</ul>
								</li>
								<div class="wrapper-mobile" forth first>
									<li class="menu-link nte has-submenu" third forth>
										<span class="no-link" third><a class="nte" third>
												About us</a>
											<svg class="icon icon-arrow-down" third>
												<use xlink:href="#icon-arrow-down"></use>
											</svg>
										</span>
										<ul class="submenu nte" third>
											<div class="before nte" third></div>
											<div class="submenu-bridge nte" third></div>
											<div class="caret nte" third></div>
											<li class="nte" third><a href="about-us" class="nte clickNext" third><span
														class="nte" third>Company</span></a></li>
											<li class="nte" third><a href="career" class="nte clickNext" third><span
														class="nte" third>Career</span></a></li>
											<div class="after nte" third></div>
										</ul>
									</li>
								</div>
								<li class="menu-link nte has-submenu wrapper-dashboard" third first>
									<span class="no-link" third><a class="nte" third>
											About us</a>
										<svg class="icon icon-arrow-down" third>
											<use xlink:href="#icon-arrow-down"></use>
										</svg>
									</span>
									<ul class="submenu nte" third>
										<div class="before nte" third></div>
										<div class="submenu-bridge nte" third></div>
										<div class="caret nte" third></div>
										<li class="nte" third><a href="about-us" class="nte clickNext" third><span
													class="nte" third>Company</span></a></li>
										<li class="nte" third><a href="career" class="nte clickNext" third><span
													class="nte" third>Career</span></a></li>
										<div class="after nte" third></div>
									</ul>
								</li>
								<li class="menu-link nte has-submenu" third first>
									<span class="no-link" third>
										<a href="https://couriers.getgologistics.com/#/schedule-new-shipment" class="nte clickNext"
											third="">
											Login
										</a>
									</span>
								</li>
							</ul>
						</nav>
						<div class="wrapper-links" first>
							<div first><button class="btn primary-medium btn contactShow" first>Get Free Consultation</button>
							</div>
							<div first><a rel="noopener noreferrer" class="btn btn modalShow" first>Create free
									account</a></div>
							<div first><a href="track-shipment" class="btn primary-medium btn" first>Track
									shipment</a></div>
						</div>
					</div>
					<div class="overlay nte" first></div>
				</header>
				<div class="wrapper-contact wrapper-contact-modal hide" fifth second>
					<div class="overlay" fifth></div>
					<div class="contact-popup" fifth>
						<div class="header-contact align-center has-bg bg-primary-medium" fifth>
							<div class="succHide" fifth>
								<h3 style="margin: 0">Power your deliveries with GetGo</h3>
								<!-- <h4>Enjoy express deliveries to 20000+ pin codes across India now.</h4> -->
							</div>
							<svg class="icon icon-wave-inverted succHide" fifth>
								<use xlink:href="#icon-wave-inverted" fifth></use>
							</svg>
							<button aria-label="Close the form" role="button" type="button" class="btn-close" fifth>
								<span class="visually-hidden" fifth>Close the form</span>
								<svg class="icon icon-cross" fifth>
									<use xlink:href="#icon-cross" fifth></use>
								</svg>
							</button>
								<div class="successMsg hide">Success!<br />
									Thank you for signup.
									<div class="wrapper-submit" sixth>
										<a href="https://accounts.getgologistics.com/auth/login" style="width: 150px"
											class="btn btn-arrow" sixth>
											<span sixth>Login</span>
											<svg class="icon icon-arrow-right" sixth>
												<use xlink:href="#icon-arrow-right" sixth></use>
											</svg>
										</a>
									</div>
								</div>
						</div>
						<div id="createUserFormDiv" class="content-contact succHide" fifth>
							<div sixth fifth>
								<p class="errorFr errorFrTop" id="createUserFormError"></p>
								<form action="https://accounts.getgologistics.com/auth/lead/create" name="createUserForm" id="createUserForm" method="POST"
									role="form" class="form form--contact-us" sixth modelAttribute="leadRequest">
									<div class="field double" sixth><label for="name" class="required" seven
											sixth><input name="name" id="name" placeholder="Full name" required
												type="text" seven>
												<span class="errorFr" id="nameError"></span>
										</label>
										<label for="email" class="required" seven sixth><input name="email" id="email"
												required type="email" placeholder="Email id" seven>
												<span class="errorFr" id="emailError"></span>
										</label>
									</div>
									<div class="field double" sixth><label for="password" class="required" seven
											sixth><input name="password" id="password" placeholder="Create new password"
												required type="password" seven><span class="errorFr" id="passwordError"></span>
										</label>
										<label for="companyType" class="select" seven sixth>
											<select aria-label="companyType" name="companyType" id="companyType" seven>
												<option disabled hidden selected seven>Company type</option>
												<option value="Sole_Propreitorship" seven>
													Sole Propreitorship
												</option>
												<option value="Pvt_Ltd" seven>
													Pvt Ltd
												</option>
												<option value="Partnership" seven>
													Partnership
												</option>
												<option value="LLP" seven>
													LLP
												</option>
												<option value="Not_registered" seven>
													Not registered
												</option>
											</select>
											<span class="errorFr" id="companyTypeError"></span>
										</label>
										</label>
									</div>
									<div class="field double" sixth>
										<label for="Phone" class="required" seven sixth><input name="contact_number"
												id="Phone" placeholder="Contact number" required type="tel"
												seven><span class="errorFr" id="contact_numberError"></span></label>
												<label for="company" class="required" seven
											sixth><input name="company_name" id="company" placeholder="Company name"
												required type="text" seven><span class="errorFr" id="company_nameError"></span></label>
										
									</div>
									<!-- <div class="field double" sixth><label for="Town__c" class="required" seven sixth><input name="city"
												id="Town__c" placeholder="Enter your city" required type="text"
												seven><span class="errorFr" id="cityError"></span></label>
										<label for="current_logistics_partner" class="required" seven sixth><input
												name="current_logistics_partner" id="current_logistics_partner"
												placeholder="Current logistics partner" required type="text"
												seven><span class="errorFr" id="current_logistics_partnerError"></span></label>
									</div> -->
									<div class="field" sixth>
										<label for="Dayly__c" seven sixth>
											<div class="dropdown">
												<label class="dropdown-label">Interested in</label>
												<div class="dropdown-list">
													<div class="checkbox">
														<input type="checkbox" onchange="valueChanged()" value="courier"
															name="interested_in" class="check checkbox-custom"
															id="checkbox01" />
														<label for="checkbox01" class="checkbox-custom-label">Courier
															(500 gms to 25 kg)</label>
													</div>
													<div class="checkbox">
														<input type="checkbox" onchange="Express_Cargo()"
															value="Express_Cargo" name="interested_in"
															class="check checkbox-custom" id="checkbox02" />
														<label for="checkbox02" class="checkbox-custom-label">Express
															cargo (25 kg to 5 tonnes)</label>
													</div>
												</div>
											</div>
										</label>
									</div>
									<div class="field interest" sixth id="Courier">
										<label for="courier_number_of_shipments" class="select" seven sixth>
											<select aria-label="Dayly__c" name="number_of_shipments"
												id="courier_number_of_shipments" seven>
												<option disabled hidden selected seven>
													Current number of couriers per day
												</option>
												<option value="Less than 10 per day" seven>
													Less than 10 per day
												</option>
												<option value="10 to 100 per day" seven>
													10 to 100 per day
												</option>
												<option value="100 to 1000 per day" seven>
													100 to 1000 per day
												</option>
												<option value="More than 1000 per day" seven>
													More than 1000 per day
												</option>
											</select>
										</label>
									</div>
									<div class="field interest" sixth id="Express_Cargo">
										<label for="movement_per_month" class="select" seven sixth>
											<select aria-label="movement_per_month" name="transportBillMonthly"
												id="movement_per_month" seven>
												<option disabled hidden selected seven>Current movement per month in
													cargo</option>
												<option value="less than 1 tonne per month" seven>
													Less than 1 tonne per month
												</option>
												<option value="1 tonne to 10 tonnes per month" seven>
													1 tonne to 10 tonnes per month
												</option>
												<option value="10 tonne to 100 tonnes per month" seven>
													10 tonne to 100 tonnes per month
												</option>
												<option value="more than 100 tonnes per month" seven>
													More than 100 tonnes per month
												</option>
											</select>
										</label>
									</div>
									<!-- <div class="field" sixth><label for="description" seven sixth><textarea name="description"
												id="description" seven></textarea> <span class="label-txt" seven>Description</span></label>
									</div> -->
									<!-- <div class="field check" sixth>
										<label sixth>
											<input type="checkbox" required sixth>
											<div sixth>
												<p>By submitting this form I confirm that I have read and understood the
													<a href="privacy" target="_blank" rel="noopener"
														title="Privacy Policy">Privacy
														Policy</a> and that the GetGo sales team can reach out to me by
													using the
													information I provided above.</p>
											</div>
										</label>
									</div> -->
									<div class="grecaptcha-badge" sixth></div>
									<div class="wrapper-submit" sixth>
										<button type="submit" class="btn huge full-width btn-arrow loadingBtn" sixth>
											<span sixth>Create account now</span>
											<svg class="icon icon-arrow-right" sixth>
												<use xlink:href="#icon-arrow-right" sixth></use>
											</svg>
										</button>
									</div>
									<!---->
								</form>
							</div>
						</div>
						<!---->
					</div>
				</div>
