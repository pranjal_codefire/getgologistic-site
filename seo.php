<?php

$seo = array(
        "HomePage" => array( 
					"Title" => "Logistics Solution | Lowest Shipping Rate | GetGo Logistics", 
					"Description" => "Is your business bearing the brunt of your logistics partner? Log in to partner with one of
          the best logistics solution provider in India and get the lowest shipping rates.", 
					 ), 
          
    
		"Couriers & parcel page" => array( 
			                       "Title" =>"Widest Coverage Courier Company | Lowest Courier Rates" , 
			                       "Description" => "Looking for a courier company with the lowest courier rates and a wide coverage? We offer you low 
                             courier rates and proudly define ourselves as the widest coverage courier company.", 
			 ), 
      
     
       "Express cargo & part load" => array( 
          
                                    "Title" => "Express Cargo Logistics | Packers and Movers | Part Load", 
                                    "Description" => "Find safe and affordable packers and movers to relocate.  Also, whether you have full load or part loads, we are 
                                    the one stop solution for express cargo logistics in India.", 
        
                                    ), 
    
       "about-us" => array( 
          
                   "Title" => "Ecommerce Shipping and Logistics Solutions | Reverse Logistics", 
                   "Description" => "GetGo Logistics defines itself as an ecommerce shipping and logistics solutions provider. From providing NDR management, 
                   COD services to reverse logistics, we have it all.", 
       
                   ),
       "Careers page" => array( 
          
                      "Title" => "Careers | GetGo Logistics", 
                      "Description" => "Learn more about job and career opportunities at GetGo Logistics. Search our current
                      openings today to find the best fit for you and your career goals.", 
        
                      ),
); 

$URI = $_SERVER['REQUEST_URI'];
$array = explode('/', $URI);
$URL= end($array);
switch ($URL){
case 'about-us':
 $title= $seo['about-us']['Title'];
 $description = $seo['about-us']['Description'];
 break;
 
case 'career':
 $title= $seo['Careers page']['Title'];
 $description = $seo['Careers Page']['Description'];
 break;
 
case 'courier':
 $title= $seo['Couriers & parcel page']['Title'];
 $description = $seo['Couriers & parcel page']['Description'];
 break;
 
case 'cargo':
 $title= $seo['Express cargo & part load']['Title'];
 $description = $seo['Express cargo & part load']['Description'];
 break;
 
default:
  $title= $seo['HomePage']['Title'];
  $description = $seo['HomePage']['Description'];
 
}


?>