<?php include("header.php"); ?>
<style>
  .mod.medium-padding {
    padding-top: 150px;
    padding-bottom: 150px;
  }

  .hero[data-v-46aa99ba] {
    display: flex;
    align-items: center;
    position: relative;
    padding: 50px 0;
    z-index: 1;
    min-height: calc(100vh - 0px)
  }

  .hero h1[data-v-46aa99ba] {
    color: #fff
  }

  .hero.half .container[data-v-46aa99ba] {
    display: flex;
    flex-direction: row-reverse;
    align-items: center;
    justify-content: space-between
  }

  .hero.half .wrapper-text[data-v-46aa99ba] {
    flex: 0 0 auto;
    width: calc(50% - 25px)
  }

  .hero.half .wrapper-illus[data-v-46aa99ba] {
    flex: 0 1 auto;
    margin-right: 25px;
    width: calc(50vw - 100px);
    max-width: 750px
  }

  .hero .wrapper-illus[data-v-46aa99ba] {
    position: relative
  }

  .hero .subtitle[data-v-46aa99ba] {
    margin-bottom: 2.5em;
    font-size: 2rem;
    max-width: 425px
  }

  @-webkit-keyframes float-data-v-46aa99ba {
    0% {
      -webkit-transform: translatey(0);
      transform: translatey(0)
    }

    50% {
      -webkit-transform: translatey(-10px);
      transform: translatey(-10px)
    }

    to {
      -webkit-transform: translatey(0);
      transform: translatey(0)
    }
  }

  @keyframes float-data-v-46aa99ba {
    0% {
      -webkit-transform: translatey(0);
      transform: translatey(0)
    }

    50% {
      -webkit-transform: translatey(-10px);
      transform: translatey(-10px)
    }

    to {
      -webkit-transform: translatey(0);
      transform: translatey(0)
    }
  }

  @-webkit-keyframes floatlarge-data-v-46aa99ba {
    0% {
      -webkit-transform: translatey(0);
      transform: translatey(0)
    }

    50% {
      -webkit-transform: translatey(-20px);
      transform: translatey(-20px)
    }

    to {
      -webkit-transform: translatey(0);
      transform: translatey(0)
    }
  }

  @keyframes floatlarge-data-v-46aa99ba {
    0% {
      -webkit-transform: translatey(0);
      transform: translatey(0)
    }

    50% {
      -webkit-transform: translatey(-20px);
      transform: translatey(-20px)
    }

    to {
      -webkit-transform: translatey(0);
      transform: translatey(0)
    }
  }

  .cloud[data-v-46aa99ba] {
    position: absolute;
    -webkit-animation: float-data-v-46aa99ba 5s ease-in-out infinite;
    animation: float-data-v-46aa99ba 5s ease-in-out infinite
  }

  .cloud.left[data-v-46aa99ba] {
    top: 23%;
    left: -5%;
    width: 15%;
    -webkit-animation: float-data-v-46aa99ba 6s ease-in-out infinite;
    animation: float-data-v-46aa99ba 6s ease-in-out infinite
  }

  .cloud.right[data-v-46aa99ba] {
    top: 27%;
    right: -2%;
    width: 10%;
    -webkit-animation: floatlarge-data-v-46aa99ba 5s ease-in-out 2s infinite;
    animation: floatlarge-data-v-46aa99ba 5s ease-in-out 2s infinite
  }

  @media (max-width:1100px) {

    .hero.half .wrapper-illus[data-v-46aa99ba],
    .hero.half .wrapper-text[data-v-46aa99ba] {
      width: calc(50% - 25px)
    }

    .hero.half .wrapper-illus[data-v-46aa99ba] {
      margin-left: 0
    }
  }

  @media (max-width:780px) {
    .hero[data-v-46aa99ba] {
      padding: 100px 0 50px
    }

    .hero.half .container[data-v-46aa99ba] {
      flex-wrap: wrap;
      justify-content: center
    }

    .hero.half .wrapper-illus[data-v-46aa99ba],
    .hero.half .wrapper-text[data-v-46aa99ba] {
      width: 100%
    }

    .hero.half .wrapper-illus[data-v-46aa99ba] {
      max-width: 400px;
      margin-top: 20px
    }
  }

  .wrapper-headband[data-v-0fefbdef] {
    position: absolute;
    width: 100%;
    height: 41px;
    left: 0;
    z-index: 2
  }

  .wrapper-headband.headband-top .mouse[data-v-0fefbdef],
  .wrapper-headband.headband-top[data-v-0fefbdef] {
    top: 0
  }

  .wrapper-headband.headband-top .icon-quotes[data-v-0fefbdef] {
    top: 5px
  }

  .wrapper-headband.headband-bottom[data-v-0fefbdef] {
    bottom: 0
  }

  .wrapper-headband.headband-bottom .icon-wave-inverted[data-v-0fefbdef],
  .wrapper-headband.headband-bottom .icon-wave[data-v-0fefbdef] {
    top: 1px;
    -webkit-transform: rotate(180deg);
    transform: rotate(180deg)
  }

  .wrapper-headband.headband-bottom .mouse[data-v-0fefbdef] {
    bottom: 0
  }

  .wrapper-headband.headband-bottom .icon-quotes[data-v-0fefbdef] {
    bottom: 5px
  }

  .wrapper-headband.bg-grey-light[data-v-0fefbdef],
  .wrapper-headband.bg-primary-dark[data-v-0fefbdef],
  .wrapper-headband.bg-primary-medium[data-v-0fefbdef],
  .wrapper-headband.bg-primary[data-v-0fefbdef],
  .wrapper-headband.bg-secondary[data-v-0fefbdef] {
    background: 0 0
  }

  .wrapper-headband.bg-primary-dark .headband[data-v-0fefbdef] {
    background-color: #0d3b60
  }

  .wrapper-headband.bg-primary-dark .icon-wave[data-v-0fefbdef] {
    fill: #0d3b60
  }

  .wrapper-headband.bg-primary .headband[data-v-0fefbdef] {
    background-color: #11a3eb
  }

  .wrapper-headband.bg-primary .icon-wave[data-v-0fefbdef] {
    fill: #11a3eb
  }

  .wrapper-headband.bg-grey-light .headband[data-v-0fefbdef] {
    background: #f8fbff
  }

  .wrapper-headband.bg-grey-light .icon-wave[data-v-0fefbdef] {
    fill: #f8fbff
  }

  .wrapper-headband.bg-primary-medium .headband[data-v-0fefbdef] {
    background: #105285
  }

  .wrapper-headband.bg-primary-medium .icon-wave[data-v-0fefbdef] {
    fill: #105285
  }

  .wrapper-headband.bg-secondary .headband[data-v-0fefbdef] {
    background: #d2eefc
  }

  .wrapper-headband.bg-secondary .icon-wave[data-v-0fefbdef] {
    fill: #d2eefc
  }

  .headband[data-v-0fefbdef] {
    display: block;
    position: absolute;
    width: calc(50% - 49px);
    height: 100%
  }

  .headband.left[data-v-0fefbdef] {
    left: 0
  }

  .headband.right[data-v-0fefbdef] {
    right: 0
  }

  .icon-wave-inverted[data-v-0fefbdef],
  .icon-wave[data-v-0fefbdef] {
    width: 100px;
    height: 40px;
    position: absolute;
    top: 0;
    left: 50%;
    margin-left: -50px
  }

  .icon-wave-inverted[data-v-0fefbdef] {
    fill: #fff
  }

  .mouse[data-v-0fefbdef] {
    width: 18px;
    height: 26px;
    margin-left: -9px;
    color: #11a3eb;
    border: 2px solid;
    border-radius: 9px
  }

  .mouse[data-v-0fefbdef],
  .mouse[data-v-0fefbdef]:before {
    position: absolute;
    left: 50%
  }

  .mouse[data-v-0fefbdef]:before {
    content: "";
    display: block;
    width: 2px;
    height: 4px;
    top: 5px;
    margin-left: -1px;
    background: currentColor;
    border-radius: 1px;
    -webkit-animation-name: scroll-data-v-0fefbdef;
    animation-name: scroll-data-v-0fefbdef;
    -webkit-animation-duration: 4s;
    animation-duration: 4s;
    -webkit-animation-delay: 5s;
    animation-delay: 5s;
    -webkit-animation-timing-function: ease-in-out;
    animation-timing-function: ease-in-out;
    -webkit-animation-direction: normal;
    animation-direction: normal;
    -webkit-animation-iteration-count: infinite;
    animation-iteration-count: infinite;
    -webkit-animation-play-state: running;
    animation-play-state: running
  }

  .back-to-top-button[data-v-0fefbdef] {
    position: absolute;
    left: 50%;
    margin-left: -10px
  }

  .back-to-top[data-v-0fefbdef] {
    position: relative;
    display: block;
    height: 27px;
    width: 20px;
    -webkit-transform-origin: 50% 0;
    transform-origin: 50% 0;
    color: #105285
  }

  .back-to-top>span[data-v-0fefbdef] {
    position: absolute;
    display: block;
    box-sizing: border-box
  }

  .back-to-top .point[data-v-0fefbdef] {
    width: 6px;
    height: 6px;
    background-color: currentColor;
    border-radius: 50%;
    -webkit-transform-origin: 50% 50%;
    transform-origin: 50% 50%;
    top: 0;
    left: 7px
  }

  .back-to-top .left[data-v-0fefbdef] {
    -webkit-transform-origin: 100% 50%;
    transform-origin: 100% 50%;
    -webkit-transform: rotate(-45deg) scaleX(.1);
    transform: rotate(-45deg) scaleX(.1);
    left: 0
  }

  .back-to-top .left[data-v-0fefbdef],
  .back-to-top .right[data-v-0fefbdef] {
    width: 10px;
    height: 2px;
    background-color: currentColor;
    border-radius: 50px;
    top: 0
  }

  .back-to-top .right[data-v-0fefbdef] {
    -webkit-transform-origin: 0 50%;
    transform-origin: 0 50%;
    -webkit-transform: rotate(45deg) scaleX(.1);
    transform: rotate(45deg) scaleX(.1);
    right: 0
  }

  .back-to-top .bottom[data-v-0fefbdef] {
    height: 15px;
    width: 2px;
    background-color: currentColor;
    top: 2px;
    left: calc(50% - 1px);
    border-radius: 5px;
    -webkit-transform-origin: 50% 0;
    transform-origin: 50% 0;
    -webkit-transform: scaleY(0);
    transform: scaleY(0)
  }

  .icon-quotes[data-v-0fefbdef] {
    width: 22px;
    height: 19px;
    position: absolute;
    left: 50%;
    margin-left: -11px;
    fill: #11a3eb
  }

  @-webkit-keyframes scroll-data-v-0fefbdef {
    0% {
      -webkit-transform: translateZ(0) scaleY(1);
      transform: translateZ(0) scaleY(1)
    }

    6.25% {
      -webkit-transform: translate3d(0, 100%, 0) scaleY(2);
      transform: translate3d(0, 100%, 0) scaleY(2)
    }

    12.5% {
      -webkit-transform: translate3d(0, 200%, 0) scaleY(1);
      transform: translate3d(0, 200%, 0) scaleY(1)
    }

    18.75% {
      -webkit-transform: translate3d(0, 100%, 0) scaleY(2);
      transform: translate3d(0, 100%, 0) scaleY(2)
    }

    25% {
      -webkit-transform: translateZ(0) scaleY(1);
      transform: translateZ(0) scaleY(1)
    }
  }

  @keyframes scroll-data-v-0fefbdef {
    0% {
      -webkit-transform: translateZ(0) scaleY(1);
      transform: translateZ(0) scaleY(1)
    }

    6.25% {
      -webkit-transform: translate3d(0, 100%, 0) scaleY(2);
      transform: translate3d(0, 100%, 0) scaleY(2)
    }

    12.5% {
      -webkit-transform: translate3d(0, 200%, 0) scaleY(1);
      transform: translate3d(0, 200%, 0) scaleY(1)
    }

    18.75% {
      -webkit-transform: translate3d(0, 100%, 0) scaleY(2);
      transform: translate3d(0, 100%, 0) scaleY(2)
    }

    25% {
      -webkit-transform: translateZ(0) scaleY(1);
      transform: translateZ(0) scaleY(1)
    }
  }

  .wrapper-block-rounded[data-v-3d37c500] {
    background-size: cover;
    background-position: 50%
  }

  .block-rounded[data-v-3d37c500] {
    position: relative;
    padding: 80px 120px 100px;
    text-align: center;
    background: #fff;
    color: #a1a6ac;
    border-radius: 60px;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden
  }

  .block-rounded[data-v-3d37c500]:after,
  .block-rounded[data-v-3d37c500]:before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    border-radius: 60px
  }

  .block-rounded[data-v-3d37c500]:before {
    background-color: #fff;
    z-index: -1
  }

  .block-rounded[data-v-3d37c500]:after {
    box-shadow: 0 21px 155px rgba(16, 82, 133, .1);
    z-index: -2
  }

  .block-rounded[data-v-3d37c500] p {
    max-width: 800px;
    margin-left: auto;
    margin-right: auto;
    font-size: 1.9rem;
    text-align: justify;
    text-align-last: center;
  }

  .bg-half[data-v-3d37c500] {
    position: absolute;
    top: -1px;
    bottom: 50%;
    left: 0;
    width: 100%
  }

  .rounded-columns[data-v-3d37c500] {
    display: flex;
    justify-content: space-between;
    margin: 7rem 0 0
  }

  .rounded-columns>li[data-v-3d37c500] {
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 28%;
    list-style-type: none
  }

  .rounded-columns p[data-v-3d37c500] {
    width: 100%;
    font-size: 1.6rem
  }

  .wrapper-column-img[data-v-3d37c500] {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: 80px
  }

  .wrapper-column-img>img[data-v-3d37c500] {
    max-height: 100%
  }

  .block-rounded-out[data-v-3d37c500] {
    -webkit-transform: scale(.99);
    transform: scale(.99);
    transition: .3s ease-out
  }

  .block-rounded-out[data-v-3d37c500]:after {
    -webkit-transform: scale(.8);
    transform: scale(.8);
    transition: .3s ease-out
  }

  .block-rounded-in[data-v-3d37c500],
  .block-rounded-in[data-v-3d37c500]:after {
    -webkit-transform: scale(1);
    transform: scale(1);
    transition: .3s ease-out
  }

  @media (max-width:1400px) {
    .block-rounded[data-v-3d37c500] {
      padding: 80px 100px 100px
    }
  }

  @media (max-width:1100px) {
    .block-rounded[data-v-3d37c500] {
      padding: 60px 80px 80px
    }

    .block-rounded[data-v-3d37c500] p {
      font-size: 1.8rem
    }
  }

  @media (max-width:960px) {
    .block-rounded[data-v-3d37c500] {
      padding: 50px 70px 70px
    }

    .block-rounded[data-v-3d37c500],
    .block-rounded[data-v-3d37c500]:after,
    .block-rounded[data-v-3d37c500]:before {
      border-radius: 50px
    }

    .block-rounded[data-v-3d37c500] p {
      font-size: 1.6rem
    }
  }

  @media (max-width:780px) {
    .block-rounded[data-v-3d37c500] {
      padding: 40px 60px 60px
    }

    .block-rounded[data-v-3d37c500],
    .block-rounded[data-v-3d37c500]:after,
    .block-rounded[data-v-3d37c500]:before {
      border-radius: 40px
    }

    .rounded-columns[data-v-3d37c500] {
      align-items: center;
      flex-direction: column
    }

    .rounded-columns>li[data-v-3d37c500] {
      width: 100%;
      max-width: 300px;
      margin: 0 0 50px
    }

    .rounded-columns>li[data-v-3d37c500]:last-child {
      margin: 0
    }
  }

  @media (max-width:580px) {
    .block-rounded[data-v-3d37c500] {
      padding: 30px 40px 40px
    }

    .block-rounded[data-v-3d37c500],
    .block-rounded[data-v-3d37c500]:after,
    .block-rounded[data-v-3d37c500]:before {
      border-radius: 30px
    }
  }

  @media (max-width:400px) {
    .block-rounded[data-v-3d37c500] {
      padding: 30px 30px 40px
    }

    .block-rounded[data-v-3d37c500],
    .block-rounded[data-v-3d37c500]:after,
    .block-rounded[data-v-3d37c500]:before {
      border-radius: 20px
    }
  }

  .module-cta-block+.module-fifty[data-v-5ba14530] .mod {
    padding-top: 50px
  }

  .module-rounded-toggle+.module-steps-timeline[data-v-5ba14530] .mod {
    padding-top: 100px
  }

  .module-rounded-card+.module-fifty[data-v-5ba14530] .mod {
    padding-top: 80px
  }

  @media (max-width:960px) {
    .module-cta-block+.module-fifty[data-v-5ba14530] .mod {
      padding-top: 30px
    }

    .module-rounded-toggle+.module-steps-timeline[data-v-5ba14530] .mod {
      padding-top: 0
    }

    .module-rounded-card+.module-fifty[data-v-5ba14530] .mod {
      padding-top: 50px
    }
  }

  @media (max-width:780px) {
    .module-cta-block+.module-fifty[data-v-5ba14530] .mod {
      padding-top: 0
    }
  }

  .wrapper-row[data-v-d7eeecfc] {
    position: relative;
    height: 95px
  }

  .wrapper-row.has-three-lines[data-v-d7eeecfc] {
    height: 492px
  }

  .wrapper-row.has-three-lines .wrapper-logos[data-v-d7eeecfc] {
    flex-wrap: wrap
  }

  .wrapper-row.has-three-lines .wrapper-logos>li[data-v-d7eeecfc] {
    margin: 42px 0
  }

  .wrapper-logos[data-v-d7eeecfc] {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    list-style-type: none
  }

  .wrapper-logos>li[data-v-d7eeecfc],
  .wrapper-logos[data-v-d7eeecfc] {
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 15px 0
  }

  .wrapper-logos>li[data-v-d7eeecfc] {
    flex: 1 1 25%;
    position: relative;
    height: 70px;
    padding: 0 20px;
    display: none;
  }

  .wrapper-logos img[data-v-d7eeecfc] {
    max-height: 100%;
    max-width: 140px;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    will-change: transform
  }

  .wrapper-logos img.is-animated[data-v-d7eeecfc] {
    opacity: 0;
    -webkit-transform: translateZ(0) scale(.9);
    transform: translateZ(0) scale(.9)
  }

  .wrapper-logos>li[data-v-d7eeecfc]:nth-child(1) img,
  .wrapper-logos>li[data-v-d7eeecfc]:nth-child(2) img,
  .wrapper-logos>li[data-v-d7eeecfc]:nth-child(3) img,
  .wrapper-logos>li[data-v-d7eeecfc]:nth-child(4) img,
  .wrapper-logos>li[data-v-d7eeecfc]:nth-child(5) img {
    display: block;
    opacity: 1;
    margin: 0 auto;
  }

  @media (max-width:960px) {
    .wrapper-row.has-three-lines[data-v-d7eeecfc] {
      height: 338px
    }

    .wrapper-row.has-three-lines .wrapper-logos>li[data-v-d7eeecfc]:nth-child(n+9) {
      display: none
    }
  }

  @media (max-width:780px) {
    .wrapper-row.has-three-lines[data-v-d7eeecfc] {
      height: 95px
    }

    .wrapper-row.has-three-lines .wrapper-logos>li[data-v-d7eeecfc] {
      margin: 0
    }

    .wrapper-logos[data-v-d7eeecfc] {
      flex-wrap: wrap
    }

    .wrapper-logos>li[data-v-d7eeecfc] {
      flex: 0 0 33.3333%;
      margin: 0;
      padding: 0 15px
    }

    .wrapper-logos>li[data-v-d7eeecfc]:nth-child(n+4) {
      display: none
    }
  }

  .wrapper-big-list[data-v-ef0c82cc] {
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    align-items: stretch;
    color: #a1a6ac;
    padding-bottom: 0px;
  }

  .wrapper-big-list.three-cols .intro[data-v-ef0c82cc] {
    padding-left: 50px;
    padding-right: 50px
  }

  .wrapper-big-list.three-cols .big-list[data-v-ef0c82cc] {
    padding-left: 0
  }

  .big-list[data-v-ef0c82cc],
  .intro[data-v-ef0c82cc] {
    flex: 0 0 auto
  }

  .intro[data-v-ef0c82cc] {
    width: 33.3333%;
    padding-right: 100px
  }

  .below-list[data-v-ef0c82cc] {
    width: 100%
  }

  .below-list .list-line[data-v-ef0c82cc] {
    flex-wrap: wrap
  }

  .below-list .list-item[data-v-ef0c82cc] {
    width: 33.3333%
  }

  .big-list[data-v-ef0c82cc] {
    flex-direction: column;
    justify-content: space-between;
    list-style-type: none;
    width: 66.6666%;
    margin: 0;
    padding-left: 50px
  }

  .big-list[data-v-ef0c82cc],
  .list-item[data-v-ef0c82cc],
  .list-line[data-v-ef0c82cc] {
    display: flex
  }

  .list-item[data-v-ef0c82cc] {
    position: relative;
    align-items: flex-start;
    flex-direction: column;
    width: 50%;
    margin: 1em 0;
    padding: 0 50px
  }

  .list-item.has-title .content-item[data-v-ef0c82cc]:before {
    top: 0
  }

  .list-item .content-item[data-v-ef0c82cc] {
    position: relative
  }

  .list-item .content-item[data-v-ef0c82cc]:before {
    content: "0"attr(index);
    display: block;
    position: absolute;
    top: 3px;
    right: calc(100% + 15px);
    font-size: 1.6rem;
    font-weight: 700;
    line-height: 1;
    color: #11a3eb
  }

  .list-item .title[data-v-ef0c82cc] {
    position: relative;
    display: flex;
    align-items: flex-end;
    text-transform: uppercase;
    font-family: gilroy, sans-serif;
    font-size: 1.2rem;
    font-weight: 700;
    letter-spacing: .8px;
    color: #11a3eb
  }

  .list-item .title+.list-text[data-v-ef0c82cc] {
    margin-top: 10px
  }

  .image-item[data-v-ef0c82cc] {
    margin-right: 22px
  }

  .image-item+.content-item[data-v-ef0c82cc] {
    margin-top: 10px
  }

  .list-text[data-v-ef0c82cc] {
    display: block;
    width: 100%;
    text-align: justify;
  }

  .list-text[data-v-ef0c82cc] h5 {
    color: #646a71;
    margin-top: 0
  }

  .list-text[data-v-ef0c82cc] p {
    margin: 0
  }

  @media (max-width:1400px) {
    .intro[data-v-ef0c82cc] {
      padding-right: 50px
    }

    .big-list[data-v-ef0c82cc] {
      padding-left: 0
    }
  }

  @media (max-width:1100px) {
    .wrapper-big-list.three-cols .intro[data-v-ef0c82cc] {
      padding: 0 10px
    }

    .list-item[data-v-ef0c82cc] {
      display: block;
      margin: 0;
      padding: 15px 10px
    }

    .list-item .content-item[data-v-ef0c82cc]:before {
      position: static;
      margin-bottom: 5px
    }

    .list-item .title[data-v-ef0c82cc] {
      align-items: flex-start;
      display: flex;
      flex-direction: column;
      margin-bottom: 15px
    }

    .image-item[data-v-ef0c82cc] {
      display: block;
      margin: 0 0 15px
    }
  }

  @media (max-width:960px) {
    .wrapper-big-list[data-v-ef0c82cc] {
      display: block
    }

    .intro[data-v-ef0c82cc] {
      width: 100%;
      margin-bottom: 30px;
      padding: 0
    }

    .big-list[data-v-ef0c82cc] {
      width: 100%
    }

    .big-list[data-v-ef0c82cc],
    .list-line[data-v-ef0c82cc] {
      justify-content: space-between
    }

    .below-list .list-item[data-v-ef0c82cc],
    .list-item[data-v-ef0c82cc] {
      width: calc(50% - 25px)
    }
  }

  @media (max-width:580px) {
    .big-list>li[data-v-ef0c82cc]:nth-last-child(2) {
      margin-bottom: 30px
    }

    .list-line[data-v-ef0c82cc] {
      display: block
    }

    .below-list .list-item[data-v-ef0c82cc],
    .list-item[data-v-ef0c82cc] {
      width: 100%
    }

    .image-item[data-v-ef0c82cc] {
      margin: 0 0 10px
    }
  }

  .mod-toggle[data-v-5254fdbf]:before {
    content: "";
    display: block;
    position: absolute;
    top: 401px;
    bottom: 0;
    left: 0;
    right: 0;
    background: #f5fbfe
  }

  .wrapper-toggle[data-v-5254fdbf] {
    text-align: center
  }

  .wrapper-wave[data-v-5254fdbf] {
    position: absolute;
    top: 400px;
    left: 0;
    width: 100%;
    overflow: hidden
  }

  .wrapper-wave[data-v-5254fdbf]:before {
    content: "";
    display: block;
    position: absolute;
    top: 1px;
    bottom: 0;
    left: 0;
    right: 0;
    background: #f5fbfe
  }

  .wrapper-wave .wave[data-v-5254fdbf] {
    display: block;
    position: relative;
    width: 102%;
    left: -1%
  }

  .wrapper-wave .wave[data-v-5254fdbf]:before {
    content: "";
    display: block;
    padding-bottom: 15%
  }

  .wrapper-wave .icon[data-v-5254fdbf] {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    fill: #fff
  }

  .toggles[data-v-5254fdbf] {
    display: flex;
    align-items: flex-start;
    justify-content: center;
    flex-wrap: wrap;
    margin: 100px 0 0;
  }

  .toggle[data-v-5254fdbf] {
    position: relative;
    width: calc(25% - 50px);
    margin: 0 25px;
    text-align: center;
    border-radius: 35px;
    background: #fff;
    box-shadow: 0 11px 43px rgba(16, 82, 133, .16);
    transition: 0.3s all ease-in-out;
  }

  .toggle.open .btn-toggle-icon[data-v-5254fdbf] {
    background: #a1a6ac
  }

  .toggle.open .btn-toggle-icon[data-v-5254fdbf]:after {
    opacity: 0
  }

  .btn-toggle[data-v-5254fdbf] {
    display: block;
    width: 100%;
    padding: 25px;
    text-transform: uppercase;
    font-family: gilroy, sans-serif;
    font-weight: 600;
    font-size: 1.6rem;
    line-height: 1.1;
    color: #11a3eb
  }

  .btn-toggle-icon[data-v-5254fdbf] {
    position: absolute;
    bottom: -17px;
    left: 50%;
    display: block;
    width: 34px;
    height: 34px;
    margin-left: -17px;
    border-radius: 50%;
    background: #5eca4f;
    z-index: 3;
  }

  .btn-toggle-icon[data-v-5254fdbf]:after,
  .btn-toggle-icon[data-v-5254fdbf]:before {
    content: "";
    position: absolute;
    top: 50%;
    left: 50%;
    background: #fff
  }

  .btn-toggle-icon[data-v-5254fdbf]:before {
    width: 8px;
    height: 2px;
    margin: -1px 0 0 -4px
  }

  .btn-toggle-icon[data-v-5254fdbf]:after {
    width: 2px;
    height: 8px;
    margin: -4px 0 0 -1px;
    opacity: 1
  }

  .toggle-content[data-v-5254fdbf] {
    color: #a1a6ac;
    overflow: hidden;
    opacity: 0;
  }

  .toggle[data-v-5254fdbf].open .toggle-content[data-v-5254fdbf] {
    opacity: 1;
    transition: 0.5s all ease-in-out;
  }

  .toggle[data-v-5254fdbf] {
    max-height: 66px;
    animation: heightOut 0.7s ease-out;
    transform-origin: 100% 0%;
  }

  .toggle[data-v-5254fdbf].open {
    max-height: none;
    animation: heightIn 0.7s ease-in;
  }

  @keyframes heightIn {
    0% {
      transform: scaleY(0.9);
    }

    0.1% {
      transform: scaleY(0.6);
    }

    100% {
      transform: scaleY(1);
    }
  }

  @keyframes heightOut {
    100% {
      transform: scaleY(0.9);
    }

    0% {
      transform: scaleY(1);
    }
  }

  .toggle-content>div[data-v-5254fdbf] {
    padding: 35px 35px 50px;
    border-top: 1px solid #e0e0e0
  }

  .toggle-content p[data-v-5254fdbf] {
    margin-top: 0
  }

  .toggle-content[data-v-5254fdbf] a {
    margin-top: 1em
  }

  @media (max-width:1100px) {
    .toggles[data-v-5254fdbf] {
      margin: 70px 0 0
    }

    .toggle[data-v-5254fdbf] {
      width: calc(50% - 50px);
      margin: 25px
    }
  }

  @media (max-width:780px) {
    .toggles[data-v-5254fdbf] {
      margin: 50px 0 0
    }

    .toggle[data-v-5254fdbf] {
      width: 100%;
      max-width: 400px;
      margin: 25px 0
    }
  }

  @media (max-width:580px) {
    .toggles[data-v-5254fdbf] {
      margin: 30px 0 0
    }

    .toggle[data-v-5254fdbf] {
      border-radius: 25px
    }

    .btn-toggle[data-v-5254fdbf] {
      padding: 20px;
      font-size: 1.6rem
    }
  }

  .mod[data-v-a6a951ea] {
    position: relative;
    z-index: 1
  }

  .illus-over-wave[data-v-a6a951ea] {
    padding-bottom: 0
  }

  .illus-over-wave .wrapper-split.half .wrapper-img img[data-v-a6a951ea] {
    max-height: none;
    width: calc(50vw - 100px);
    max-width: 760px;
    margin-bottom: -280px
  }

  .illus-over-wave .wrapper-wave[data-v-a6a951ea] {
    -webkit-transform: translate3d(0, 81%, 0) rotateY(180deg);
    transform: translate3d(0, 81%, 0) rotateY(180deg)
  }

  h3+.wrapper-split[data-v-a6a951ea] {
    margin-top: 80px
  }

  .wrapper-split[data-v-a6a951ea] {
    display: flex;
    align-items: center;
    justify-content: space-between;
    color: #a1a6ac
  }

  .wrapper-split.half>div[data-v-a6a951ea] {
    width: calc(50% - 80px);
  }

  .wrapper-split.half>div[data-v-a6a951ea] p {
    text-align: justify;
  }

  .wrapper-split.half.reversed .wrapper-txt[data-v-a6a951ea] {
    order: 2
  }

  .wrapper-split.half.reversed .wrapper-img[data-v-a6a951ea],
  .wrapper-split.half.reversed .wrapper-second-txt[data-v-a6a951ea] {
    order: 1
  }

  .wrapper-split.half .wrapper-img img[data-v-a6a951ea] {
    max-height: 500px
  }

  .wrapper-split.img-cover[data-v-a6a951ea] {
    min-height: 650px
  }

  .wrapper-split.img-cover.reversed[data-v-a6a951ea] {
    justify-content: flex-end
  }

  .wrapper-split.img-cover.reversed .wrapper-img[data-v-a6a951ea] {
    left: 0;
    right: auto;
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
    border-top-right-radius: 25px;
    border-bottom-right-radius: 25px
  }

  .wrapper-split.img-cover .wrapper-img[data-v-a6a951ea] {
    position: absolute;
    top: 50%;
    right: 0;
    width: 50vw;
    height: 650px;
    margin: -325px 0 0;
    background-repeat: no-repeat;
    background-size: cover;
    background-position: 50% 50%;
    border-top-left-radius: 25px;
    border-bottom-left-radius: 25px
  }

  .wrapper-split.img-cover .wrapper-img img[data-v-a6a951ea] {
    display: none
  }

  .wrapper-split.img-cover .wrapper-txt[data-v-a6a951ea] {
    width: calc(50% - 250px)
  }

  .wrapper-split.two-cols-text[data-v-a6a951ea] {
    align-items: flex-start
  }

  .deco[data-v-a6a951ea] {
    position: absolute;
    bottom: 30%;
    left: 0;
    width: 40%;
    z-index: -1
  }

  .deco.reversed[data-v-a6a951ea] {
    left: auto;
    right: 0
  }

  .wrapper-wave[data-v-a6a951ea] {
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    -webkit-transform: translate3d(0, 81%, 0);
    transform: translate3d(0, 81%, 0);
    overflow: hidden;
    pointer-events: none;
    z-index: -1
  }

  .wrapper-wave .wave[data-v-a6a951ea] {
    display: block;
    position: relative;
    width: 102%;
    left: -1%
  }

  .wrapper-wave .wave[data-v-a6a951ea]:before {
    content: "";
    display: block;
    padding-bottom: 15%
  }

  .wrapper-wave .icon[data-v-a6a951ea] {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%
  }

  .fifty-fifty-out .wrapper-img[data-v-a6a951ea],
  .fifty-fifty-out .wrapper-second-txt[data-v-a6a951ea],
  .fifty-fifty-out .wrapper-txt[data-v-a6a951ea] {
    opacity: 0;
    -webkit-transform: translateY(40px);
    transform: translateY(40px);
    transition: .3s ease-out
  }

  .fifty-fifty-out .wrapper-img[data-v-a6a951ea],
  .fifty-fifty-out .wrapper-second-txt[data-v-a6a951ea] {
    transition-delay: .2s
  }

  .fifty-fifty-in .wrapper-img[data-v-a6a951ea],
  .fifty-fifty-in .wrapper-second-txt[data-v-a6a951ea],
  .fifty-fifty-in .wrapper-txt[data-v-a6a951ea] {
    opacity: 1;
    -webkit-transform: translateY(0);
    transform: translateY(0);
    transition: .4s ease-out
  }

  .fifty-fifty-in .wrapper-img[data-v-a6a951ea],
  .fifty-fifty-in .wrapper-second-txt[data-v-a6a951ea] {
    transition-delay: .2s
  }

  @media (max-width:1600px) {
    .wrapper-split.img-cover[data-v-a6a951ea] {
      min-height: 500px
    }

    .wrapper-split.img-cover .wrapper-img[data-v-a6a951ea] {
      height: 500px;
      margin: -250px 0 0
    }

    .wrapper-split.img-cover .wrapper-second-txt[data-v-a6a951ea],
    .wrapper-split.img-cover .wrapper-txt[data-v-a6a951ea] {
      width: calc(50% - 65px)
    }
  }

  @media (max-width:1400px) {
    .illus-over-wave .wrapper-split.half .wrapper-img img[data-v-a6a951ea] {
      margin-bottom: -250px
    }
  }

  @media (max-width:1200px) {
    .illus-over-wave[data-v-a6a951ea] {
      padding-bottom: 0
    }

    .illus-over-wave .wrapper-split.half .wrapper-img img[data-v-a6a951ea] {
      max-height: 500px;
      width: auto;
      max-width: 100%;
      margin-bottom: 0
    }
  }

  @media (max-width:1100px) {
    .wrapper-split.half>div[data-v-a6a951ea] {
      width: calc(50% - 50px)
    }

    .wrapper-split.img-cover[data-v-a6a951ea] {
      min-height: 400px
    }

    .wrapper-split.img-cover .wrapper-img[data-v-a6a951ea] {
      height: 400px;
      margin: -200px 0 0
    }
  }

  @media (max-width:960px) {
    .wrapper-split.half>div[data-v-a6a951ea] {
      width: calc(50% - 25px)
    }

    .wrapper-split.img-cover[data-v-a6a951ea] {
      flex-wrap: wrap;
      justify-content: center;
      min-height: 0
    }

    .wrapper-split.img-cover.reversed[data-v-a6a951ea] {
      justify-content: center;
      left: auto
    }

    .wrapper-split.img-cover.reversed .wrapper-txt[data-v-a6a951ea] {
      order: 1
    }

    .wrapper-split.img-cover.reversed .wrapper-img[data-v-a6a951ea],
    .wrapper-split.img-cover.reversed .wrapper-second-txt[data-v-a6a951ea] {
      order: 2
    }

    .wrapper-split.img-cover .wrapper-img[data-v-a6a951ea] {
      position: relative;
      top: auto;
      right: auto;
      height: auto;
      width: auto;
      max-width: 500px;
      margin: 20px 0 0;
      background: 0 0 !important
    }

    .wrapper-split.img-cover .wrapper-img img[data-v-a6a951ea] {
      display: block
    }

    .wrapper-split.img-cover .wrapper-second-txt[data-v-a6a951ea],
    .wrapper-split.img-cover .wrapper-txt[data-v-a6a951ea] {
      width: 100%
    }
  }

  @media (max-width:780px) {
    h3+.wrapper-split[data-v-a6a951ea] {
      margin-top: 50px
    }

    .wrapper-split[data-v-a6a951ea] {
      flex-wrap: wrap
    }

    .wrapper-split.half[data-v-a6a951ea] {
      justify-content: center
    }

    .wrapper-split.half>div[data-v-a6a951ea] {
      width: 100%
    }

    .wrapper-split.half.reversed .wrapper-txt[data-v-a6a951ea] {
      order: 1
    }

    .wrapper-split.half.reversed .wrapper-img[data-v-a6a951ea],
    .wrapper-split.half.reversed .wrapper-second-txt[data-v-a6a951ea] {
      order: 2
    }

    .wrapper-split.half .wrapper-img[data-v-a6a951ea] {
      max-width: 400px;
      margin-top: 20px
    }

    .deco[data-v-a6a951ea] {
      display: none
    }
  }

  .wrapper-split-panel .slide-txt.lists-anim ul li:after {
    color: #fff
  }

  .wrapper-split-panel .slide-txt.txts-anim h1,
  .wrapper-split-panel .slide-txt.txts-anim h2,
  .wrapper-split-panel .slide-txt.txts-anim h3,
  .wrapper-split-panel .slide-txt.txts-anim h4,
  .wrapper-split-panel .slide-txt.txts-anim h5,
  .wrapper-split-panel .slide-txt.txts-anim h6 {
    transition: color .2s ease-in-out
  }

  .wrapper-split-panel .slide-txt ul li:before {
    display: block;
    transition: background .2s ease-in-out
  }

  @media (max-width:780px) {
    .wrapper-split-panel .slide-txt.lists-anim ul li:before {
      background: #cfe6f1 !important
    }

    .wrapper-split-panel .slide-txt.lists-anim ul li:after {
      color: #11a3eb !important
    }
  }

  .wrapper-blockquote-links[data-v-341f37ba] {
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%
  }

  .wrapper-blockquote-links blockquote[data-v-341f37ba] {
    width: calc(50% - 50px)
  }

  .wrapper-links[data-v-341f37ba] {
    width: calc(50% - 100px);
    padding: 0 50px
  }

  .circle-links[data-v-341f37ba]:before {
    content: "";
    display: block;
    width: 100%;
    padding-bottom: 100%
  }

  .content-circle[data-v-341f37ba] {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    border: 1px dashed #1774bc;
    border-radius: 50%
  }

  .content-circle[data-v-341f37ba],
  .wrapper-logo[data-v-341f37ba] {
    display: flex;
    align-items: center;
    justify-content: center
  }

  .wrapper-logo[data-v-341f37ba] {
    border-radius: 50%;
    background: #fff
  }

  .logo[data-v-341f37ba],
  .wrapper-logo[data-v-341f37ba] {
    width: 70%;
    height: 70%
  }

  .logo[data-v-341f37ba] {
    display: block;
    background-repeat: no-repeat;
    background-position: 50% 50%;
    background-size: contain
  }

  .bs[data-v-341f37ba] {
    width: 200px;
    height: 200px;
    border-radius: 50%;
    padding: 0;
    list-style: none;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    margin: 0
  }

  .bs>[data-v-341f37ba],
  .bs[data-v-341f37ba] {
    position: absolute
  }

  .bs>[data-v-341f37ba] {
    display: block;
    top: 50%;
    left: 50%;
    margin: -7px;
    width: 14px;
    height: 14px
  }

  .bs[data-v-341f37ba]>:first-of-type {
    -webkit-transform: rotate(-35deg) translate(100px) rotate(35deg);
    transform: rotate(-35deg) translate(100px) rotate(35deg)
  }

  .bs[data-v-341f37ba]>:nth-of-type(2) {
    -webkit-transform: rotate(37deg) translate(100px) rotate(-37deg);
    transform: rotate(37deg) translate(100px) rotate(-37deg)
  }

  .bs[data-v-341f37ba]>:nth-of-type(3) {
    -webkit-transform: rotate(109deg) translate(100px) rotate(-109deg);
    transform: rotate(109deg) translate(100px) rotate(-109deg)
  }

  .bs[data-v-341f37ba]>:nth-of-type(4) {
    -webkit-transform: rotate(181deg) translate(100px) rotate(-181deg);
    transform: rotate(181deg) translate(100px) rotate(-181deg)
  }

  .bs[data-v-341f37ba]>:nth-of-type(5) {
    -webkit-transform: rotate(253deg) translate(100px) rotate(-253deg);
    transform: rotate(253deg) translate(100px) rotate(-253deg)
  }

  .bs>span[data-v-341f37ba],
  .bs>span[data-v-341f37ba]:before {
    border-radius: 50%
  }

  .bs>span[data-v-341f37ba]:before {
    content: "";
    display: block;
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -4px 0 0 -4px;
    width: 8px;
    height: 8px;
    background: #fff
  }

  .bs>span.active[data-v-341f37ba] {
    border: 2px solid #fff
  }

  .bs>span.active[data-v-341f37ba]:before {
    content: none
  }

  .bs-title[data-v-341f37ba] {
    position: absolute;
    display: inline-flex;
    padding: 16px 26px;
    border-radius: 15px;
    font-family: gilroy, sans-serif;
    font-weight: 700;
    font-size: 1.7rem;
    text-transform: uppercase;
    white-space: nowrap;
    color: #11a3eb;
    background: #fff;
    box-shadow: 8px 13px 73px 11px rgba(0, 0, 0, .1);
    -webkit-transform-origin: 50% 50%;
    transform-origin: 50% 50%;
    transition: none;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden
  }

  @media (max-width:1400px) {
    .bs-title[data-v-341f37ba] {
      padding: 13px 18px;
      font-size: 1.3rem;
      border-radius: 12px
    }
  }

  @media (max-width:1100px) {
    .wrapper-blockquote-links[data-v-341f37ba] {
      flex-direction: column
    }

    .wrapper-blockquote-links blockquote[data-v-341f37ba] {
      width: 100%;
      margin-top: 10rem;
      margin-bottom: 0
    }

    .bs[data-v-341f37ba],
    .circle-links[data-v-341f37ba] {
      width: 150px
    }

    .bs[data-v-341f37ba] {
      height: 150px;
      border-radius: 50%;
      padding: 0;
      list-style: none
    }

    .bs>[data-v-341f37ba] {
      display: block;
      position: absolute;
      top: 50%;
      left: 50%;
      margin: -7px;
      width: 14px;
      height: 14px
    }

    .bs[data-v-341f37ba]>:first-of-type {
      -webkit-transform: rotate(-35deg) translate(75px) rotate(35deg);
      transform: rotate(-35deg) translate(75px) rotate(35deg)
    }

    .bs[data-v-341f37ba]>:nth-of-type(2) {
      -webkit-transform: rotate(37deg) translate(75px) rotate(-37deg);
      transform: rotate(37deg) translate(75px) rotate(-37deg)
    }

    .bs[data-v-341f37ba]>:nth-of-type(3) {
      -webkit-transform: rotate(109deg) translate(75px) rotate(-109deg);
      transform: rotate(109deg) translate(75px) rotate(-109deg)
    }

    .bs[data-v-341f37ba]>:nth-of-type(4) {
      -webkit-transform: rotate(181deg) translate(75px) rotate(-181deg);
      transform: rotate(181deg) translate(75px) rotate(-181deg)
    }

    .bs[data-v-341f37ba]>:nth-of-type(5) {
      -webkit-transform: rotate(253deg) translate(75px) rotate(-253deg);
      transform: rotate(253deg) translate(75px) rotate(-253deg)
    }

    .bs-title[data-v-341f37ba] {
      padding: 10px 16px;
      font-size: 1.3rem;
      border-radius: 9px
    }
  }

  @media (max-width:960px) {
    .wrapper-blockquote-links blockquote[data-v-341f37ba] {
      margin-top: 0;
      margin-bottom: 0
    }

    .wrapper-links[data-v-341f37ba] {
      display: none
    }
  }

  .mod.small-padding {
    padding-top: 70px;
    padding-bottom: 70px;
  }

  .slides[data-v-cd71eb18] .slide[data-v-cd71eb18]:nth-child(2) .slide-txt ul li::before {
    background-color: rgb(255, 129, 117);
  }

  .slides[data-v-cd71eb18] .slide[data-v-cd71eb18]:nth-child(3) .slide-txt ul li::before {
    background-color: rgb(247, 187, 23);
  }

  @media (max-width:1010px) {
    .slide-img[data-v-cd71eb18] {
      height: calc(100vh - 70px)
    }

    .slide-img.fixed[data-v-cd71eb18] {
      top: 70px
    }

    .slide[data-v-cd71eb18],
    .slides[data-v-cd71eb18] {
      min-height: calc(100vh - 70px)
    }
  }

  @media (max-width:780px) {
    .wrapper-split-panel[data-v-cd71eb18] {
      background: 0 0
    }

    .slides[data-v-cd71eb18] {
      width: 100%;
      min-height: 0
    }

    .slide[data-v-cd71eb18] {
      display: block;
      min-height: 0;
      padding-bottom: 100px
    }

    .slide-img[data-v-cd71eb18] {
      position: relative;
      top: auto;
      left: auto;
      width: auto;
      height: auto;
      margin: 0 0 30px;
      padding: 30px 0;
      opacity: 1 !important;
      -webkit-transform: translateY(0) !important;
      transform: translateY(0) !important
    }

    .slide-img.img-cover[data-v-cd71eb18] {
      padding: 0
    }

    .slide-img.img-cover .img-bg>img[data-v-cd71eb18] {
      max-width: 100%;
      width: 100%
    }

    .slide-img .img-bg[data-v-cd71eb18] {
      position: relative;
      top: auto;
      right: auto;
      bottom: auto;
      left: auto;
      background: 0 0 !important;
      width: 100%
    }

    .slide-img .img-bg>img[data-v-cd71eb18],
    .slide-img-bg[data-v-cd71eb18] {
      display: block
    }

    .slide-txt[data-v-cd71eb18] {
      padding: 0 30px
    }

    .wrapper-bg[data-v-cd71eb18] {
      display: none
    }
  }

  @media (max-width:580px) {
    .slide[data-v-cd71eb18] {
      padding-bottom: 70px
    }
  }

  .module-fifty.first .mod.small-padding {
    padding-top: 0;
  }

  .wrapper-slider[data-v-fb673edc] {
    display: flex;
    justify-content: flex-end;
    align-items: center;
    min-height: 650px;
    color: #a1a6ac;
    z-index: 1
  }

  .slider[data-v-fb673edc] {
    width: calc(50% - 65px);
    height: auto;
    position: static;
    overflow: visible;
  }

  .slider>div {
    position: static;
    height: auto;
  }

  .slides[data-v-fb673edc] {
    display: block;
    list-style-type: none;
    margin: 35px 0 0
  }

  .slides>li[data-v-fb673edc] {
    display: block;
    margin: 5px 0
  }

  .slide.active .btn-slide[data-v-fb673edc]:before {
    opacity: 1
  }

  .slide.active .btn-slide>span[data-v-fb673edc]:before {
    font-family: icons !important;
    content: "\E915";
    speak: none;
    font-style: normal;
    font-weight: 400;
    -webkit-font-feature-settings: normal;
    font-feature-settings: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    font-size: 6px;
    background: #5eca4f;
    color: #fff
  }

  .btn-slide[data-v-fb673edc] {
    position: relative;
    display: block;
    width: 100%;
    z-index: 1;
    color: #a1a6ac;
    text-align: left
  }

  .btn-slide[data-v-fb673edc]:before {
    content: "";
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    border-radius: 10px;
    background: #fff;
    box-shadow: 13px 20px 35px rgba(5, 36, 59, .17);
    opacity: 0;
    z-index: -1
  }

  .btn-slide>span[data-v-fb673edc] {
    position: relative;
    display: block;
    line-height: 1.2;
    padding: 10px 18px 10px 45px;
    border-radius: 10px;
    overflow: hidden
  }

  .btn-slide>span[data-v-fb673edc]:before {
    position: absolute;
    top: 10px;
    left: 18px;
    content: "+";
    flex: 0 0 auto;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 16px;
    height: 16px;
    margin: 0 10px 0 0;
    background: #e6f0fe;
    border-radius: 50%;
    font-family: gilroy, sans-serif;
    font-weight: 700;
    line-height: 1;
    speak: none;
    font-style: normal;
    -webkit-font-feature-settings: normal;
    font-feature-settings: normal;
    font-variant: normal;
    color: #11a3eb
  }

  .progression[data-v-fb673edc] {
    display: block;
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 3px;
    -webkit-transform: scaleX(0);
    transform: scaleX(0);
    -webkit-transform-origin: 0 50%;
    transform-origin: 0 50%;
    background: #5eca4f;
  }

  .wrapper-images[data-v-fb673edc] {
    position: absolute;
    top: 50%;
    left: 0;
    width: calc(50% - 200px);
    height: 650px;
    margin: -325px 0 0
  }

  .bg-img[data-v-fb673edc] {
    width: 100%;
    height: 100%;
    background-repeat: no-repeat;
    background-size: cover;
    background-position: 50% 50%;
    border-top-right-radius: 50px;
    border-bottom-right-radius: 50px;
    opacity: 0
  }

  .slide.active .bg-img[data-v-fb673edc] {
    opacity: 1;
    transform: matrix(1, 0, 0, 1, 0, 0);
  }

  .slide.active .progression[data-v-fb673edc] {
    display: block;
    transform: scale(1);
    opacity: 1;

    transition: 3s all ease-in-out;
  }

  .wrapper-screens[data-v-fb673edc] {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0
  }

  .wrapper-screens>img[data-v-fb673edc]:nth-of-type(2) {
    top: 190px
  }

  .wrapper-screens>img[data-v-fb673edc]:nth-of-type(3) {
    top: 290px
  }

  .wrapper-screens>img[data-v-fb673edc]:nth-of-type(4) {
    top: 340px
  }

  .wrapper-screens>img[data-v-fb673edc]:nth-of-type(5) {
    top: 490px
  }

  .screen[data-v-fb673edc] {
    position: absolute;
    top: 90px;
    right: -150px;
    max-width: 75%;
    z-index: 1;
    -webkit-filter: drop-shadow(18px 31px 79px rgba(3, 35, 59, .32));
    filter: drop-shadow(18px 31px 79px rgba(3, 35, 59, .32));
    opacity: 0;
    will-change: filter
  }

  @media (max-width:1400px) {
    .wrapper-images[data-v-fb673edc] {
      width: calc(50% - 50px);
      height: 500px;
      margin-top: -250px
    }

    .wrapper-screens[data-v-fb673edc] {
      display: none
    }

    .screen[data-v-fb673edc] {
      right: -125px
    }
  }

  @media (max-width:1100px) {
    .wrapper-images[data-v-fb673edc] {
      height: 400px;
      margin-top: -200px
    }
  }

  @media (max-width:960px) {
    .wrapper-slider[data-v-fb673edc] {
      min-height: 0
    }

    .slider[data-v-fb673edc] {
      width: 100%
    }

    .slides>li[data-v-fb673edc] {
      position: relative;
      margin: 20px 0;
      padding-left: 25px
    }

    .slides>li[data-v-fb673edc]:after,
    .slides>li[data-v-fb673edc]:before {
      position: absolute
    }

    .slides>li[data-v-fb673edc]:before {
      content: "";
      display: block;
      position: absolute;
      top: 2px;
      left: 0;
      width: 16px;
      height: 16px;
      border-radius: 50%;
      background: #cfe6f1
    }

    .slides>li[data-v-fb673edc]:after {
      font-family: icons !important;
      content: "\E915";
      speak: none;
      font-style: normal;
      font-weight: 400;
      -webkit-font-feature-settings: normal;
      font-feature-settings: normal;
      font-variant: normal;
      text-transform: none;
      line-height: 1;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
      color: #11a3eb;
      font-size: 6px;
      top: 7px;
      left: 4px
    }

    .slide.active .btn-slide[data-v-fb673edc]:before {
      opacity: 0
    }

    .slide.active .btn-slide>span[data-v-fb673edc]:before {
      content: none
    }

    .btn-slide[data-v-fb673edc] {
      display: inline;
      margin: 0;
      font-size: 1.6rem;
      cursor: auto
    }

    .btn-slide>span[data-v-fb673edc] {
      padding: 0;
      border-radius: 0
    }

    .btn-slide>span[data-v-fb673edc]:before {
      content: none
    }

    .wrapper-images[data-v-fb673edc],
    .slide.active .progression[data-v-fb673edc] {
      display: none
    }
  }

  @media (max-width:780px) {
    .wrapper-images[data-v-fb673edc] {
      height: 300px
    }
  }

  @media (max-width:580px) {
    .wrapper-images[data-v-fb673edc] {
      height: 250px
    }

    .hero[data-v-46aa99ba] {
      padding-top: 0 !important;
      min-height: calc(100vh - 64px)
    }
  }

  @media (max-width:400px) {
    .wrapper-images[data-v-fb673edc] {
      height: 200px
    }
  }

  .paddSlide .mod.medium-padding {
    padding: 50px 0;
  }
</style>
<main role="main" class="main" second>
  <div second>
    <div data-v-da4b56e8="">
      <div class="getHeight">
        <div data-v-46aa99ba="">
          <div data-v-46aa99ba="" class="block">
            <div data-v-0fefbdef="" data-v-46aa99ba="" class="wrapper-headband headband-bottom" style="fill: white;">
              <span data-v-0fefbdef="" class="headband left" style="background-color: rgb(210, 177, 147);"></span> <svg
                data-v-0fefbdef="" class="icon icon-wave" style="fill: rgb(210, 177, 147);">
                <use xlink:href="#icon-wave"></use>
              </svg> <span data-v-0fefbdef="" class="mouse" style="color: rgb(126, 106, 88);"></span> <span
                data-v-0fefbdef="" class="headband right" style="background-color: rgb(210, 177, 147);"></span></div>
            <section data-v-46aa99ba="" class="hero half has-bg block"><span data-v-46aa99ba="" class="bg"
                style="background-color: rgb(210, 177, 147);"></span>
              <div data-v-46aa99ba="" class="container large">
                <div data-v-46aa99ba="" class="wrapper-text">
                  <h1 data-v-46aa99ba=""><strong>Ecommerce moves fast</strong></h1>
                  <p data-v-46aa99ba="" class="subtitle" style="color: rgb(126, 106, 88);">We can help you keep up</p>
                  <button data-v-46aa99ba="" class="btn btn-arrow modalShow font-change"
                    style="background-color: rgb(126, 106, 88);"><span>Start shipping now</span> <svg
                      class="icon icon-arrow-right">
                      <use xlink:href="#icon-arrow-right"></use>
                    </svg></button>
                </div>
                <div data-v-46aa99ba="" class="wrapper-illus">
                  <!---->
                  <!----> <img data-v-46aa99ba="" alt="Ecommerce courier" src="img/Couriers -1st fold.png"
                    alt="Delivery for e-commerce and online shops">
                </div>
              </div>
            </section>
          </div>
        </div>
        <div data-v-3d37c500="">
          <div data-v-3d37c500="" class="block">
            <div data-v-3d37c500="" class="block mod medium-padding has-bg wrapper-block-rounded"
              style="background-color: rgb(255, 255, 255); background-image: url(img/1554902447-ecommerceimgintro.webp)">
              <span data-v-3d37c500="" class="bg"></span>
              <!---->
              <div data-v-3d37c500="" class="container">
                <div data-v-3d37c500="" class="block-rounded block-rounded-in">
                  <div data-v-3d37c500="" class="section-title">E-commerce</div>
                  <div data-v-3d37c500="">
                    <h2>The Best E-Commerce Shipping Partner in India</h2>
                    <p><span>We provide hi-tech shipping solutions for the demand for a premium delivery experience through our advanced technology integrating with yours, allowing you to conveniently manage and ship your inventory at the lowest courier rates. Notifications are driven through NDR Management, real-time reports, and the IVR system. Request a courier in three clicks when needed, using a pay-as-you-go model and deliver using a professional courier fleet. Simple, right? GetGo is your logistics solution for all your urgent deliveries at the lowest shipping rates.</span></p>
                  </div>
                  <!---->
                  <!---->
                </div>
              </div>
            </div>
          </div>
        </div>
        <div data-v-d7eeecfc="" data-v-5ba14530="" class="module module-logos">
          <div data-v-d7eeecfc="" class="block">
            <div data-v-d7eeecfc="" class="block mod x-small-padding has-bg" style="background-color: #105285;">
              <span data-v-d7eeecfc="" class="bg"></span>
              <div data-v-d7eeecfc="" class="wrapper-row container">
                <ul data-v-d7eeecfc="" class="wrapper-logos">
                  <li data-v-d7eeecfc=""><img src="img/BSE_LOGO.png" alt="Ecommerce logistics" data-v-d7eeecfc=""
                      class="is-animated"></li>
                  <li data-v-d7eeecfc=""><img src="img/continental coffee.png" data-v-d7eeecfc="" class="is-animated"
                      alt="Courier aggregator"></li>
                  <li data-v-d7eeecfc=""><img src="img/insta360.png" data-v-d7eeecfc="" class="is-animated"
                      alt="Shipping solution"></li>
                  <li data-v-d7eeecfc=""><img src="img/iskon.png" data-v-d7eeecfc="" class="is-animated"
                      alt="NDR"></li>
                  <li data-v-d7eeecfc=""><img src="img/origami.png" alt="Reverse logistics" data-v-d7eeecfc="" class="is-animated">
                  </li>
                  <li data-v-d7eeecfc=""><img src="img/OYO_Logo.png" data-v-d7eeecfc="" alt="Shipping solution"></li>
                  <li data-v-d7eeecfc=""><img src="img/srisri.png" alt="Cash on delivery service" data-v-d7eeecfc=""></li>
                  <li data-v-d7eeecfc=""><img src="img/swiggy.png" alt="COD service" data-v-d7eeecfc=""></li>
                  <li data-v-d7eeecfc=""><img src="img/talwalkars.png" data-v-d7eeecfc="" alt="Top courier companies in india"></li>
                  <li data-v-d7eeecfc=""><img src="img/william pen.png" data-v-d7eeecfc="" alt="Ecommerce shipping partner"></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div data-v-ef0c82cc="" data-v-5ba14530="" class="module module-big-list">
          <div data-v-ef0c82cc="" class="block">
            <div data-v-ef0c82cc="" class="container large">
              <div data-v-ef0c82cc="" class="block mod medium-padding has-bg wrapper-big-list three-cols"><span
                  data-v-ef0c82cc="" class="bg"></span>
                <div data-v-ef0c82cc="" class="intro small-margin">
                  <h3>The perfect E-commerce shipping solutions </h3>
                </div>
                <div data-v-ef0c82cc="" class="big-list">
                  <div data-v-ef0c82cc="" class="list-line">
                    <div data-v-ef0c82cc="" class="list-item">
                      <!----> <span data-v-ef0c82cc="" index="1" class="content-item">
                        <!----> <span data-v-ef0c82cc="" class="list-text">
                          <h5>Simple competitive pricing</h5>
                          <p><span>Partner with the cheapest shipping solutions provider with the lowest courier rates. With GetGo you get simple single pricing for all partners.</span></p>
                        </span></span></div>
                    <div data-v-ef0c82cc="" class="list-item">
                      <!----> <span data-v-ef0c82cc="" index="2" class="content-item">
                        <!----> <span data-v-ef0c82cc="" class="list-text">
                          <h5>Cash on delivery </h5>
                          <p>GetGo offers cash on delivery services across India in all locations, with the fastest COD remittance enabling merchants to collect cash payments.</p>
                        </span></span></div>
                  </div>
                </div>
                <div data-v-ef0c82cc="" class="below-list">
                  <div data-v-ef0c82cc="" class="list-line">
                    <div data-v-ef0c82cc="" class="list-item">
                      <!----> <span data-v-ef0c82cc="" index="3" class="content-item">
                        <!----> <span data-v-ef0c82cc="" class="list-text">
                          <h5>AI driven processes</h5>
                          <p><span>It’s a dynamic platform designed to be simple, effective and adaptable to your needs.
                              It selects the most effective delivery option & creates accurate shipping labels.</span>
                          </p>
                        </span></span></div>
                    <div data-v-ef0c82cc="" class="list-item">
                      <!----> <span data-v-ef0c82cc="" index="4" class="content-item">
                        <!----> <span data-v-ef0c82cc="" class="list-text">
                          <h5>Best support in the industry</h5>
                          <p><span>Gain access to a team fanatical about dedicated support. We don’t do automated voice
                              recordings or ignore difficult tickets, we live by our commitments.</span>
                          </p>
                        </span></span></div>
                    <div data-v-ef0c82cc="" class="list-item">
                      <!----> <span data-v-ef0c82cc="" index="5" class="content-item">
                        <!----> <span data-v-ef0c82cc="" class="list-text">
                          <!-- / Detailed Analytics -->
                          <h5>Valuable insights</h5>
                          <p>We’re dedicated to understanding your business in and out so we can help you grow. We
                            provide actionable insights for strategic business planning and forecasting.
                          </p>
                        </span></span></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div data-v-5254fdbf="" data-v-5ba14530="" class="block module module-toggle">
          <div data-v-5254fdbf="" class="block mod mod-toggle medium-padding has-bg">
            <div data-v-5254fdbf="" class="wrapper-toggle"><span data-v-5254fdbf="" class="wrapper-wave"><span
                  data-v-5254fdbf="" class="wave"><svg data-v-5254fdbf="" class="icon icon-wave-mod">
                    <use data-v-5254fdbf="" xlink:href="#icon-wave-mod"></use>
                  </svg></span></span>
              <div data-v-5254fdbf="" class="container large">
                <div data-v-5254fdbf="">
                  <h3>How can we work together ?</h3>
                </div>
                <div data-v-5254fdbf="" class="toggles">
                  <div data-v-5254fdbf="" class="toggle"><button data-v-5254fdbf="" class="btn-toggle">FORWARD
                    </button> <button data-v-5254fdbf="" class="btn-toggle-icon"></button>
                    <div data-v-5254fdbf="" class="toggle-content">
                      <div data-v-5254fdbf="">
                        <div data-v-5254fdbf="">
                          <p>Fast, flexible and cost-effective solution to ship products to your customers</p>
                        </div> <a data-v-5254fdbf="" class="btn primary-medium modalShow font-change">Create account</a>
                      </div>
                    </div>
                  </div>
                  <div data-v-5254fdbf="" class="toggle"><button data-v-5254fdbf="" class="btn-toggle">REVERSE
                    </button> <button data-v-5254fdbf="" class="btn-toggle-icon"></button>
                    <div data-v-5254fdbf="" class="toggle-content">
                      <div data-v-5254fdbf="">
                        <div data-v-5254fdbf="">
                          <p><span>Easy and convenient process to return for all your customers</span></p>
                        </div> <a data-v-5254fdbf="" class="btn primary-medium modalShow font-change">Create account</a>
                      </div>
                    </div>
                  </div>
                  <div data-v-5254fdbf="" class="toggle"><button data-v-5254fdbf="" class="btn-toggle">SAME-DAY DELIVERY
                    </button> <button data-v-5254fdbf="" class="btn-toggle-icon"></button>
                    <div data-v-5254fdbf="" class="toggle-content">
                      <div data-v-5254fdbf="">
                        <div data-v-5254fdbf="">
                          <p><span>Cost-effective solution for urgent deliveries</span></p>
                        </div> <a data-v-5254fdbf="" class="btn primary-medium modalShow font-change">Create account</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="module module-fifty" data-v-a6a951ea="" data-v-5ba14530="">
          <div class="block" data-v-a6a951ea="">
            <div class="block mod small-padding has-bg fifty bg-white" data-v-a6a951ea=""><span class="bg"
                data-v-a6a951ea=""></span>
              <!---->
              <div class="pos-relative" data-v-a6a951ea="">
                <div class="container" data-v-a6a951ea="">
                  <!---->
                  <div class="wrapper-split half fifty-fifty-in reversed" data-v-a6a951ea="">
                    <div class="wrapper-txt default-style nte" data-v-a6a951ea="">
                      <h3>Comprehensive integrations
                      </h3>
                      <p><span>API so easy to use, integrate GetGo into your eCommerce platform or Warehouse Management System, Amazon, eBay, Shopify or even WooCommerce, decide your platform and leave the rest on the multi-channel integrated logistics solution, GetGo Logistics. Don’t waste time on the technical part, just invest in the best logistics solution provider in India.</span>
                      </p>
                    </div>
                    <div class="wrapper-img nte" style="background-image:none" data-v-a6a951ea=""><img
                        src="img/comprehensive integration.png" data-v-a6a951ea="" alt="Logistics solution"></div>
                  </div>
                </div>
              </div>
              <!---->
            </div>
          </div>
        </div>
        <div class="module module-fifty first" data-v-a6a951ea="" data-v-5ba14530="">
          <div class="block" data-v-a6a951ea="">
            <div class="block mod small-padding has-bg fifty bg-white" data-v-a6a951ea=""><span class="bg"
                data-v-a6a951ea=""></span>
              <!---->
              <div class="pos-relative" data-v-a6a951ea="">
                <div class="container" data-v-a6a951ea="">
                  <!---->
                  <div class="wrapper-split half fifty-fifty-in" data-v-a6a951ea="">
                    <div class="wrapper-txt default-style nte" data-v-a6a951ea="">
                      <h3>Best in industry performance</h3>
                      <p><span>With
                          customer's growing appetite for faster deliveries, GetGo is one platform that will help you
                          power all your last-mile needs and delight your clients. Unleash the full potential of
                          flexible,
                          convenient and precise deliveries by partnering with us. Experience Happy Customers and
                          Increase
                          in Revenue with us</span></p>
                    </div>
                    <div class="wrapper-img nte" style="background-image:none" data-v-a6a951ea=""><img
                        src="img/courier-1.svg" data-v-a6a951ea="" alt="Ecommerce courier solution"></div>
                  </div>
                </div>
              </div>
              <!---->
            </div>
          </div>
        </div>
        <div class="module module-fifty first" data-v-a6a951ea="" data-v-5ba14530="">
          <div class="block" data-v-a6a951ea="">
            <div class="block mod small-padding has-bg fifty bg-white" data-v-a6a951ea=""><span class="bg"
                data-v-a6a951ea=""></span>
              <!---->
              <div class="pos-relative" data-v-a6a951ea="">
                <div class="container" data-v-a6a951ea="">
                  <!---->
                  <div class="wrapper-split half fifty-fifty-in reversed" data-v-a6a951ea="">
                    <div class="wrapper-txt default-style nte" data-v-a6a951ea="">
                      <h3>Fastest COD Remittance</h3>
                      <p><span>GetGo Logistics always stays a step ahead in the game by providing the best cash on delivery services. Make your clients happy and become even happier with the fastest COD remittance – twice a week. </span></p>
                    </div>
                    <div class="wrapper-img nte" style="background-image:none" data-v-a6a951ea=""><img
                        src="img/courier-2.svg" data-v-a6a951ea="" alt="Courier aggregator ecommerce"></div>
                  </div>
                </div>
              </div>
              <!---->
            </div>
          </div>
        </div>
        <div class="module module-fifty first" data-v-a6a951ea="" data-v-5ba14530="">
          <div class="block" data-v-a6a951ea="">
            <div class="block mod small-padding has-bg fifty bg-white" data-v-a6a951ea=""><span class="bg"
                data-v-a6a951ea=""></span>
              <!---->
              <div class="pos-relative" data-v-a6a951ea="">
                <div class="container" data-v-a6a951ea="">
                  <!---->
                  <div class="wrapper-split half fifty-fifty-in" data-v-a6a951ea="">
                    <div class="wrapper-txt default-style nte" data-v-a6a951ea="">
                      <h3>Widest coverage</h3>
                      <p><span>Pick a location and we’re there. We are the widest coverage courier company in India with more than 20000 Pin Codes. Our network makes it incredibly easy to ship your goods via express air or surface as per the demand.</span></p>
                    </div>
                    <div class="wrapper-img nte" style="background-image:none" data-v-a6a951ea=""><img
                        src="img/courier-3.svg" data-v-a6a951ea="" alt="All aggregators"></div>
                  </div>
                </div>
              </div>
              <!---->
            </div>
          </div>
        </div>
        <div class="module module-fifty first" data-v-a6a951ea="" data-v-5ba14530="">
          <div class="block" data-v-a6a951ea="">
            <div class="block mod small-padding has-bg fifty bg-white" data-v-a6a951ea=""><span class="bg"
                data-v-a6a951ea=""></span>
              <!---->
              <div class="pos-relative" data-v-a6a951ea="">
                <div class="container" data-v-a6a951ea="">
                  <!---->
                  <div class="wrapper-split half fifty-fifty-in reversed" data-v-a6a951ea="">
                    <div class="wrapper-txt default-style nte" data-v-a6a951ea="">
                      <h3>Seamless returns</span>
                      </h3>
                      <p><span>Your customer ordered a product but is not satisfied. You offer a return shipment and we’ll make it seamless. Lowest RTO rates in the industry making it a win-win for the buyer and the business. </span></p>
                    </div>
                    <div class="wrapper-img nte" style="background-image:none" data-v-a6a951ea=""><img
                        src="img/courier-4.svg" data-v-a6a951ea="" alt="Reduce RTO"></div>
                  </div>
                </div>
              </div>
              <!---->
            </div>
          </div>
        </div>
        <div class="module module-fifty first" data-v-a6a951ea="" data-v-5ba14530="">
          <div class="block" data-v-a6a951ea="">
            <div class="block mod small-padding has-bg fifty bg-white" data-v-a6a951ea=""><span class="bg"
                data-v-a6a951ea=""></span>
              <!---->
              <div class="pos-relative" data-v-a6a951ea="">
                <div class="container" data-v-a6a951ea="">
                  <!---->
                  <div class="wrapper-split half fifty-fifty-in" data-v-a6a951ea="">
                    <div class="wrapper-txt default-style nte" data-v-a6a951ea="">
                      <h3>Customized solution
                      </h3>
                      <p><span>Whether you own a big size firm or are just starting out in this industry, if you sell, we provide you the best logistics solution. Lowest shipping rates for all our customers is a promise. </span></p>
                    </div>
                    <div class="wrapper-img nte" style="background-image:none" data-v-a6a951ea=""><img
                        src="img/Cargo - Customised solution.png" data-v-a6a951ea="" alt="Return shipments">
                    </div>
                  </div>
                </div>
              </div>
              <!---->
            </div>
          </div>
        </div>
      </div>
      <div class="wrapper-split-panel module module-split-panel" data-v-cd71eb18="" data-v-5ba14530="">
        <div class="wrapper-bg" style="background-color: #fff" data-v-cd71eb18=""></div>
        <div class="slides" data-v-cd71eb18="">
          <section class="slide" data-v-cd71eb18="">
            <div class="slide-img" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); justify-content: flex-end;"
              data-v-cd71eb18=""><span class="slide-img-bg" style="background-color:#11a3eb" data-v-cd71eb18=""></span>
              <img src="img/home-api.png" data-v-cd71eb18="" alt="Shiprocket API integration"></div>
            <div class="slide-txt default-style txts-anim lists-anim" data-v-cd71eb18="">
              <div data-v-cd71eb18="">
                <h2 style="color: rgb(17, 163, 235);"><img src="img/1554923319-api.png" alt="Multiple courier companies">
                </h2>
                <h6 style="color: rgb(17, 163, 235);">Add on-demand delivery to your business:</h6>
                <ul>
                  <li>Sell on the top channels and marketplaces while you ship with the most reliable shipping platform
                  </li>
                  <li>Direct integrations into some of the most popular industry platforms</li>
                  <li>Automatic dispatch to most adequate courier following availability and package size</li>
                </ul>
              </div>
            </div>
          </section>
          <section class="slide" data-v-cd71eb18="">
            <div class="slide-img" style="opacity: 0; transform: matrix(1, 0, 0, 1, 0, 20); justify-content: flex-end;"
              data-v-cd71eb18=""><span class="slide-img-bg" style="background-color:rgb(255, 129, 117)"
                data-v-cd71eb18=""></span>
              <img src="img/home-dashboard.png" data-v-cd71eb18="" alt="Lowest courier rate"></div>
            <div class="slide-txt default-style txts-anim lists-anim" data-v-cd71eb18="">
              <div data-v-cd71eb18="">
                <h2><img src="img/1554923315-dashboard.png" alt="Lowest shipping rate">
                  <h6 style="color: rgb(255, 129, 117)">Create your business account in no time, and start delivering
                    today:</h6>
                  <ul>
                    <li>Select your pick up & drop off address</li>
                    <li>Select package size</li>
                    <li>Review pricing & launch the job
                    </li>
                  </ul>
              </div>
            </div>
          </section>
          <section class="slide" data-v-cd71eb18="">
            <div class="slide-img" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); justify-content: flex-end;"
              data-v-cd71eb18=""><span class="slide-img-bg" style="background-color:rgb(247, 187, 23)"
                data-v-cd71eb18=""></span>
              <img src="img/channel integrations (couriers and cargo).png" data-v-cd71eb18="" alt="Widest coverage courier company"></div>
            <div class="slide-txt default-style txts-anim lists-anim" data-v-cd71eb18="">
              <div data-v-cd71eb18="">
                <h2 style="color: rgb(247, 187, 23);"><img src="img/mobileapp.png" alt="Fast COD Remittance">
                </h2>
                <h6 style="color: rgb(247, 187, 23);">Sell anywhere, ship using GetGo</h6>
                <ul>
                  <li>Compatible with all major marketplaces, ERP and order processing system</li>
                  <li>Effectual process automation guarantee on-time shipments regularly.</li>
                  <li>Automatically fetches order information to reduce turnaround time</li>
                </ul>
              </div>
            </div>
          </section>
        </div>
      </div>
      <div class="module module-slider paddSlide" data-v-fb673edc="" data-v-5ba14530="">
        <div class="block" data-v-fb673edc="">
          <div class="block mod medium-padding has-bg" style="background-color:transparent" data-v-fb673edc=""><span
              class="bg" data-v-fb673edc=""></span>
            <div class="wrapper-slider container" data-v-fb673edc="">
              <div class="slider" data-v-fb673edc="">
                <div class="wrapper-title default-style no-margin" data-v-fb673edc="">
                  <h3><span>Grow customer loyalty with awesome delivery</span></h3>
                  <p><span>By partnerning with GetGo get ready to offer the best delivery experience in the market to
                      all
                      your clients:</span></p>
                </div>
                <ul class="slides" data-v-fb673edc="">
                  <li class="slide" data-v-fb673edc="" style=""><button class="btn-slide" data-v-fb673edc=""><span
                        data-v-fb673edc=""><span data-v-fb673edc="">Real-time courier tracking</span> <span
                          class="progression" data-v-fb673edc=""></span></span></button>
                    <div class="wrapper-images" data-v-fb673edc="">
                      <div class="bg-img"
                        style="background-image: url(img/Cargo-real-time-courier-tracking-third-last-fold.png);"
                        data-v-fb673edc=""></div>
                      <div class="wrapper-screens" data-v-fb673edc=""><img src="" class="screen" data-v-fb673edc="" alt="NDR management">
                      </div>
                    </div>
                  </li>
                  <li class="slide active" data-v-fb673edc="" style=""><button class="btn-slide"
                      data-v-fb673edc=""><span data-v-fb673edc=""><span data-v-fb673edc="">Delivery estimated time of
                          arrival</span> <span class="progression" data-v-fb673edc=""></span></span></button>
                    <div class="wrapper-images" data-v-fb673edc="">
                      <div class="bg-img" style="background-image: url(img/Cargo-Delivery-estimate-time-arrival.jpg);"
                        data-v-fb673edc=""></div>
                      <div class="wrapper-screens" data-v-fb673edc=""><img src="" class="screen" data-v-fb673edc="" alt="Courier cost calculator">
                      </div>
                    </div>
                  </li>
                  <li class="slide" data-v-fb673edc="" style=""><button class="btn-slide" data-v-fb673edc=""><span
                        data-v-fb673edc=""><span data-v-fb673edc="">Advanced SMS notifications at every step of the
                          delivery - from pickup to completion</span> <span class="progression"
                          data-v-fb673edc=""></span></span></button>
                    <div class="wrapper-images" data-v-fb673edc="">
                      <div class="bg-img"
                        style="background-image: url(&quot;https://www.datocms-assets.com/8505/1554925962-industriesfinal-sectionall-languages3.jpg&quot;);"
                        data-v-fb673edc=""></div>
                      <div class="wrapper-screens" data-v-fb673edc=""><img
                          src="https://www.datocms-assets.com/8505/1554926062-notif-smsuk.png?auto=compress,format"
                          class="screen" data-v-fb673edc="">
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="module module-quote" data-v-341f37ba="" data-v-5ba14530="">
        <div class="block" data-v-341f37ba="">
          <div class="block mod small-padding has-bg bg-primary-medium" data-v-341f37ba=""><span class="bg"
              data-v-341f37ba=""></span>
            <div class="container wrapper-blockquote-links" data-v-341f37ba="">
              <div class="wrapper-links" data-v-341f37ba="">
                <div class="circle-links" id="app" data-v-341f37ba="">
                  <z-canvas :views='$options.components' style="background-color: transparent">
                  </z-canvas>
                </div>
              </div>
              <blockquote cite="#" data-v-341f37ba="">
                <p data-v-341f37ba=""><span class="blockquote-content" data-v-341f37ba="">We engaged Getgo for a review
                    of our domestic freight and, three years later, we’ve completed our supply chain project with them.
                    They have broad experience across their team and have shown flexibility in adapting to our changing
                    circumstances and needs.</span>
                  <span class="blockquote-author" data-v-341f37ba=""><cite
                      data-v-341f37ba="">Navin, </cite> Ecommerce Head</span>
                </p>
              </blockquote>
            </div>
          </div>
        </div>
      </div>
      <section class="block wrapper-findoutmore module module-find-out has-mockup" sixteen="" ten="">
        <!---->
        <div class="wrapper-mockup" sixteen="">
          <div sixteen=""><img src="img/footerSecond.png" alt="Mockup screen" class="mockup-img" sixteen=""> <img
              src="img/9th-fold.png" alt="Hang on! Your courier will be with you shortly." class="mockup-screen"
              sixteen=""></div>
        </div>
        <div class="container container-findoutmore" sixteen="">
          <div class="content-findoutmore" sixteen>
            <div sixteen>
              <h3 style="color:#105285" sixteen>Go ahead</h3>
              <h5 style="color:#a1a6ac" sixteen>Start powering your deliveries with GetGo today!
              </h5>
            </div>
            <div class="wrapper-buttons" sixteen><span sixteen><button class="btn contactShow" sixteen>Get Free Consultation</button></span><span sixteen>
                <a rel="noopener noreferrer" class="btn primary-medium modalShow font-change" sixteen>Create account</a></span>
            </div>
          </div>
        </div>
        <!---->
      </section>
    </div>
</main>
<svg aria-hidden="true" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
  style="position:absolute;width:0;height:0;overflow:hidden" second>
  <defs>
    <symbol id="icon-cross" viewBox="0 0 31 32">
      <title>Close</title>
      <path
        d="M18.127 16c0 0 0 0 0 0l12.393-12.301c0.832-0.832 0.832-2.22 0-3.052-0.462-0.462-1.017-0.647-1.572-0.647s-1.11 0.185-1.48 0.647l-11.931 11.838-11.838-11.838c-0.832-0.832-2.22-0.832-3.052 0s-0.832 2.22 0 3.052l12.301 12.301c0 0 0 0 0 0l-12.301 12.393c-0.832 0.832-0.832 2.22 0 3.052 0.462 0.462 1.017 0.647 1.48 0.647 0.555 0 1.11-0.185 1.48-0.647l11.931-11.931 11.931 11.931c0.832 0.832 2.22 0.832 3.052 0s0.832-2.22 0-3.052l-12.393-12.393z">
      </path>
    </symbol>
    <symbol id="icon-wave-mod" viewBox="0 0 215 32">
      <title>Wave separation</title>
      <path
        d="M0 24.273v-1.573zM0 0v22.7c1.584 1.37 3.724 1.969 5.833 2.205 8.423 0.942 13.742-5.362 20.27-9.215 10.66-6.282 23.513-9.45 35.949-9.525 11.088-0.064 21.512 1.98 32.321 4.12 5.801 1.145 11.494 2.665 17.402 3.221 6.004 0.567 12.468 0.642 18.483 0.278 5.726-0.353 11.163-1.573 16.952-1.177 12.554 0.856 24.251 6.015 35.414 11.184 7.588 3.51 15.679 6.272 24.037 7.46l7.909 0.76v-32.011h-214.571z">
      </path>
    </symbol>
    <symbol id="icon-star" viewBox="0 0 32 32">
      <title>Star</title>
      <path
        d="M17.676 1.089l3.495 7.403c0.263 0.561 0.779 0.966 1.397 1.069l0.011 0.002 7.818 1.189c1.534 0.234 2.146 2.201 1.034 3.33l-5.657 5.765c-0.35 0.362-0.566 0.856-0.566 1.401 0 0.117 0.010 0.231 0.029 0.342l-0.002-0.012 1.334 8.137c0.262 1.594-1.342 2.811-2.713 2.058l-6.995-3.843c-0.251-0.141-0.551-0.224-0.87-0.224s-0.619 0.083-0.879 0.229l0.009-0.005-6.995 3.843c-1.371 0.753-2.975-0.464-2.713-2.058l1.333-8.137c0.017-0.099 0.027-0.214 0.027-0.33 0-0.544-0.216-1.039-0.566-1.402l0.001 0.001-5.643-5.765c-1.107-1.129-0.495-3.099 1.035-3.33l7.818-1.189c0.629-0.105 1.145-0.509 1.403-1.059l0.005-0.012 3.495-7.403c0.294-0.647 0.934-1.089 1.677-1.089s1.384 0.442 1.673 1.077l0.005 0.012z">
      </path>
    </symbol>
    <symbol id="icon-play" viewBox="0 0 32 32">
      <title>play</title>
      <path fill="#105285"
        d="M16 0.47c8.577 0 15.53 6.953 15.53 15.53s-6.954 15.53-15.53 15.53-15.53-6.952-15.53-15.53 6.954-15.53 15.53-15.53z">
      </path>
      <path fill="#fff" d="M21.979 16.602l-9.56 5.368v-10.735z"></path>
    </symbol>
    <symbol id="icon-quotes" viewBox="0 0 38 32">
      <title>Doublequotes</title>
      <path
        d="M16.858 13.197c0.678-1.267 1.062-2.701 1.062-4.237 0-4.954-4.006-8.96-8.96-8.96-0.001 0-0.002 0-0.003 0-4.945 0-8.954 4.009-8.954 8.954 0 3.25 1.731 6.095 4.322 7.664l0.040 0.022c-3.046 10.97-4.365 15.36-4.365 15.36h6.72l10.112-18.765v-0.013l0.026-0.026zM37.043 13.197c0.678-1.267 1.062-2.701 1.062-4.237 0-4.954-4.006-8.96-8.96-8.96-0.001 0-0.002 0-0.003 0-4.945 0-8.954 4.009-8.954 8.954 0 3.25 1.731 6.095 4.322 7.664l0.040 0.022c-3.046 10.97-4.365 15.36-4.365 15.36h6.72l10.112-18.765v-0.013l0.026-0.026z">
      </path>
    </symbol>
    <symbol id="icon-help-center" viewBox="0 0 32 32">
      <title>help-center</title>
      <path fill="#11a3eb"
        d="M24.888 29.936h-16.624c-2.844 0-5.172-2.328-5.172-5.172v-16.628c-0.004-2.848 2.324-5.176 5.172-5.176h16.628c2.844 0 5.172 2.328 5.172 5.172v16.628c0 2.848-2.328 5.176-5.176 5.176z"
        style="fill:var(--color6,#11a3eb)"></path>
      <path fill="#fff"
        d="M20.936 20.764s-0.244 0.768-1.080 0.768h-1.172c-0.224-0.648-0.832-1.112-1.548-1.112-0.904 0-1.64 0.74-1.64 1.656s0.736 1.656 1.64 1.656c0.72 0 1.328-0.468 1.552-1.116h0.752c1.22 0 1.988-0.176 2.616-1.64-0.004 0-0.772 0.068-1.12-0.212zM20.404 11.788c0.8 0.78 1.036 1.384 1.036 1.388h1.188c-0.016-0.040-0.496-1.2-1.432-2.128-0.868-0.86-2.38-1.884-4.652-1.884-2.276 0-3.772 1.028-4.624 1.888-0.92 0.928-1.484 2.084-1.5 2.124h1.256c0.076-0.188 0.26-0.616 1.040-1.392 1.024-1.020 2.312-1.536 3.824-1.536 1.516 0 2.816 0.52 3.864 1.54zM11.544 13.72h-0.628c-1.184 0-2.156 0.968-2.156 2.156v2.336c0 1.184 0.968 2.156 2.156 2.156h0.636c0.312 0 0.564-0.252 0.564-0.564v-5.512c0-0.316-0.256-0.572-0.572-0.572zM21.564 13.72c-0.296 0-0.54 0.24-0.536 0.54l0.020 5.592c0 0.284 0.232 0.516 0.52 0.516h0.672c1.184 0 2.156-0.968 2.156-2.156v-2.336c0-1.184-0.968-2.156-2.156-2.156h-0.676z"
        style="fill:var(--color2,#fff)"></path>
    </symbol>
    <symbol id="icon-WE" viewBox="0 0 50 32">
      <title>WE</title>
      <path
        d="M2.168 1.637h1.084l0.361 1.807c0.723 0 1.085-0.361 1.085-1.084h0.361l0.723 0.723-0.361 2.168h0.361l-0.361 0.361v0.361l0.361 0.361v0.362h-0.361l0.361 0.361v0.362l-0.361 11.926v0.361h0.361l1.084-3.613h0.723l-2.53 6.866h0.723l4.336-12.287h-0.723l-1.446 4.698h-0.723v-0.361l2.53-5.782h0.361v0.723l2.891-0.723c0.964 0.354 1.446 0.716 1.446 1.084l0.361-0.361h1.084l1.446 8.673h0.361c0-1.077 1.899-7.284 5.697-18.621h0.362l0.361 1.084h0.362v-0.723h0.361c1.786 0 2.75 0.361 2.891 1.084v0.723l-0.723 1.084v0.361h0.723l0.361-2.529h0.723l0.723 0.723v0.361l-8.227 28.379-1.084 0.361v-0.723l-0.361 0.361h-0.362l-0.361-0.361c0 0.964-0.362 1.446-1.084 1.446-1.927-0.17-2.891-0.531-2.891-1.084h0.361c-0.241-1.204-0.723-2.048-1.446-2.529l-1.445-7.95c-0.411 0.014-1.857 3.387-4.337 10.118l-1.807 1.807h-0.361l-0.362-1.446c3.684-6.094 5.612-10.791 5.782-14.093-0.638 0.638-2.204 4.252-4.698 10.841-0.298 0-0.78 1.446-1.446 4.337h-3.252l-0.362-1.446-0.361 0.361h-0.361c0-1.304-0.362-2.267-1.084-2.891v-1.445l1.446 1.084-0.723-6.143 0.362-0.361-0.362-0.362 0.362-0.361h-0.362l0.362-0.361v-5.060l0.361-1.807h-0.361l0.361-0.361v-9.395l-0.361-0.362v-0.361l1.084-0.723zM23.404 1.807c-3.557 11.068-5.336 17.275-5.336 18.621v0.361h0.362l5.696-18.983h-0.722zM22.767 15.731l-0.723 3.252v0.361h0.361l0.723-3.252v-0.362h-0.361zM20.598 24.403l-0.723 3.252v0.723h0.361l0.723-3.614v-0.361h-0.361zM48.127 2.423l1.743 1.212c-0.51 2.14-1.431 3.167-2.763 3.082l-9.438 0.425-0.042 0.723 1.786 0.127 2.189-0.212 0.34 0.404 0.362 0.021 2.551-0.213 0.723 0.064-0.021 0.361-8.397 0.871-0.659 4.677 0.361 0.021 9.821-0.787 0.319 0.383-0.021 0.362-0.723-0.064v0.361l2.487 0.553-0.022 0.361c-0.524 1.347-3.982 2.438-10.373 3.274l-3.635 0.128 0.34 0.361-1.743 4.592h-0.361l0.212-3.252-0.361-0.021-0.701 5.017c7.213-0.964 12.308-1.346 15.284-1.148l1.764 0.489-0.021 0.723-0.723-0.042 0.319 0.383-0.064 1.084c-0.694 0.907-2.168 1.29-4.422 1.148l-0.021 0.361 2.891 0.191-0.064 0.723-16.814 2.466-0.361-0.021c-1.162-0.255-1.743-0.631-1.743-1.127v-0.064l0.063-1.084 0.999-3.911-0.34-0.383 0.723 0.021-0.127 1.807 0.361 0.042c0.142-1.204 0.425-1.786 0.851-1.743l-0.34-0.404 0.255-3.593-0.723-0.064-0.489 1.786 0.404-0.34 0.361 0.021-0.149 2.168-0.361-0.042 0.064-1.063-0.361-0.021-0.043 0.723-0.361-0.021 0.042-0.723 1.148-6.101-0.361-0.021 0.829-1.743-0.34-0.383 0.021-0.361 0.787-6.101 0.34 0.383 0.361 0.021 0.043-0.723-0.234-1.828 0.574-3.231 0.064-1.084 0.361 0.042 0.34 0.382 0.383-0.34 0.361 0.042 0.34 0.362 0.383-0.34 1.084 0.085 12.691-0.234zM30.036 18.217l-0.17 2.53 0.361 0.021 0.17-2.508-0.361-0.042zM41.962 18.685h0.362l-0.043 0.361-6.547 0.659-0.34-0.404 0.021-0.34 3.274-0.149-0.022 0.361 3.295-0.489z">
      </path>
    </symbol>
    <symbol id="icon-spanish" viewBox="0 0 32 32">
      <title>Spanish</title>
      <path fill="#e60012"
        d="M32 28.613v0-4.618h-32v4.618c0 1.868 1.519 3.387 3.387 3.387h25.226c1.868 0 3.387-1.519 3.387-3.387z"
        style="fill:var(--color3,#e60012)"></path>
      <path fill="#ffe200" d="M32 8.005h-32v15.99h32z" style="fill:var(--color5,#ffe200)"></path>
      <path fill="#e60012"
        d="M32 3.387v0c0-0.944-0.39-1.786-0.985-2.402-0.021-0.021-0.021-0.021-0.041-0.041-0.062-0.062-0.123-0.123-0.205-0.185-0.062-0.041-0.123-0.103-0.185-0.144-0.021-0.021-0.041-0.041-0.082-0.062-0.534-0.349-1.191-0.554-1.888-0.554h-25.226c-1.868 0-3.387 1.519-3.387 3.387v4.618h32v-4.618z"
        style="fill:var(--color3,#e60012)"></path>
    </symbol>
    <symbol id="icon-french" viewBox="0 0 32 32">
      <title>French</title>
      <path fill="#fff" d="M10.674 0v32h10.653v-32z" style="fill:var(--color2,#fff)"></path>
      <path fill="#0b318f"
        d="M3.387 0c-1.868 0-3.387 1.519-3.387 3.387v25.247c0 1.868 1.519 3.387 3.387 3.387h7.287v-32.021h-7.287z"
        style="fill:var(--color4,#0b318f)"></path>
      <path fill="#e60012"
        d="M32 28.613c0 0 0 0 0 0v-25.226c0 0 0 0 0 0 0-1.868-1.519-3.387-3.387-3.387h-7.287v32h7.287c1.868 0 3.387-1.519 3.387-3.387z"
        style="fill:var(--color3,#e60012)"></path>
    </symbol>
    <symbol id="icon-english" viewBox="0 0 32 32">
      <title>English</title>
      <path fill="#1d2088" d="M0 8.072v3.427h5.822l-5.822-4.356zM26.178 11.499h5.822v-4.356z"
        style="fill:var(--color1,#1d2088)"></path>
      <path fill="#1d2088"
        d="M20.315 22.359v9.641h9.621c0.083-0.041 0.165-0.083 0.248-0.124 0.041-0.021 0.083-0.041 0.124-0.062s0.083-0.041 0.124-0.083c0.041-0.021 0.083-0.062 0.124-0.083s0.062-0.041 0.103-0.083c0.041-0.041 0.083-0.062 0.145-0.103 0 0 0.021-0.021 0.021-0.021 0.124-0.103 0.227-0.206 0.33-0.33 0 0 0 0 0.021-0.021 0.041-0.062 0.103-0.124 0.145-0.186 0-0.021 0.021-0.021 0.021-0.041 0.041-0.062 0.083-0.124 0.124-0.186l-11.148-8.32zM0 20.212v4.356l5.822-4.356zM31.215 1.218c-0.619-0.743-1.548-1.218-2.601-1.218v0h-8.299v9.373l10.901-8.155zM11.685 0h-8.299c-1.053 0-1.982 0.475-2.601 1.218l10.901 8.155v-9.373zM11.685 22.359l-11.148 8.341c0 0 0 0 0 0 0.041 0.062 0.083 0.124 0.124 0.186 0 0.021 0.021 0.021 0.041 0.041 0.041 0.062 0.083 0.124 0.145 0.165 0 0.021 0.021 0.021 0.021 0.021 0.083 0.103 0.186 0.206 0.289 0.289 0.021 0.021 0.041 0.041 0.062 0.041 0.041 0.041 0.083 0.062 0.124 0.103 0.041 0.021 0.083 0.062 0.103 0.083 0.041 0.021 0.083 0.062 0.124 0.083s0.103 0.062 0.145 0.083c0.041 0.021 0.062 0.041 0.103 0.062 0.083 0.041 0.165 0.083 0.248 0.124h9.621v-9.621zM32 24.568v-4.356h-5.822z"
        style="fill:var(--color1,#1d2088)"></path>
      <path fill="#fff"
        d="M11.685 11.499v-2.126l-10.901-8.155c-0.495 0.599-0.785 1.363-0.785 2.188v0.475l10.178 7.618h1.507zM32 27.83l-10.178-7.618h-1.507v2.126l11.148 8.341c0.083-0.124 0.145-0.248 0.206-0.372 0-0.021 0.021-0.021 0.021-0.041 0.021-0.062 0.041-0.103 0.062-0.165 0.021-0.041 0.021-0.083 0.041-0.103 0.021-0.041 0.041-0.103 0.041-0.145 0.021-0.083 0.041-0.165 0.062-0.248 0-0.041 0.021-0.062 0.021-0.103 0.021-0.062 0.021-0.145 0.041-0.206 0-0.041 0-0.062 0.021-0.103 0-0.103 0.021-0.206 0.021-0.31v0-1.053zM7.267 11.499l-7.267-5.43v1.074l5.822 4.356zM24.733 20.212l7.267 5.45v-1.094l-5.822-4.356z"
        style="fill:var(--color2,#fff)"></path>
      <path fill="#e60012"
        d="M10.178 11.499l-10.178-7.597v2.168l7.267 5.43zM21.822 20.212l10.178 7.618v-2.168l-7.267-5.45z"
        style="fill:var(--color3,#e60012)"></path>
      <path fill="#fff"
        d="M20.315 10.467l11.355-8.485c-0.124-0.268-0.289-0.516-0.454-0.743l-10.901 8.134v1.094zM11.685 21.265l-11.52 8.63c0 0 0 0 0 0 0.041 0.124 0.083 0.268 0.145 0.392 0 0.021 0.021 0.041 0.021 0.062 0.062 0.124 0.124 0.248 0.186 0.351 0 0 0 0 0 0l11.148-8.341v-1.094zM26.178 11.499l5.822-4.356v-3.241l-10.178 7.597zM5.822 20.212l-5.822 4.356v3.262l10.178-7.618z"
        style="fill:var(--color2,#fff)"></path>
      <path fill="#e60012"
        d="M11.685 20.212h-1.507l-10.178 7.618v1.032c0 0.103 0 0.206 0.021 0.31 0 0.041 0 0.062 0.021 0.103 0 0.083 0.021 0.145 0.041 0.206 0 0.041 0.021 0.062 0.021 0.103 0.021 0.083 0.041 0.165 0.062 0.248 0 0.021 0 0.041 0.021 0.041 0 0 0 0 0 0l11.499-8.609v-1.053zM20.315 11.499h1.507l10.178-7.597v-0.475c0-0.516-0.124-1.012-0.33-1.445l-11.355 8.485v1.032z"
        style="fill:var(--color3,#e60012)"></path>
      <path fill="#fff"
        d="M11.685 11.499h-11.685v1.755h11.685zM20.315 20.212h11.685v-1.734h-11.685zM21.822 11.499h-1.507v1.755h11.685v-1.755h-5.822zM10.178 20.212h1.507v-1.734h-11.685v1.734h5.822zM18.601 18.477v13.523h1.714v-13.523zM11.685 11.499v1.755h1.714v-13.254h-1.714v9.373zM20.315 13.254v-13.254h-1.714v13.254zM11.685 18.477v13.523h1.714v-13.523z"
        style="fill:var(--color2,#fff)"></path>
      <path fill="#e60012"
        d="M13.399 18.477v13.523h5.203v-13.523h13.399v-5.223h-13.399v-13.254h-5.203v13.254h-13.399v5.223h11.685z"
        style="fill:var(--color3,#e60012)"></path>
    </symbol>
    <symbol id="icon-arrow-right" viewBox="0 0 18 32">
      <title>Arrow right</title>
      <path
        d="M3.713 0.441c-0.835-0.835-2.227-0.835-3.063 0s-0.835 2.227 0 3.063l12.345 12.438-12.345 12.345c-0.835 0.835-0.835 2.227 0 3.063 0.371 0.464 0.928 0.65 1.485 0.65s1.114-0.186 1.578-0.65l13.923-13.923c0.835-0.835 0.835-2.227 0-3.063l-13.923-13.923z">
      </path>
    </symbol>
    <symbol id="icon-arrow-left" viewBox="0 0 18 32">
      <title>Arrow left</title>
      <path
        d="M14.509 31.404c0.795 0.795 2.186 0.795 3.081 0s0.795-2.186 0-3.081l-12.323-12.323 12.224-12.323c0.795-0.795 0.795-2.186 0-3.081-0.298-0.398-0.894-0.596-1.391-0.596-0.596 0-1.093 0.199-1.59 0.596l-13.913 13.913c-0.795 0.795-0.795 2.186 0 3.081 0-0.099 13.913 13.814 13.913 13.814z">
      </path>
    </symbol>
    <symbol id="icon-arrow-up" viewBox="0 0 54 32">
      <title>Arrow up</title>
      <path
        d="M52.812 23.646l-22.229-22.229c-1.905-1.905-5.032-1.905-6.937 0l-22.229 22.229c-0.879 0.879-1.417 2.15-1.417 3.469s0.489 2.54 1.417 3.469c0.928 0.928 2.15 1.417 3.469 1.417s2.54-0.489 3.469-1.417l18.76-18.76 18.809 18.809c0.873 0.861 2.072 1.392 3.395 1.392s2.523-0.532 3.396-1.393l-0.001 0.001c1.905-1.905 1.954-5.032 0.098-6.986z">
      </path>
    </symbol>
    <symbol id="icon-arrow-down" viewBox="0 0 56 32">
      <title>Arrow down</title>
      <path
        d="M55.228 6.497c1.462-1.462 1.462-3.898 0-5.36s-3.898-1.462-5.36 0l-21.767 21.604-21.604-21.604c-1.462-1.462-3.898-1.462-5.36 0-0.812 0.65-1.137 1.624-1.137 2.599s0.325 1.949 1.137 2.761l24.365 24.365c1.462 1.462 3.898 1.462 5.36 0l24.365-24.365z">
      </path>
    </symbol>
    <symbol id="icon-wave-inverted" viewBox="0 0 80 32">
      <title>Wave inverted</title>
      <path
        d="M17.2 14.3c4.8 9.7 12.2 16.9 22.8 16.9s17.8-7.6 22.8-16.9c3.6-6.7 7.1-11.8 14-13.9 0.6-0.2 2-0.4 3.2-0.4h-80c0.8 0 1.7 0.2 2.4 0.4 6.7 2.1 10.8 5.7 14.8 13.9z">
      </path>
    </symbol>
    <symbol id="icon-wave" viewBox="0 0 80 32">
      <title>Wave</title>
      <path
        d="M0 0v32h80v-32c-1.2 0-2.576 0.214-3.2 0.4-6.868 2.049-10.4 7.2-13.998 13.892-5.005 9.308-12.17 16.946-22.802 16.946s-18-7.238-22.802-16.946c-4.036-8.161-8.077-11.781-14.798-13.892-0.728-0.229-1.6-0.4-2.4-0.4z">
      </path>
    </symbol>
    <symbol id="icon-instagram" viewBox="0 0 24 28">
      <title>Instagram</title>
      <path
        d="M16 14c0-2.203-1.797-4-4-4s-4 1.797-4 4 1.797 4 4 4 4-1.797 4-4zM18.156 14c0 3.406-2.75 6.156-6.156 6.156s-6.156-2.75-6.156-6.156 2.75-6.156 6.156-6.156 6.156 2.75 6.156 6.156zM19.844 7.594c0 0.797-0.641 1.437-1.437 1.437s-1.437-0.641-1.437-1.437 0.641-1.437 1.437-1.437 1.437 0.641 1.437 1.437zM12 4.156c-1.75 0-5.5-0.141-7.078 0.484-0.547 0.219-0.953 0.484-1.375 0.906s-0.688 0.828-0.906 1.375c-0.625 1.578-0.484 5.328-0.484 7.078s-0.141 5.5 0.484 7.078c0.219 0.547 0.484 0.953 0.906 1.375s0.828 0.688 1.375 0.906c1.578 0.625 5.328 0.484 7.078 0.484s5.5 0.141 7.078-0.484c0.547-0.219 0.953-0.484 1.375-0.906s0.688-0.828 0.906-1.375c0.625-1.578 0.484-5.328 0.484-7.078s0.141-5.5-0.484-7.078c-0.219-0.547-0.484-0.953-0.906-1.375s-0.828-0.688-1.375-0.906c-1.578-0.625-5.328-0.484-7.078-0.484zM24 14c0 1.656 0.016 3.297-0.078 4.953-0.094 1.922-0.531 3.625-1.937 5.031s-3.109 1.844-5.031 1.937c-1.656 0.094-3.297 0.078-4.953 0.078s-3.297 0.016-4.953-0.078c-1.922-0.094-3.625-0.531-5.031-1.937s-1.844-3.109-1.937-5.031c-0.094-1.656-0.078-3.297-0.078-4.953s-0.016-3.297 0.078-4.953c0.094-1.922 0.531-3.625 1.937-5.031s3.109-1.844 5.031-1.937c1.656-0.094 3.297-0.078 4.953-0.078s3.297-0.016 4.953 0.078c1.922 0.094 3.625 0.531 5.031 1.937s1.844 3.109 1.937 5.031c0.094 1.656 0.078 3.297 0.078 4.953z">
      </path>
    </symbol>
    <symbol id="icon-linkedin" viewBox="0 0 24 28">
      <title>LinkedIn</title>
      <path
        d="M5.453 9.766v15.484h-5.156v-15.484h5.156zM5.781 4.984c0.016 1.484-1.109 2.672-2.906 2.672v0h-0.031c-1.734 0-2.844-1.188-2.844-2.672 0-1.516 1.156-2.672 2.906-2.672 1.766 0 2.859 1.156 2.875 2.672zM24 16.375v8.875h-5.141v-8.281c0-2.078-0.75-3.5-2.609-3.5-1.422 0-2.266 0.953-2.641 1.875-0.125 0.344-0.172 0.797-0.172 1.266v8.641h-5.141c0.063-14.031 0-15.484 0-15.484h5.141v2.25h-0.031c0.672-1.062 1.891-2.609 4.672-2.609 3.391 0 5.922 2.219 5.922 6.969z">
      </path>
    </symbol>
    <symbol id="icon-facebook" viewBox="0 0 16 28">
      <title>Facebook</title>
      <path
        d="M14.984 0.187v4.125h-2.453c-1.922 0-2.281 0.922-2.281 2.25v2.953h4.578l-0.609 4.625h-3.969v11.859h-4.781v-11.859h-3.984v-4.625h3.984v-3.406c0-3.953 2.422-6.109 5.953-6.109 1.687 0 3.141 0.125 3.563 0.187z">
      </path>
    </symbol>
    <symbol id="icon-twitter" viewBox="0 0 26 28">
      <title>Twitter</title>
      <path
        d="M25.312 6.375c-0.688 1-1.547 1.891-2.531 2.609 0.016 0.219 0.016 0.438 0.016 0.656 0 6.672-5.078 14.359-14.359 14.359-2.859 0-5.516-0.828-7.75-2.266 0.406 0.047 0.797 0.063 1.219 0.063 2.359 0 4.531-0.797 6.266-2.156-2.219-0.047-4.078-1.5-4.719-3.5 0.313 0.047 0.625 0.078 0.953 0.078 0.453 0 0.906-0.063 1.328-0.172-2.312-0.469-4.047-2.5-4.047-4.953v-0.063c0.672 0.375 1.453 0.609 2.281 0.641-1.359-0.906-2.25-2.453-2.25-4.203 0-0.938 0.25-1.797 0.688-2.547 2.484 3.062 6.219 5.063 10.406 5.281-0.078-0.375-0.125-0.766-0.125-1.156 0-2.781 2.25-5.047 5.047-5.047 1.453 0 2.766 0.609 3.687 1.594 1.141-0.219 2.234-0.641 3.203-1.219-0.375 1.172-1.172 2.156-2.219 2.781 1.016-0.109 2-0.391 2.906-0.781z">
      </path>
    </symbol>
    <symbol id="icon-check" viewBox="0 0 42 32">
      <title>Check</title>
      <path
        d="M14.985 22.894l19.258-19.249c1.342-1.342 3.508-1.342 4.842 0v0c1.342 1.342 1.342 3.508 0 4.842l-19.258 19.258c-1.342 1.342-3.508 1.342-4.842 0v0c-1.342-1.342-1.342-3.508 0-4.851z">
      </path>
      <path
        d="M19.903 22.834l-10.967-10.967c-1.342-1.342-3.508-1.342-4.842 0l-0.085 0.085c-1.342 1.342-1.342 3.508 0 4.842l10.967 10.967c1.342 1.342 3.508 1.342 4.842 0l0.085-0.085c1.334-1.334 1.334-3.5 0-4.842z">
      </path>
    </symbol>
    <symbol id="icon-logo" viewBox="0 0 111 32">
      <title>Logo</title>
      <path
        d="M26.353 7.649c-0.205 0-0.411 0.051-0.633 0.171l-11.927 7.29-10.781-6.434 10.798-6.451 0.359 0.205c0.034 0.017 0.068 0.034 0.103 0.051l4.261 2.464c0.428 0.222 0.753 0.24 1.078 0.051l0.702-0.411c0.171-0.103 0.274-0.24 0.291-0.411 0.017-0.137-0.051-0.274-0.188-0.394-0.034-0.034-0.068-0.051-0.103-0.086l-4.004-2.31-1.917-1.112c-0.274-0.154-0.702-0.188-1.010-0.068-0.068 0.034-0.137 0.051-0.188 0.086l-6.314 3.799-5.852 3.508c-0.428 0.274-0.667 0.633-0.667 1.027-0.017 0.359 0.188 0.702 0.565 0.975l12.287 7.392c0.325 0.188 0.873 0.188 1.198 0l11.123-6.742v12.697l-11.739 7.016-11.739-7.016v-0.445c0-0.034-0.017-0.086-0.017-0.12v-3.884c0-0.394-0.188-0.719-0.496-0.89-0.034-0.034-0.068-0.051-0.103-0.086l-0.582-0.342c-0.051-0.034-0.086-0.051-0.137-0.068-0.291-0.103-0.548 0.051-0.599 0.342v6.024c0.051 0.359 0.308 0.736 0.582 0.924l12.458 7.427c0.154 0.103 0.376 0.154 0.599 0.154s0.445-0.051 0.599-0.154l12.458-7.461c0.342-0.205 0.599-0.667 0.599-1.078v-14.58c-0.017-0.496-0.411-1.061-1.061-1.061z">
      </path>
      <path
        d="M12.338 24.385l-2.721-1.574c-0.291-0.171-0.565 0.017-0.565 0.342v1.078c0 0.291 0.188 0.616 0.428 0.753l2.738 1.591c0.068 0.051 0.154 0.068 0.222 0.068 0.103 0 0.188-0.051 0.257-0.12 0.051-0.068 0.086-0.171 0.086-0.291v-1.095c-0.017-0.291-0.205-0.616-0.445-0.753z">
      </path>
      <path
        d="M12.766 21.134c0-0.291-0.188-0.616-0.428-0.753l-4.347-2.481c-0.291-0.171-0.565 0.017-0.565 0.342v1.078c0 0.291 0.188 0.616 0.428 0.753l4.347 2.481c0.068 0.051 0.154 0.068 0.222 0.068 0.103 0 0.188-0.051 0.257-0.12 0.051-0.068 0.086-0.171 0.086-0.291v-1.078z">
      </path>
      <path
        d="M54.383 3.97h-3.183v15.812c0 2.207 1.369 4.347 4.364 4.347h2.55v-2.704h-1.985c-1.147 0-1.745-0.667-1.745-1.797v-8.488h3.405v-2.824h-3.405v-4.347zM110.34 11.14v-2.824h-3.405v-4.347h-3.183v15.812c0 2.207 1.369 4.347 4.364 4.347h2.55v-2.704h-1.985c-1.147 0-1.745-0.667-1.745-1.797v-8.488h3.405zM43.534 14.785l-2.259-0.222c-1.66-0.154-2.208-0.753-2.208-1.728 0-1.249 0.89-2.071 2.995-2.071 1.369 0 2.806 0.342 3.885 1.164l1.985-1.985c-1.403-1.283-3.611-1.831-5.715-1.831-3.73 0-6.246 1.694-6.246 4.911 0 2.704 1.814 4.021 4.723 4.278l2.584 0.222c1.369 0.12 2.139 0.684 2.139 1.831 0 1.574-1.506 2.207-3.576 2.207-1.523 0-3.251-0.342-4.501-1.643l-2.173 2.036c1.745 1.797 4.090 2.293 6.417 2.293 4.312 0 6.982-1.694 6.982-5.099 0.017-2.721-1.745-4.039-5.031-4.364zM82.498 8.128c-2.738 0-4.398 0.684-5.835 2.327l2.105 1.951c0.993-1.129 1.985-1.54 3.662-1.54 2.481 0 3.32 0.873 3.32 2.96v0.787h-4.175c-4.021 0-6.024 2.105-6.024 4.74 0 3.149 2.55 4.911 5.356 4.911 2.259 0 3.662-0.53 4.877-1.831v1.609h3.149v-10.473c0.017-3.679-2.053-5.442-6.434-5.442zM95.675 8.317h-3.149v15.726h3.183v-7.632c0-3.422 2.396-5.373 5.202-5.373v-2.892c-1.985 0-3.885 0.787-5.236 2.036v-1.865zM69.972 18.635c0 1.472-1.301 2.584-3.029 2.584s-3.063-1.095-3.063-2.584v-10.319h-3.149v10.182c0 3.303 2.652 5.75 6.212 5.75 3.576 0 6.246-2.447 6.246-5.75v-10.182h-3.217v10.319zM85.75 17.78c0 2.396-2.139 3.645-4.056 3.645-1.591 0-3.063-0.53-3.063-2.105 0-1.42 1.147-2.207 3.44-2.207h3.662v0.667h0.017z">
      </path>
    </symbol>
  </defs>
</svg>
<div class="wrapper-contact wrapper-career hide" fifth second>
  <div class="overlay" fifth></div>
  <div class="contact-popup" fifth>
    <div class="header-contact align-center has-bg bg-primary-medium" fifth>
      <div fifth>
        <h3>Apply with GetGoLogistics</h3>
      </div>
      <svg class="icon icon-wave-inverted" fifth>
        <use xlink:href="#icon-wave-inverted" fifth></use>
      </svg>
      <button aria-label="Close the form" role="button" type="button" class="btn-close" fifth>
        <span class="visually-hidden" fifth>Close the form</span>
        <svg class="icon icon-cross" fifth>
          <use xlink:href="#icon-cross" fifth></use>
        </svg>
      </button>
    </div>
    <div class="content-contact" fifth>
      <div sixth fifth>
        <form action="http://192.168.1.178:8082/auth/lead/create" method="POST" role="form"
          class="form form--contact-us" sixth modelAttribute="leadRequest">
          <div class="field double" sixth><label for="name" class="required" seven sixth><input name="name" id="name"
                placeholder="Full Name" required type="text" seven>
            </label>
            <label for="email" class="required" seven sixth><input name="email" id="email" required type="email"
                placeholder="Email ID" seven>
            </label>
          </div>
          <div class="field double" sixth>
            <label for="Phone" class="required" seven sixth><input name="contact_number" id="Phone"
                placeholder="Contact Number" required type="tel" seven></label>
            <label for="companyType" class="select" seven sixth>
              <select aria-label="companyType" name="companyType" id="companyType" seven>
                <option disabled hidden selected seven>Interested in which position</option>
                <option value="Sole_Propreitorship" seven>
                  Sole Propreitorship
                </option>
                <option value="Pvt_Ltd" seven>
                  Pvt Ltd
                </option>
                <option value="Partnership" seven>
                  Partnership
                </option>
                <option value="LLP" seven>
                  LLP
                </option>
                <option value="Not_registered" seven>
                  Not registered
                </option>
              </select>
            </label>
            </label>
          </div>
          <div class="field" sixth><label for="company" class="required" seven sixth><input name="company_name"
                id="company" placeholder="Current CTC" required type="text" seven></label>
          </div>
          <div class="field" sixth><label for="company" class="required" seven sixth><input name="company_name"
                id="company" required type="file" seven></label>
          </div>
      </div>
      <div class="wrapper-submit" sixth>
        <button type="submit" class="btn huge full-width btn-arrow" sixth>
          <span sixth>Submit</span>
          <svg class="icon icon-arrow-right" sixth>
            <use xlink:href="#icon-arrow-right" sixth></use>
          </svg>
        </button>
      </div>
      <!---->
      </form>
    </div>
  </div>
  <!---->
</div>
</div>
<?php include("footer.php"); ?>
<script>
  //$countElm =  $('.slides[data-v-cd71eb18] .slide[data-v-cd71eb18]').index;
  $(document).ready(function () {
    if ($(window).width() > 780) {
      $(window).on('load resize scroll', function () {
        $myElem = $('.slide-img[data-v-cd71eb18]');
        $scroll = $('.getHeight').outerHeight();
        $Elem = parseInt($('.slides[data-v-cd71eb18] .slide[data-v-cd71eb18]:first-child').outerHeight());
        var scrollTop = parseInt($(this).scrollTop());
        if (scrollTop >= $scroll - 50) {
          $('.slides[data-v-cd71eb18] .slide[data-v-cd71eb18] .slide-img[data-v-cd71eb18]')
            .removeClass('fixed').css({
              'opacity': '0',
              'justify-content': 'flex-end',
              'z-index': 2
            });
          $('.slides[data-v-cd71eb18] .slide[data-v-cd71eb18]:first-child .slide-img[data-v-cd71eb18]')
            .addClass(
              'fixed').css({
              'opacity': '1',
              'justify-content': 'flex-end',
              'background': 'rgb(17, 163, 235)',
              'z-index': 3
            });
        }
        if (scrollTop >= ($scroll + $Elem - 250)) {
          $('.slides[data-v-cd71eb18] .slide[data-v-cd71eb18] .slide-img[data-v-cd71eb18]')
            .removeClass('fixed').css({
              'opacity': '0',
              'justify-content': 'flex-end',
              'z-index': 2
            });
          $('.slides[data-v-cd71eb18] .slide[data-v-cd71eb18]:nth-child(2) .slide-img[data-v-cd71eb18]')
            .addClass(
              'fixed').css({
              'opacity': '1',
              'justify-content': 'flex-end',
              'background': 'rgb(255, 129, 117)',
              'z-index': 9
            });
        }
        if (scrollTop >= ($scroll + ($Elem * 2) - 250)) {
          $('.slides[data-v-cd71eb18] .slide[data-v-cd71eb18] .slide-img[data-v-cd71eb18]')
            .removeClass('fixed').css({
              'opacity': '0',
              'justify-content': 'flex-end',
              'z-index': 2
            });
          $('.slides[data-v-cd71eb18] .slide[data-v-cd71eb18]:nth-child(3) .slide-img[data-v-cd71eb18]')
            .addClass(
              'fixed').css({
              'opacity': '1',
              'justify-content': 'flex-end',
              'background': 'rgb(247, 187, 23)',
              'z-index': 9
            });
        }
        if (scrollTop > ($scroll + ($Elem * 2) - 100)) {
          $('.slides[data-v-cd71eb18] .slide[data-v-cd71eb18] .slide-img[data-v-cd71eb18]')
            .removeClass('fixed').css({
              'opacity': '0',
              'justify-content': 'flex-end',
              'z-index': 2
            });
          $('.slides[data-v-cd71eb18] .slide[data-v-cd71eb18]:nth-child(3) .slide-img[data-v-cd71eb18]')
            .removeClass('fixed').css({
              'opacity': '1',
              'justify-content': 'flex-end',
              'background': 'rgb(247, 187, 23)',
              'bottom': '0',
              'top': 'auto',
              'z-index': 9
            });
        }
        if (scrollTop <= $scroll) {
          $('.slides[data-v-cd71eb18] .slide[data-v-cd71eb18] .slide-img[data-v-cd71eb18]')
            .removeClass('fixed').css({
              'opacity': '0',
              'justify-content': 'flex-end',
              'z-index': 2
            });
          $('.slides[data-v-cd71eb18] .slide[data-v-cd71eb18]:first-child .slide-img[data-v-cd71eb18]')
            .removeClass('fixed').css({
              'opacity': '1',
              'justify-content': 'flex-end',
              'background': 'rgb(17, 163, 235)',
              'z-index': 3
            });
        }
      });
    }
  });

  var flag = true;
  $('.wrapper-slider .slides[data-v-fb673edc] li').click(function () {
    $('.wrapper-slider .slides[data-v-fb673edc] li').removeClass('active')
    $(this).addClass('active');
    flag = false;
  })
  if (flag) {
    setInterval(function () {
      var cur = $('.wrapper-slider .slides[data-v-fb673edc] li.active');
      if (cur.index() == $('.wrapper-slider .slides[data-v-fb673edc] li').length - 1) {
        cur.removeClass('active');
        $('.wrapper-slider .slides[data-v-fb673edc] li:first').addClass('active');
      } else {
        cur.removeClass('active').next().addClass('active');
      }
    }, 3000);
  }

  var demo = {angle: 0}
const store = {
  debug: true,
  state: {
    ang: 0,
    dist: 0,
    mov: TweenLite.to(demo, 700, {angle:360, repeat: -1, onUpdate: function () {
      store.state.ang = demo.angle
    }}),
    ecosystem: [
      {name: 'EXPRESS CARGO'},
      {name: 'REVERSE SHIPPING'},
      {name: 'PART LOAD'},
      {name: 'E-COMMERCE'}
    ]
  }
}
const home = {
  template: `<z-view class="mainC">

  <span><img src="img/TestimonialCouriersITC.png" width="70%" height="70%" /></span>
  
  <section slot="extension">

    <z-spot 
      v-for="(element, index) in ecosystem"
      class="inactive"
      :angle="(360 / ecosystem.length * index) + sharedState.ang * 4"
      :distance="80"
      size="medium"
      :to-view="element.viewName"
      :label="element.name"
      :key="index">


    </z-spot>

  </section>

</z-view>`,
  data () {
    return {
      sharedState: store.state,
      ecosystem: store.state.ecosystem
    }
  },
  updated () {
    var vm = this
    this.$nextTick(function () {
      if (vm.$el.classList.contains('is-previous-view') || vm.$el.classList.contains('is-past-view')) {
        vm.sharedState.mov.pause()
      }
    })
  }
}

new Vue({
  el: '#app',
  components: {
    home
  },
  mounted () {
    this.$zircle.setView('home')
  }
});
</script>