$('.modalShow').click(function () {
  $('.wrapper-contact-modal').removeClass('hide');
  $('body').css('overflow', 'hidden');
});
$('.wrapper-contact-modal .btn-close').click(function () {
  $('.wrapper-contact-modal').addClass('hide');
  $('body').css('overflow', 'visible');
});

$('.careerShow').click(function () { 
  $('.wrapper-career').removeClass('hide');
  $('body').css('overflow', 'hidden');
});
$('.wrapper-career .btn-close').click(function () {
  $('.wrapper-career').addClass('hide');
  $('body').css('overflow', 'visible');
});

$('.contactShow').click(function () {
  $('.wrapper-contact-us').removeClass('hide');
  $('body').css('overflow', 'hidden');
});
$('.wrapper-contact-us .btn-close').click(function () {
  $('.wrapper-contact-us').addClass('hide');
  $('body').css('overflow', 'visible');
});

$(window).scroll(function () {
  var scroll = $(window).scrollTop();
  if (scroll >= 100) {
    $("header").addClass("fixed");
  } else {
    $("header").removeClass("fixed");
  }
});

(function ($) {
  //"use strict";
  $.fn.sliderResponsive = function (settings) {

    var set = $.extend({
        // slidePause: 5000,
        fadeSpeed: 800,
        autoPlay: "on",
        showArrows: "off",
        hideDots: "off",
        hoverZoom: "on",
        titleBarTop: "off"
      },
      settings
    );

    var $slider = $(this);
    var size = $slider.find("> div").length; //number of slides
    var position = 0; // current position of carousal
    var sliderIntervalID; // used to clear autoplay

    // Add a Dot for each slide
    $slider.append("<ul></ul>");
    $slider.find("> div").each(function () {
      $slider.find("> ul").append('<li></li>');
    });

    // Put .show on the first Slide
    $slider.find("div:first-of-type").addClass("show");

    // Put .showLi on the first dot
    $slider.find("li:first-of-type").addClass("showli")

    //fadeout all items except .show
    $slider.find("> div").not(".show").fadeOut();

    // If Autoplay is set to 'on' than start it
    // if (set.autoPlay === "on") {
    //   startSlider();
    // }

    // If showarrows is set to 'on' then don't hide them
    if (set.showArrows === "on") {
      $slider.addClass('showArrows');
    }

    // If hideDots is set to 'on' then hide them
    if (set.hideDots === "on") {
      $slider.addClass('hideDots');
    }

    // If hoverZoom is set to 'off' then stop it
    if (set.hoverZoom === "off") {
      $slider.addClass('hoverZoomOff');
    }

    // If titleBarTop is set to 'on' then move it up
    if (set.titleBarTop === "on") {
      $slider.addClass('titleBarTop');
    }

    // // function to start auto play
    // function startSlider() {
    //   sliderIntervalID = setInterval(function () {
    //     nextSlide();
    //   }, 0);
    // }

    //on right arrow click
    $slider.find("> .right").click(nextSlide)

    //on left arrow click
    $slider.find("> .left").click(prevSlide);

    // Go to next slide
    function nextSlide() {
      position = $slider.find(".show").index() + 1;
      if (position > size - 1) position = 0;
      changeCarousel(position);
    }

    // Go to previous slide
    function prevSlide() {
      position = $slider.find(".show").index() - 1;
      if (position < 0) position = size - 1;
      changeCarousel(position);
    }

    //when user clicks slider button
    $slider.find(" > ul > li").click(function () {
      position = $(this).index();
      changeCarousel($(this).index());
    });

    //this changes the image and button selection
    function changeCarousel() {
      $slider.find(".show").removeClass("show").fadeOut();
      $slider
        .find("> div")
        .eq(position)
        .fadeIn(set.fadeSpeed)
        .addClass("show");
    }



    var TxtType = function (el, toRotate, period) {
      this.toRotate = toRotate;
      this.el = el;
      this.loopNum = 0;
      this.period = parseInt(period, 10) || 2000;
      this.txt = '';
      this.tick();
      this.isDeleting = false;
    };
    
    TxtType.prototype.tick = function () {
      var colorList = ['rgb(207, 184, 130)', 'rgb(126, 106, 88)', 'rgb(50, 64, 122)', 'rgb(101, 126, 100)', '#105285'];
      var i = this.loopNum % this.toRotate.length;
      var fullTxt = this.toRotate[i];
    
      if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
      } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
      }
    
      this.el.innerHTML = '<span class="wrap changeColor">' + this.txt + '</span>';
      $('.changeColor').css('color', colorList[i]);
    
      // var lengthtext = this.txt.length;
      // var timeset = lengthtext * 1500;
     
      var that = this;
      var delta = 200 - Math.random() * 100;
    
      if (this.isDeleting) {
        delta /= 2;
      }
    
      
      if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
      } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        nextSlide();
        
      }
      setTimeout(function () {
        that.tick();
      }, delta);
    };
    
    window.onload = function () {
      var is_sign_up = getUrlParam('is_sign_up','');
      console.log('is_sign_up',is_sign_up);
      if(is_sign_up == 'true'){
        console.log('is_sign_up',is_sign_up);
        $(".modalShow").click();
      }
      var elements = document.getElementsByClassName('typewrite');
      for (var i = 0; i < elements.length; i++) {
        var toRotate = elements[i].getAttribute('data-type');
        var period = elements[i].getAttribute('data-period');
        if (toRotate) {
          new TxtType(elements[i], JSON.parse(toRotate), period);
        }
      }
      // INJECT CSS
      var css = document.createElement("style");
      css.type = "text/css";
      css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
      document.body.appendChild(css);
    };
    
      return $slider;
  };
  
})(jQuery);

//////////////////////////////////////////////
// Activate each slider - change options
//////////////////////////////////////////////

$('.owl-carousel').owlCarousel({
  loop: true,
  nav: false,
  stagePadding: 80,
  margin: 25,
  items: 4,
  responsive: {
    0: {
      items: 1,
      stagePadding: 40,
    },
    600: {
      items: 2,
      stagePadding: 40
    },
    1000: {
      items: 3,
      stagePadding: 80
    },
    1300: {
      items: 3,
      margin: 30,
      stagePadding: 120
    },
    1400: {
      items: 4,
      stagePadding: 50,
      margin: 25
    },
    1600: {
      items: 5,
      stagePadding: 20,
    }
  }
});


$("#slider3").sliderResponsive({
  hoverZoom: "off",
  hideDots: "on",
  // slidePause: 5000
});

if ($(window).width() < 1024){
  $('.burger').click(function(){
    $('.wrapper-content.nte').toggleClass('open');
    $('header').toggleClass('open');
  });
  $('.menu-link.has-submenu[third]').addClass('closed');
  $('.menu-link.has-submenu[third]').click(function(){
    $('.menu-link.has-submenu[third]').addClass('closed');  
    $(this).toggleClass('closed');
  })
}
$('button.btn-product').on('click', function(){
  $('button.btn-product').removeClass('active');
  $(this).addClass('active');
  var data_id = $(this).data('id');
  $('.productApi').removeClass('active');
  $('#'+data_id).addClass('active');
});

$('.back-to-top-button').click(function () {
  $("html, body").animate({
      scrollTop: 0
  }, 1000);
  return false;
});


// function checkboxDropdown(el) {
//   var $el = $(el)

//   function updateStatus(label, result) {
//     if(!result.length) {
//       label.html();
//     }
//   };
  
//   $el.each(function(i, element) {
//     var $list = $(this).find('.dropdown-list'),
//       $label = $(this).find('.dropdown-label'),
//       $checkAll = $(this).find('.check-all'),
//       $inputs = $(this).find('.check'),
//       defaultChecked = $(this).find('input[type=checkbox]:checked'),
//       result = [];
    
//     updateStatus($label, result);
//     if(defaultChecked.length) {
//       defaultChecked.each(function () {
//         result.push($(this).next().text());
//         $label.html(result.join(", "));
//       });
//     }
    
//     $label.on('click', ()=> {
//       $(this).toggleClass('open');
//     });

//     $checkAll.on('change', function() {
//       var checked = $(this).is(':checked');
//       var checkedText = $(this).next().text();
//       result = [];
//       if(checked) {
//         result.push(checkedText);
//         $label.html(result);
//         $inputs.prop('checked', false);
//       }else{
//         $label.html(result);
//       }
//         updateStatus($label, result);
//     });

//     $inputs.on('change', function() {
//       var checked = $(this).is(':checked');
//       var checkedText = $(this).next().text();
//       if($checkAll.is(':checked')) {
//         result = [];
//       }
//       if(checked) {
//         result.push(checkedText);
//         $label.html(result.join(", "));
//         $checkAll.prop('checked', false);
//       }else{
//         let index = result.indexOf(checkedText);
//         if (index >= 0) {
//           result.splice(index, 1);
//         }
//         $label.html(result.join(", "));
//       }
//       updateStatus($label, result);
//     });

//     $(document).on('click touchstart', e => {
//       if(!$(e.target).closest($(this)).length) {
//         $(this).removeClass('open');
//       }
//     });
//   });
// };

// checkboxDropdown('.dropdown');

 $('.toggles[data-v-5254fdbf] .toggle[data-v-5254fdbf]').click(function(){
  $('.toggles[data-v-5254fdbf] .toggle[data-v-5254fdbf]').not(this).removeClass('open');
   $(this).toggleClass('open');
 });

// $('.clickNext').click(function(){
//   $('body').addClass('activeAbout');
//   setTimeout(function(){
//     $('body').removeClass('activeAbout');
//   }, 500)
// });

function trackShipment( ) {
  $('#span-tracking-error').css("display","none");
  $('#section-track-shipment').css("display","none");

  $.ajax({
    url: "https://apicouriers.getgologistics.com/search/couriers/shipment/track/"+$('#tracking-number-id').val(),
    type: "GET",
    success: function (data) {
      if(data.success) {
        var trackingStatusObject = data.trackingStatus;
        setData(data.tracking_number ,trackingStatusObject);
      } else {
        $('#span-tracking-error').html(data.message);
        $('#span-tracking-error').css("display","block");
      }
    },
    error: function (xhr, status, error) {
      $('#span-tracking-error').html(xhr.responseJSON.message);
      $('#span-tracking-error').css("display","block");
      console.log("error "+error);
    },
  });
}

function setData(trackingNumber, trackingStatusObject) {
  var currentTrackingStatus = trackingStatusObject[0].current_tracking_status;
  var statusTimeMilliseconds = currentTrackingStatus.status_date_time;
  var statusDate = convertMillisToDate(statusTimeMilliseconds);
  var trackingHistory = trackingStatusObject[0].tracking_history;
  $('#span-tracking-number').html("Tracking number: "+trackingNumber);
  $('#tracking-status').html(currentTrackingStatus.status);
  $('#tracking-status-date').text(statusDate);

  for(var i=0; i<trackingHistory.length; i++) {
    var trackHistory = trackingHistory[i];
    var status = trackHistory.status;
    var statusDateTime = convertMillisToDate(trackHistory.status_date_time);
    var location = trackHistory.location == null ? '' : trackHistory.location;
    var messageDetail = trackHistory.message_detail == null ? '' : "("+trackHistory.message_detail+")";
    var classStatus = "";
    
    if(currentTrackingStatus.status === status ) {
      classStatus = "expired_status";
    } else {
      classStatus = "";
    }

    var liTrackHistory = '<li class="active" style=""><div class="ng-scope">'+
    '<div class="timeline-icon '+ classStatus +'"><i class="fa fa-dot-circle-o"></i></div>'+
    '<div class="timeline-time">'+statusDateTime+'</div>'+
    '<div class="timeline-content"><p class="push-bit"><strong class="">'+status+
    ' <em class="timeline-detail">'+messageDetail+
    '</em></strong></p><p class="push-bit colorGray">'+location+
    '</p></div></div></li>';

    $('#timeline-track-status').append(liTrackHistory);
  }

  $('#section-track-shipment').css("display","block");
}

function convertMillisToDate(millis) {
  var date = new Date(millis);
  statusDate = date.toDateString();

  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12;
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  statusDate = statusDate + " " + strTime;
  
  return statusDate;
}
$(document).ready(function(){
  $('.successMsg').hide();
  $('.wrapper-contact-us .btn-close').click(function(){
    $('.wrapper-contact-us .succHide').show();
    $('.wrapper-contact-us .successMsg').hide();
    $('.wrapper-contact-us form input').val('');
  })
  $('.wrapper-career .btn-close').click(function(){
    $('.wrapper-career .succHide').show();
    $('.wrapper-career .successMsg').hide();
    $('.wrapper-career form input').val('');
  })
  $('.wrapper-contact-modal .btn-close').click(function(){
    $('.wrapper-contact-modal .succHide').show();
    $('.wrapper-contact-modal .successMsg').hide();
    $('.wrapper-contact-modal form input').val('');
  })
});

$(document).ready(function(){
  $('.wrapper-contact-us .overlay').click(function(){
    $('.wrapper-contact-us').addClass('hide');
    $('.wrapper-contact-us .succHide').show();
    $('.wrapper-contact-us .successMsg').hide();
    $('.wrapper-contact-us form input').val('');
  })
  $('.wrapper-career .overlay').click(function(){
    $('.wrapper-career').addClass('hide');
    $('.wrapper-career .succHide').show();
    $('.wrapper-career .successMsg').hide();
    $('.wrapper-career form input').val('');
  })
  $('.wrapper-contact-modal .overlay').click(function(){
    $('.wrapper-contact-modal').addClass('hide');
    $('.wrapper-contact-modal .succHide').show();
    $('.wrapper-contact-modal .successMsg').hide();
    $('.wrapper-contact-modal form input').val('');
  })
});

function setLogoInterval(){
  setTimeout(function () {
    $('.wrapper-logos li img').css('animation', 'trnaStateOut 0.5s ease-in-out');
  }, 2600);

  setTimeout(function () {
    // var first = $('.wrapper-logos>li[data-v-d7eeecfc]:first-child');
    var first = $('.wrapper-logos>li:eq(0), .wrapper-logos>li:eq(1), .wrapper-logos>li:eq(2), .wrapper-logos>li:eq(3)');
    $('.wrapper-logos').append(first);
    $('.wrapper-logos li img').css('animation', 'trnaStateIn 0.5s ease-in-out');
  }, 3000);
}
setInterval(function () {
  setLogoInterval();
}, 3000);

$('#createUserForm').submit(function(){
  $('.loaderSign').addClass('active');
  var utmSource = getUrlParam('utm_source','');
  var utmMedium = getUrlParam('utm_medium','');
  var utmCampaign = getUrlParam('utm_campaign','');
  var referralUrl = getUrlParam('referral_url','');
	
	var error = false;
	var only_letters = /^[a-zA-Z ]*$/;
	var only_letters_numbers = /^[a-zA-Z0-9 ]*$/;
	var email_rules = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var regMobile = /^([6-9]{1})([0-9]{9})$/i;
	
	document.getElementById("createUserFormError").innerHTML = '';	
	document.getElementById("nameError").innerHTML = '';
	document.getElementById("emailError").innerHTML = '';
	document.getElementById("contact_numberError").innerHTML = '';
	document.getElementById("company_nameError").innerHTML = '';
	// document.getElementById("cityError").innerHTML = '';
	// document.getElementById("current_logistics_partnerError").innerHTML = '';
	document.getElementById("passwordError").innerHTML = '';
	
	
	var name = document.createUserForm.name.value;
	var email = document.createUserForm.email.value;
	var phone = document.createUserForm.contact_number.value;
	var company = document.createUserForm.company_name.value;
	// var city = document.createUserForm.city.value;
	var password = document.createUserForm.password.value;
	// var current_logistics_partner = document.createUserForm.current_logistics_partner.value;
  console.log(error);
	if (!only_letters.test(name)) {
		document.getElementById("nameError").innerHTML = 'Only alphabets allowed';
		error = true;
	}	
	else if(name.length > 30) {
		document.getElementById("nameError").innerHTML = 'Max 30 letters';
		error = true;
	}
	
	if(!email_rules.test(String(email).toLowerCase())){
		document.getElementById("emailError").innerHTML = 'Please enter valid email address';
		error = true;
	}
	
	if(!regMobile.test(phone)){
		document.getElementById("contact_numberError").innerHTML = 'Enter valid phone number';
		error = true;
	}
	else if(phone.length != 10) {
		document.getElementById("contact_numberError").innerHTML = 'Phone must contain 10 digits';
		error = true;
	}
	
	if(!only_letters_numbers.test(company)){
		document.getElementById("company_nameError").innerHTML = 'Only letters & numbers allowed';
		error = true;
	}
	else if(company.length > 30) {
		document.getElementById("company_nameError").innerHTML = 'Max 30 letters allowed';
		error = true;
	}
	
	// if (!only_letters.test(city)) {
	// 	document.getElementById("cityError").innerHTML = 'Only alphabets allowed';
	// 	error = true;
	// }	
	// else if(city.length > 30) {
	// 	document.getElementById("cityError").innerHTML = 'Max 30 letters allowed';
	// 	error = true;
	// }
	
	// if (!only_letters.test(current_logistics_partner)) {
	// 	document.getElementById("current_logistics_partnerError").innerHTML = 'Only alphabets allowed';
	// 	error = true;
	// }	
	// else if(current_logistics_partner.length > 20) {
	// 	document.getElementById("current_logistics_partnerError").innerHTML = 'Max 20 letters allowed';
	// 	error = true;
	// }
	
	if(password.length < 6){
		document.getElementById("passwordError").innerHTML = 'Password should contain minimum 6 digits';
		error = true;
	}
  
  console.log(error);
	if(error == true){
    $('.loaderSign').removeClass('active');
		return false;
	}
	else{
		document.getElementById("createUserFormError").innerHTML = '';	
		document.getElementById("nameError").innerHTML = '';
		document.getElementById("emailError").innerHTML = '';
		document.getElementById("contact_numberError").innerHTML = '';
		document.getElementById("company_nameError").innerHTML = '';
		// document.getElementById("cityError").innerHTML = '';
		// document.getElementById("current_logistics_partnerError").innerHTML = '';
		document.getElementById("passwordError").innerHTML = '';
		
    var dataForm = $('#createUserForm').serializeArray();
    dataForm.push({name: 'utmSource', value: utmSource});
    dataForm.push({name: 'utmMedium', value: utmMedium});
    dataForm.push({name: 'utmCampaign', value: utmCampaign});
    dataForm.push({name: 'referralUrl', value: referralUrl});
			
		$.ajax({
		  url: $('#createUserForm').attr('action'),
		  type: 'POST',
		  data : dataForm,
		  success: function(response){
     $('.loaderSign').removeClass('active');
			console.log(response);
			if(response.status == 'success'){
          var encodedEmail = btoa(email);
          var encodedPwd = btoa(password);
          var redUrl = "u="+encodedEmail+"&w="+encodedPwd;
          var baseUrl = "https://accounts.getgologistics.com/auth/web/auto/login?";
//				$('.wrapper-contact-modal').removeClass('hide');
//				$('.wrapper-contact-modal .succHide').hide();
//				$('.wrapper-contact-modal .successMsg').show();
          window.location.href = baseUrl+redUrl;
			}
			else{
				document.getElementById("createUserFormError").innerHTML = response.message;
				document.getElementById('createUserFormDiv').scrollIntoView();
			}
		  }
		});
	}	
    return false;
});

function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = value;
  });
  return vars;
}

function getUrlParam(parameter, defaultvalue){
  var urlparameter = defaultvalue;
  console.log("parameter "+parameter);
  if(window.location.href.indexOf(parameter) > -1){
      urlparameter = getUrlVars()[parameter];
      console.log("found "+urlparameter);
  }
  return urlparameter;
}

$('#getInTouchForm').submit(function(){
  var utmSource = getUrlParam('utm_source','');
  var utmMedium = getUrlParam('utm_medium','');
  var utmCampaign = getUrlParam('utm_campaign','');
  var referralUrl = getUrlParam('referral_url','');
	
	var error = false;
	var only_letters = /^[a-zA-Z ]*$/;
	var only_letters_numbers = /^[a-zA-Z0-9 ]*$/;
	var email_rules = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var regMobile = /^([6-9]{1})([0-9]{9})$/i;
	
	document.getElementById("getInTouchFormError").innerHTML = '';	
	document.getElementById("nameGError").innerHTML = '';
	document.getElementById("emailGError").innerHTML = '';
	document.getElementById("contactNumberError").innerHTML = '';
	document.getElementById("companyNameError").innerHTML = '';
	
	var name = document.getInTouchForm.name.value;
	var email = document.getInTouchForm.email.value;
	var phone = document.getInTouchForm.contactNumber.value;
	var company = document.getInTouchForm.companyName.value;


	if (!only_letters.test(name)) {
		document.getElementById("nameGError").innerHTML = 'Only alphabets allowed';
		error = true;
	}	
	else if(name.length > 30) {
		document.getElementById("nameGError").innerHTML = 'Max 30 letters allowed';
		error = true;
	}
	
	if(!email_rules.test(String(email).toLowerCase())){
		document.getElementById("emailGError").innerHTML = 'Please enter valid email address';
		error = true;
	}
	
	if(!regMobile.test(phone)){
		document.getElementById("contactNumberError").innerHTML = 'Enter valid phone number';
		error = true;
	}
	else if(phone.length != 10) {
		document.getElementById("contactNumberError").innerHTML = 'Phone must contain 10 digits';
		error = true;
	}
	
	if(!only_letters_numbers.test(company)){
		document.getElementById("companyNameError").innerHTML = 'Only letters & numbers allowed';
		error = true;
	}
	else if(company.length > 30) {
		document.getElementById("companyNameError").innerHTML = 'Max 30 letters allowed';
		error = true;
	}


	if(error == true){
		return false;
	}
	else{
		document.getElementById("getInTouchFormError").innerHTML = '';	
		document.getElementById("nameGError").innerHTML = '';
		document.getElementById("emailGError").innerHTML = '';
		document.getElementById("contactNumberError").innerHTML = '';
    document.getElementById("companyNameError").innerHTML = '';

    var dataForm = $('#getInTouchForm').serializeArray();
    dataForm.push({name: 'utmSource', value: utmSource});
    dataForm.push({name: 'utmMedium', value: utmMedium});
    dataForm.push({name: 'utmCampaign', value: utmCampaign});
    dataForm.push({name: 'referralUrl', value: referralUrl});

		$.ajax({
		  url: $('#getInTouchForm').attr('action'),
		  type: 'POST',
		  data : dataForm,
		  success: function(response){
			console.log(response);
			if(response.status == 'success'){
				console.log(response);
				$('.wrapper-contact-us').removeClass('hide');
				$('.wrapper-contact-us .succHide').hide();
				$('.wrapper-contact-us .successMsg').show();
			}
			else{
				document.getElementById("getInTouchFormError").innerHTML = response.message;
				document.getElementById('getInTouchFormDiv').scrollIntoView();
			}
		  }
		});
	}	
    return false;
});

$('#documentFormCV').submit(function(){ 

	var error = false;
	var only_letters = /^[a-zA-Z ]*$/;
	var only_letters_numbers = /^[a-zA-Z0-9 ]*$/;
	var email_rules = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var regMobile = /^([6-9]{1})([0-9]{9})$/i;
	
	document.getElementById("carrerFormError").innerHTML = '';	
	document.getElementById("nameCError").innerHTML = '';
	document.getElementById("emailCError").innerHTML = '';
	document.getElementById("contactNumberCError").innerHTML = '';
  document.getElementById("documentError").innerHTML = '';
  
	var name = document.carrerForm.name.value;
	var email = document.carrerForm.email.value;
	var phone = document.carrerForm.contactNumber.value;
	var positionApplied = document.carrerForm.positionApplied.value;
  
	if (!only_letters.test(name)) {
		document.getElementById("nameCError").innerHTML = 'Only alphabets allowed';
		error = true;
	}	
	else if(name.length > 30) {
		document.getElementById("nameCError").innerHTML = 'Max 30 letters allowed';
		error = true;
	}
	
	if(!email_rules.test(String(email).toLowerCase())){
		document.getElementById("emailCError").innerHTML = 'Please enter valid email address';
		error = true;
	}
	
	if(!regMobile.test(phone)){
		document.getElementById("contactNumberCError").innerHTML = 'Enter valid phone number';
		error = true;
	}
	else if(phone.length != 10) {
		document.getElementById("contactNumberCError").innerHTML = 'Phone must contain 10 digits';
		error = true;
	}
	
	if(positionApplied == '' || positionApplied == undefined || positionApplied == null){
		document.getElementById("positionAppliedError").innerHTML = 'Please select position';
		error = true;
	}
  
  var documentFile = document.documentForm.document.value;
  if(documentFile == null || documentFile == "") {
    document.getElementById("documentError").innerHTML = 'Please select file for resume';
		error = true;
  }

	if(error == true){
		return false;
	} else{
		document.getElementById("carrerFormError").innerHTML = '';	
		document.getElementById("nameCError").innerHTML = '';
		document.getElementById("emailCError").innerHTML = '';
    document.getElementById("contactNumberCError").innerHTML = '';
    document.getElementById("documentError").innerHTML = '';
    
    $.ajax({
      url: $('#documentFormCV').attr('action'),
      type: 'POST',
      data :  $('#carrerForm').serializeArray(),
      success: function(response){
        if(response.status == 'success'){
          var id = response.id;
          var form = $('#documentFormCV')[0];
          var formData = new FormData(form);
        
          if(id != 0) {
            $.ajax({
              url: 'https://accounts.getgologistics.com/auth/'+id+'/upload',
              type: 'POST',
              enctype: 'multipart/form-data',
              processData: false,
              contentType: false,
              cache: false,
              timeout: 600000,
              data : formData,
              success: function(response){
                console.log(response);
                if(response.status == 'success'){
                  $('.wrapper-career').removeClass('hide');
                  $('.wrapper-career .succHide').hide();
                  $('.wrapper-career .successMsg').show();
                }else{
                  document.getElementById("carrerFormError").innerHTML = response.message;
                  document.getElementById('carrerFormDiv').scrollIntoView();
                }
              },
              error: function (e) {
                document.getElementById("carrerFormError").innerHTML = "Failed to apply, please try again!!";
                document.getElementById('carrerFormDiv').scrollIntoView();
              }
            });
          } else {
            document.getElementById("carrerFormError").innerHTML = "Failed to apply, please try again!!";
            document.getElementById('carrerFormDiv').scrollIntoView();
          }
        } else{
          document.getElementById("carrerFormError").innerHTML = response.message;
          document.getElementById('carrerFormDiv').scrollIntoView();
        }
      },
      error: function (e) {
        document.getElementById("carrerFormError").innerHTML = "Failed to apply, please try again!!";
        document.getElementById('carrerFormDiv').scrollIntoView();
      }
    });
	}	
  return false;
});