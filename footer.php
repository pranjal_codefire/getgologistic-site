            <div class="block" seventeen second>
                <div class="wrapper-headband headband-top" style="fill:#fff" eighteen seventeen>
                    <span class="headband left" style="background-color:#0d3b60" eighteen></span>
                    <svg class="icon icon-wave" style="fill:#0d3b60" eighteen>
                        <use xlink:href="#icon-wave"></use>
                    </svg>
                    <button aria-label="Back to top" role="button" type="button" class="back-to-top-button"
                        eighteen><span class="visually-hidden" eighteen>Back to top</span> <img
                            src="img/up-arrow.png" /></button> <span class="headband right"
                        style="background-color:#0d3b60" eighteen></span>
                </div>
                <footer class="footer has-bg bg-primary-dark block" seventeen>
                    <span class="bg" seventeen></span>
                    <div class="container" seventeen>
                        <div class="top" seventeen>
                            <div class="wrapper-logo-cta" seventeen>
                                <a href="/" class="logo clickNext" seventeen>
                                    <span class="visually-hidden" seventeen></span>
                                    <img src="img/logo.png?v=1.4" alt="" />
                                </a>
                                <div class="wrapper-cta" seventeen><button class="btn contactShow" seventeen>Get Free Consultation</button>
                                    <a class="btn modalShow" seventeen>Create free account</a></div>
                            </div>
                            <div seventeen class="footerAbout">
                                <h5 seventeen>About us</h5>
                                <ul seventeen>
                                    <li seventeen>Founded in 2016 we are a technology driven company that is working
                                        towards disrupting the logistics industry in India. Our only goal is to make the
                                        process of moving your goods simpler, faster, safer and cost-effective.
                                        <a href="about-us" seventeen>Read more</a></li>
                                </ul>
                            </div>
                            <div seventeen>
                                <h5 seventeen>Business solutions</h5>
                                <ul seventeen>
                                    <li seventeen><a href="courier" class="clickNext" seventeen>Courier &
                                            Parcels</a>
                                    </li>
                                    <li seventeen><a href="cargo" class="clickNext" seventeen>Express Cargo / Part
                                            Load</a></li>
                                </ul>
                            </div>
                            <div seventeen>
                                <h5 seventeen>Useful links</h5>
                                <ul seventeen>
                                    <li seventeen><a href="about-us" class="clickNext" seventeen>Company</a></li>
                                    <li seventeen><a target="_blank" href="https://getgologistics.com/blog/" class="clickNext" seventeen>Blog</a></li>
                                    <li seventeen><a href="career" class="clickNext" seventeen>Careers</a></li>
                                    <li seventeen><a href="privacy" class="clickNext" seventeen>Privacy</a></li>
                                    <li seventeen><a href="terms" class="clickNext" seventeen>Terms</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="container" seventeen style="margin-top: 100px;">

                    </div>
                    <div class="copy" seventeen>
                        <div class="container" seventeen>© 2019 GetGo Logistics Pvt Ltd</div>
                    </div>
                </footer>
            </div>
            <div class="loading" twenty second>
                <div class="white ov" twenty></div>
                <div class="blue ov" twenty></div>
            </div>
            </div>
            </div>
            </div>
            <div class="wrapper-contact wrapper-contact-us hide" fifth second>
                <div class="overlay" fifth></div>
                <div class="contact-popup" fifth>
                    <div class="header-contact align-center has-bg bg-primary-medium" fifth
                        style="background: #105285;">
                        <div class="succHide" fifth>
                            <h3>Let’s talk logistics</h3>
                            <!-- <h4>Fill out the form and one of our sales representatives will be in touch with you
                                shortly.</h4> -->
                        </div>
                        <svg class="icon icon-wave-inverted succHide" fifth>
                            <use xlink:href="#icon-wave-inverted" fifth></use>
                        </svg>
                        <button aria-label="Close the form" role="button" type="button" class="btn-close" fifth>
                            <span class="visually-hidden" fifth>Close the form</span>
                            <svg class="icon icon-cross" fifth>
                                <use xlink:href="#icon-cross" fifth></use>
                            </svg>
                        </button>
                        <div class="successMsg hide">Success!<br />
                            Thank you for submitting, our team will contact you shortly.
                        </div>
                    </div>
                    <div id="getInTouchFormDiv" class="content-contact succHide" fifth>
                        <div sixth fifth>
                            <p class="errorFr errorFrTop" id="getInTouchFormError"></p>
                            <form id="getInTouchForm" name="getInTouchForm"
                                action="https://accounts.getgologistics.com/auth/save/contact" method="POST" role="form"
                                class="form form--contact-us" sixth modelAttribute="contactUs">
                                <div class="field double" sixth><label for="namec" class="required" seven sixth>
                                        <input name="name" id="namec" placeholder="Full name" required type="text"
                                            seven>
                                        <span class="errorFr" id="nameGError"></span>
                                    </label>
                                    <label for="emailc" class="required" seven sixth><input name="email" id="emailc"
                                            required type="email" placeholder="Email id" seven>
                                        <span class="errorFr" id="emailGError"></span>
                                    </label>
                                </div>
                                <div class="field double" sixth>
                                    <label for="Phonec" class="required" seven sixth><input name="contactNumber"
                                            id="Phonec" placeholder="Contact number" required type="tel" seven>
                                        <span class="errorFr" id="contactNumberError"></span></label>
                                    <label for="companyc" class="required" seven sixth><input name="companyName"
                                            id="companyc" placeholder="Your company name" required type="text"
                                            seven><span class="errorFr" id="companyNameError"></span></label>
                                </div>
                                <div class="field" sixth>
                                    <label for="Dayly__c" seven sixth>
                                        <div class="dropdown">
                                            <label class="dropdown-label">Interested in</label>
                                            <div class="dropdown-list">
                                                <div class="checkbox">
                                                    <input type="checkbox" onchange="valueChangedc()" value="Courier"
                                                        name="interestedIn" class="check checkbox-custom"
                                                        id="checkbox01c" />
                                                    <label for="checkbox01c" class="checkbox-custom-label">Ecommerce
                                                        courier</label>
                                                </div>
                                                <div class="checkbox">
                                                    <input type="checkbox" onchange="Express_Cargoc()"
                                                        value="Express_Cargo" name="interestedIn"
                                                        class="check checkbox-custom" id="checkbox02c" />
                                                    <label for="checkbox02c" class="checkbox-custom-label">Express cargo
                                                        / Part load</label>
                                                </div>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                                <div class="field interest" sixth id="Courierc">
                                    <label for="courier_number_of_shipmentsc" class="select" seven sixth>
                                        <select aria-label="Dayly__c" name="numberOfShipments"
                                            id="courier_number_of_shipmentsc" seven>
                                            <option disabled hidden selected seven>Current number of couriers per
                                                day</option>
                                            <option value="Less than 10 per day" seven>
                                                Less than 10 per day
                                            </option>
                                            <option value="10 to 100 per day" seven>
                                                10 to 100 per day
                                            </option>
                                            <option value="100 to 1000 per day" seven>
                                                100 to 1000 per day
                                            </option>
                                            <option value="More than 1000 per day" seven>
                                                More than 1000 per day
                                            </option>
                                        </select>
                                    </label>
                                </div>
                                <div class="field interest" sixth id="Express_Cargoc">
                                    <label for="movement_per_monthc" class="select" seven sixth>
                                        <select aria-label="movement_per_month" name="transportBillMonthly"
                                            id="movement_per_monthc" seven>
                                            <option disabled hidden selected seven>Current movement per month in
                                                cargo</option>
                                            <option value="less than 1 tonne per month" seven>
                                                Less than 1 tonne per month
                                            </option>
                                            <option value="1 tonne to 10 tonnes per month" seven>
                                                1 tonne to 10 tonnes per month
                                            </option>
                                            <option value="10 tonne to 100 tonnes per month" seven>
                                                10 tonne to 100 tonnes per month
                                            </option>
                                            <option value="more than 100 tonnes per month" seven>
                                                More than 100 tonnes per month
                                            </option>
                                        </select>
                                    </label>
                                </div>
                                <div class="wrapper-submit" style="margin-top: 10px" sixth>
                                    <button type="submit" class="btn huge full-width btn-arrow" sixth>
                                        <span sixth>Submit</span>
                                        <svg class="icon icon-arrow-right" sixth>
                                            <use xlink:href="#icon-arrow-right" sixth></use>
                                        </svg>
                                    </button>
                                </div>
                                <!---->
                            </form>
                        </div>
                    </div>
                    <!---->
                </div>
            </div>
            <div class="wrapper-contact wrapper-career hide" fifth second>
                <div class="overlay" fifth></div>
                <div class="contact-popup" fifth>
                    <div class="header-contact align-center has-bg bg-primary-medium" fifth>
                        <div fifth class="succHide">
                            <h3>Get on board for an exciting career</h3>
                            <h4>A world of career opportunities</h4>
                        </div>
                        <svg class="icon icon-wave-inverted succHide" fifth>
                            <use xlink:href="#icon-wave-inverted" fifth></use>
                        </svg>
                        <button aria-label="Close the form" role="button" type="button" class="btn-close" fifth>
                            <span class="visually-hidden" fifth>Close the form</span>
                            <svg class="icon icon-cross" fifth>
                                <use xlink:href="#icon-cross" fifth></use>
                            </svg>
                        </button>
                        <div class="successMsg hide">Success!<br />
                            Thank you for submitting, our team will contact you shortly.</div>
                    </div>
                    <div id="carrerFormDiv" class="content-contact succHide" fifth>
                        <div sixth fifth>
                            <p class="errorFr errorFrTop" id="carrerFormError"></p>
                            <form id="carrerForm" name="carrerForm" modelAttribute="careers" class="form form--contact-us">
                                <div class="field double" sixth><label for="nameC" class="required" seven sixth><input
                                            name="name" id="nameC" placeholder="Full name" required type="text" seven>
                                        <span class="errorFr" id="nameCError"></span>
                                    </label>
                                    <label for="emailC" class="required" seven sixth><input name="email" id="emailC"
                                            required type="email" placeholder="Email id" seven> <span class="errorFr"
                                            id="emailCError"></span>
                                    </label>
                                </div>
                                <div class="field double" sixth>
                                    <label for="PhoneC" class="required" seven sixth><input name="contactNumber"
                                            id="PhoneC" placeholder="Contact number" required type="tel" seven><span
                                            class="errorFr" id="contactNumberCError"></span></label>
                                    <label for="positionApplied" class="select" seven sixth>
                                        <select aria-label="companyType" name="positionApplied" id="positionApplied"
                                            seven>
                                            <option value="" disabled hidden selected seven>Interested in which position
                                            </option>
                                            <option value="Digital_Marketing" seven>
                                                Digital marketing
                                            </option>
                                            <option value="Business_Development" seven>
                                                Business development
                                            </option>
                                            <option value="Customer_Support" seven>
                                                Customer support
                                            </option>
                                            <option value="Operations" seven>
                                                Operations
                                            </option>
                                            <option value="Engineering" seven>
                                                Engineering
                                            </option>
                                            <option value="Finance" seven>
                                                Finance
                                            </option>
                                        </select>
                                        <span class="errorFr" id="positionAppliedError"></span>
                                    </label>
                                </div>
                                <div class="field" sixth><label for="currentCtc" class="required" seven sixth><input
                                            name="currentCtc" id="currentCtc" placeholder="Current ctc" required
                                            type="text" seven></label>
                                </div>
                                <!---->
                            </form>
                            <form id="documentFormCV" name="documentForm" action="https://accounts.getgologistics.com/auth/save/career" method="POST" role="form"
                                 sixth modelAttribute="document" enctype="multipart/form-data">
                                <span class="errorFr" id="documentError"></span>
                                <div class="field" sixth><label for="document " class="required" seven sixth>
                                    <input name="document" id="document" type="file" seven></label>
                                </div>
                                <div class="wrapper-submit" sixth>
                                    <button type="submit" class="btn huge full-width btn-arrow" sixth>
                                        <span sixth>Submit</span>
                                        <svg class="icon icon-arrow-right" sixth>
                                            <use xlink:href="#icon-arrow-right" sixth></use>
                                        </svg>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!---->
                </div>
            </div>

            <div class="loaderSign">
                <img src="img/loader.gif" alt="">
            </div>

            <a class="mobileWhatsapp" href="https://api.whatsapp.com/send?phone=918928445993&source=getgo"><img src="img/Whatsapp-logo.png"></a>
            <!--Start of Tawk.to Script-->
            <script type="text/javascript">
                var Tawk_API = Tawk_API || {},
                    Tawk_LoadStart = new Date();
                (function () {
                    var s1 = document.createElement("script"),
                        s0 = document.getElementsByTagName("script")[0];
                    s1.async = true;
                    s1.src = 'https://embed.tawk.to/5cd2a2df2846b90c57ad7710/default';
                    s1.charset = 'UTF-8';
                    s1.setAttribute('crossorigin', '*');
                    s0.parentNode.insertBefore(s1, s0);
                })();
            </script>
            <!--End of Tawk.to Script-->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
            <script src="js/owl.carousel.min.js?v=2.2"></script>
            <script src="js/custom.js?v=3.8.1"></script>
            <script>
                function valueChanged() {
                    if ($('#checkbox01').is(":checked"))
                        $("#Courier").css('display', 'flex');
                    else
                        $("#Courier").css('display', 'none');
                }

                function Express_Cargo() {
                    if ($('#checkbox02').is(":checked"))
                        $("#Express_Cargo").css('display', 'flex');
                    else
                        $("#Express_Cargo").css('display', 'none');
                }

                function valueChangedc() {
                    if ($('#checkbox01c').is(":checked"))
                        $("#Courierc").css('display', 'flex');
                    else
                        $("#Courierc").css('display', 'none');
                }

                function Express_Cargoc() {
                    if ($('#checkbox02c').is(":checked"))
                        $("#Express_Cargoc").css('display', 'flex');
                    else
                        $("#Express_Cargoc").css('display', 'none');
                }
            </script>
            </body>

            </html>
